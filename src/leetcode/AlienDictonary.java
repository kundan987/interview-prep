package leetcode;

import java.util.*;

public class AlienDictonary {

    public String alienDict(String[] words){

        if(words == null || words.length==0)
            return "";
        Map<Character, Integer> indegree = new HashMap<>();
        Map<Character, List<Character>> graph = new HashMap<>();

        for(String word : words){
            for(char ch : word.toCharArray()){
                indegree.put(ch, 0);
                graph.put(ch, new ArrayList<>());
            }
        }

        for(int i=0; i< words.length-1; i++){
            String word1 = words[i], word2 = words[i+1];

            if(word1.length()>word2.length() && word1.contains(word2))
                return "";

            for (int j =0; j<Math.min(word1.length(), word2.length()); j++){
                char p = word1.charAt(j), c = word2.charAt(j);
                if(p != c){
                    indegree.put(c, indegree.get(c)+1);
                    graph.get(p).add(c);
                    break;
                }
            }
        }

        Queue<Character> queue = new LinkedList<>();
        for(Map.Entry<Character, Integer> map : indegree.entrySet())
            if (map.getValue()==0)
                queue.add(map.getKey());

        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()){
            Character vertex = queue.poll();
            sb.append(vertex);
            for (char c : graph.get(vertex)){
                indegree.put(c, indegree.get(c)-1);
                if (indegree.get(c)==0)
                    queue.add(c);
            }
        }

        if (indegree.size() != sb.length())
            return "";
        return sb.toString();
    }
}
