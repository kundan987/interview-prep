package leetcode;

import java.util.Arrays;
import java.util.PriorityQueue;

public class CarPooling {

    public static void main(String[] args) {
        CarPooling carPooling = new CarPooling();
        //[[9,3,4],[9,1,7],[4,2,4],[7,4,5]]
        //23
        System.out.println(carPooling.carPooling(new int[][]{{3,2,7},{3,7,9},{8,3,9}}, 11));
    }


    public boolean carPooling(int[][] trips, int capacity) {
        Arrays.sort(trips, (a,b)->a[1]-b[1]);
        PriorityQueue<int[]> queue = new PriorityQueue<>((a,b)->a[2]-b[2]);

        if(trips[0][0]> capacity)
            return false;
        int count =trips[0][0];
        queue.add(trips[0]);
        for(int i=1; i< trips.length; i++){
            int[] temp = queue.peek();
            int cur[] = trips[i];
            count = count+cur[0];
            if(cur[0]> capacity)
                return false;
            if(temp[2]> cur[1] && count>capacity){
                return false;
            } else if(temp[2]<= cur[1]){
                while(!queue.isEmpty() && temp[2]<= cur[1]){
                    count -=temp[0];
                    queue.poll();
                    if(!queue.isEmpty())
                        temp = queue.peek();
                }
                queue.add(cur);
            }
        }
        return true;

    }
}
