package leetcode.premium;

import java.util.*;

public class MeetingRooms {
    public static void main(String[] args) {
        MeetingRooms meetingRooms = new MeetingRooms();

        System.out.println(meetingRooms.canAttendMeetings(new int[][]{{0,30},{5,10},{15,20}}));
        System.out.println(meetingRooms.canAttendMeetings(new int[][]{{7,10},{2,4}}));

        System.out.println(meetingRooms.minMeetingRooms(new int[][]{{7,10},{2,4}}));
        System.out.println(meetingRooms.minMeetingRooms(new int[][]{{0,30},{5,10},{15,20}}));

        System.out.println(meetingRooms.wordBreak("leetcode", Arrays.asList("leet", "code")));
        System.out.println(meetingRooms.wordBreak("catsandog", Arrays.asList("cats", "dog", "sand", "and", "cat")));
    }

    public boolean wordBreak(String s, List<String> wordDict) {
        return rec(s, new HashSet<>(wordDict), 0, new Boolean[s.length()]);
    }
    public boolean rec(String s, Set<String> words, int start, Boolean[] dp){
        if(start==s.length()){
            return true;
        }
        if(dp[start] != null)
            return dp[start];
        for(int end = start+1; end<=s.length();end++)
            if(words.contains(s.substring(start, end)) && rec(s, words, end, dp))
                return dp[start] = true;
        return dp[start] = false;
    }

    public int minMeetingRooms(int[][] intervals) {
        Arrays.sort(intervals, Comparator.comparingInt(a->a[0]));
        PriorityQueue<int[]> heap = new PriorityQueue<>(Comparator.comparingInt(a->a[1]));
        int min =0;
        for(int[] interval : intervals){
            while (!heap.isEmpty() && interval[0] >= heap.peek()[1])
                heap.poll();
            heap.offer(interval);
            min = Math.max(min, heap.size());
        }
        return min;
    }


    public boolean canAttendMeetings(int[][] intervals) {

        Arrays.sort(intervals, Comparator.comparingInt(a->a[0]));
        Arrays.sort(intervals, (a,b)-> a[0]-b[0]);
        int end = intervals[0][1];
        int i = intervals.length;
        int j=1;
        while(j<i){
            if(end>intervals[j][0])
                return false;
            end = intervals[j][1];
            j++;
        }
        return true;
    }
}
