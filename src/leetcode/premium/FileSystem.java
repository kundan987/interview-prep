package leetcode.premium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class FileSystem {
    Directory root;
    public FileSystem() {
        root = new Directory();
    }

    public List<String> ls(String path) {
        List<String> res = new ArrayList<>();
        Directory directory = path(path);
        res.addAll(directory.files.keySet());
        res.addAll(directory.dirs.keySet());
        return res;
    }

    private Directory path(String path){
        String[] ar = path.split("/");
        int i=0;
        int n = ar.length;
        if(ar.length == 0) {
            return root;
        }
        Directory directory = root;
        while(i<n-1){
            if(!directory.dirs.containsKey(ar[i]))
                directory.dirs.put(ar[i], new Directory());
            directory = directory.dirs.get(ar[i]);
            i++;
        }
        return directory;
    }

    public void mkdir(String path) {
        path(path);
    }

    public void addContentToFile(String filePath, String content) {
        String[] ar = filePath.split("/");
        Directory directory = path(filePath.substring(0, filePath.length()-ar.length-1));
        directory.files.put(ar[ar.length-1], directory.files.getOrDefault(ar[ar.length-1], "")+content);
    }

    public String readContentFromFile(String filePath) {
        String[] ar = filePath.split("/");
        Directory directory = path(filePath.substring(0, filePath.length()-ar.length-1));
        return directory.files.get(ar[ar.length-1]);
    }
}

class Directory {
    Map<String, Directory> dirs;
    Map<String, String> files;

    Directory(){
        dirs = new HashMap<>();
        files = new HashMap<>();
    }
}

/**
 * Your FileSystem object will be instantiated and called as such:
 * FileSystem obj = new FileSystem();
 * List<String> param_1 = obj.ls(path);
 * obj.mkdir(path);
 * obj.addContentToFile(filePath,content);
 * String param_4 = obj.readContentFromFile(filePath);
 */
