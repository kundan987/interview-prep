package leetcode.premium;

import java.util.*;

class BestMeetingpoint {
    int row;
    int col;

    public static void main(String[] args) {
        BestMeetingpoint bestMeetingpoint = new BestMeetingpoint();

        int[][] grid = {{1,0,0,0,1},{0,0,0,0,0},{0,0,1,0,0}};
        System.out.println(bestMeetingpoint.minTotalDistance(grid));
        System.out.println(bestMeetingpoint.minTotalDistanceManhattan(grid));
        System.out.println(bestMeetingpoint.minTotalDistanceSorting(grid));
    }

    public int minTotalDistanceSorting(int[][] grid) {
        if(grid == null || grid.length == 0) return -1;
        int min=Integer.MAX_VALUE;
        this.row = grid.length;
        this.col = grid[0].length;
        List<Integer> rows = new ArrayList<>();
        List<Integer> cols = new ArrayList<>();
        for(int i=0; i<grid.length; i++) {
            for(int j=0; j<grid[0].length; j++) {
                if(grid[i][j]==1){
                    rows.add(i);
                    cols.add(j);
                }
            }
        }
        int r = rows.get(rows.size()/2);
        Collections.sort(cols);
        int c = cols.get(cols.size()/2);

        return minTotalDistance1D(rows, r) + minTotalDistance1D(cols, c);
    }

    public int minTotalDistance1D(List<Integer> points, int origin){
        int distance=0;
        for(Integer point : points)
            distance+=Math.abs(point-origin);
        return distance;
    }


    public int minTotalDistanceManhattan(int[][] grid) {
        if(grid == null || grid.length == 0) return -1;
        int min=Integer.MAX_VALUE;
        this.row = grid.length;
        this.col = grid[0].length;
        List<Point> points = new ArrayList<>();
        for(int i=0; i<grid.length; i++) {
            for(int j=0; j<grid[0].length; j++) {
                if(grid[i][j]==1)
                    points.add(new Point(i, j));
            }
        }
        for(int i=0; i<grid.length; i++) {
            for(int j=0; j<grid[0].length; j++) {
                int distance = manhattan(points, i, j);
                min = Math.min(min, distance);
            }
        }
        return min;
    }

    private int manhattan(List<Point> points, int r, int c) {
        int distance=0;
        for(Point point : points){
            distance+= Math.abs(point.row - r) + Math.abs(point.col - c);
        }
        return distance;
    }

    public int minTotalDistance(int[][] grid) {
        if(grid == null || grid.length == 0) return -1;
        int min=Integer.MAX_VALUE;
        this.row = grid.length;
        this.col = grid[0].length;
        for(int i=0; i<grid.length; i++) {
            for(int j=0; j<grid[0].length; j++) {
                    int distance = bfs(grid, i, j);
                    min = Math.min(min, distance);
            }
        }
        return min;
    }
    public int bfs(int[][] grid, int x, int y){
        Queue<Point> queue = new LinkedList<>();

        boolean[][] visited = new boolean[row][col];
        queue.add(new Point(x,y,0));
        int distance =0;
        while(!queue.isEmpty()){
            Point point = queue.poll();
            int r = point.row;
            int c = point.col;
            int d = point.distance;


            if (r < 0 || c < 0 || r >= row || c >= col || visited[r][c]) {
                continue;
            }

            if(grid[r][c] == 1)
                distance+= d;

            visited[r][c] = true;
            queue.add(new Point(r-1,c, d+1));
            queue.add(new Point(r+1,c, d+1));
            queue.add(new Point(r,c-1, d+1));
            queue.add(new Point(r,c+1, d+1));

        }
        return distance;
    }
    public class Point {
        int row;
        int col;
        int distance;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public Point(int row, int col, int distance) {
            this.row = row;
            this.col = col;
            this.distance = distance;
        }
    }

}

