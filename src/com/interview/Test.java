package com.interview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Test {

    public static void main(String[] args) {

        int arr[] = {1,1,1,3,3,2,2,4};
        HashMap<Integer, Integer> hm = new HashMap<>();
        ArrayList<Integer> al = new ArrayList<>();

        for(int i=0;i<arr.length;i++) {
            if(!hm.containsKey(arr[i])) {
                hm.put(arr[i], 1);
                al.add(arr[i]);
            }else{
                int val = hm.get(arr[i]);
                hm.put(arr[i], val+1);
                al.add(arr[i]);
            }
        }

        Collections.sort(al, new Sort(hm));

        for(Integer a : al) {
            System.out.println(a);
        }

    }
}


class Sort implements Comparator<Integer> {


    HashMap<Integer, Integer> hm;
    Sort(HashMap<Integer, Integer> hm) {
        this.hm = hm;
    }

    @Override
    public int compare(Integer o1, Integer o2) {

        int freq = hm.get(o1).compareTo(hm.get(o2));
        int val = o1.compareTo(o2);

        if(freq==0) {
            return  val;
        }else{
            return freq;
        }


    }



}