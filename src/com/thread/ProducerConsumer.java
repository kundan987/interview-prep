package com.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

public class ProducerConsumer {
    public static void main(String[] args) {
        List<String> buffer = new ArrayList<>();
        ReentrantLock bufferLock = new ReentrantLock();

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        MyProducer producer = new MyProducer(buffer, bufferLock);
        MyConsumer consumer1 = new MyConsumer(buffer, bufferLock);
        MyConsumer consumer2 = new MyConsumer(buffer, bufferLock);

        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("I'm printed from callable method");
                return "This is the callable result";
            }
        });

        try{
            System.out.println(future.get());
        } catch (ExecutionException ee){
            System.out.println("Something went wrong");
        } catch (InterruptedException ie){
            System.out.println("Thread got interrupted");
        }

        executorService.shutdown();

/*
        Thread tp = new Thread(producer);
        tp.setName("producer");
        tp.start();*/

        /*MyProducer producer2 = new MyProducer(buffer);
        Thread tp2 = new Thread(producer2);
        tp2.setName("producer2");
        tp2.start();*/

       /* Thread tc1 = new Thread(consumer1);
        tc1.setName("consumer1");
        tc1.start();
        Thread tc2 = new Thread(consumer2);
        tc2.setName("consumer2");
        tc2.start();*/

       // new Thread(producer).start();
       /* new Thread(consumer1).setName("consumer1");
        new Thread(consumer1).start();
        new Thread(consumer2).setName("consumer2");
        new Thread(consumer2).start();*/
    }
}

class MyProducer implements Runnable{
    private List<String> buffer;
    ReentrantLock bufferLock;

    public MyProducer(List<String> buffer, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.bufferLock = bufferLock;
    }

    @Override
    public void run() {
        Random random = new Random();

        String[] nums = {"1", "2", "3", "4"};

        for (String num :nums){
            try{
                System.out.println("Adding.. "+ num + " by "+ Thread.currentThread().getName());
                bufferLock.lock();
                buffer.add(num);
                bufferLock.unlock();
                /*synchronized (buffer){
                    buffer.add(num);
                }*/
                Thread.sleep(random.nextInt(1000));
            }catch (InterruptedException e){

            }
        }
        System.out.println("Adding EOF and Exiting");
        bufferLock.lock();
        buffer.add("EOF");
        bufferLock.unlock();
        /*synchronized (buffer){
            buffer.add("EOF");
        }*/
    }
}

class MyConsumer implements Runnable{
    private List<String> buffer;
    ReentrantLock bufferLock;
    int counter =0;

    public MyConsumer(List<String> buffer, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.bufferLock = bufferLock;
    }

    @Override
    public void run() {
        while (true){
            if (bufferLock.tryLock()){
                try{

                    if (buffer.isEmpty()){

                        continue;
                    }
                     System.out.println("Counter values : "+ counter);
                    counter=0;
                    if (buffer.get(0).equals("EOF")){
                        System.out.println("Exiting");
                        break;
                    }else{
                        System.out.println("removed " + buffer.remove(0) + " by "+ Thread.currentThread().getName());
                    }
                } finally {
                    bufferLock.unlock();
                }
            }else {
                counter++;
            }
            /*synchronized (buffer){

            }*/

        }

    }
}
