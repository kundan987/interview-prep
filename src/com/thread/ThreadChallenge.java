package com.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadChallenge {

    public static void main(String[] args) {

       // Lock lock = new ReentrantLock();

        BankAccount account = new BankAccount(1000.0, "0165");

        /*Thread t1 = new Thread(){
            public void run() {
                account.deposit(300);
                account.withdraw(50);
            }
        };

        Thread t2 = new Thread(){
            public void run(){
                account.deposit(203.75);
                account.withdraw(100);
            }
        };*/

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(300);
                account.withdraw(50);
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {

                account.deposit(203.75);
                account.withdraw(100);
            }
        });

        t1.start();
        t2.start();

    }
}

class BankAccount{
    private double balance;
    private String accountNumber;
    private Lock lock;

    public BankAccount(double balance, String accountNumber) {
        this.balance = balance;
        this.accountNumber = accountNumber;
        this.lock= new ReentrantLock();
      //  System.out.println("Account Number : "+accountNumber + " -  initial Balance : "+ balance);

    }

     public void deposit(double amount){

        boolean status = false;
        try{
            if (lock.tryLock(1000, TimeUnit.MILLISECONDS)){
                try{
                    balance+=amount;
                    status = true;
                } finally {
                    lock.unlock();
                }

            }else{
                System.out.println("Could not get the lock");
            }
        }catch (InterruptedException e){
            //do something here
        }

         System.out.println("Transaction status : "+status );
    }

    public void withdraw(double amount){

        boolean status = false;
        try{
            if (lock.tryLock(1000, TimeUnit.MILLISECONDS)){
                try{
                    balance-=amount;
                    status = true;
                } finally {
                    lock.unlock();
                }

            }else{
                System.out.println("Could not get the lock");
            }
        }catch (InterruptedException e){
            //do something here
        }
        System.out.println("Transaction status : "+ status);
    }

    /*public void deposit(double amount){
        synchronized (this){

            System.out.println("Balance before deposit : "+ balance);
            balance+=amount;
            System.out.println("Balance after deposit : "+ balance);
        }
    }

    public void withdraw(double amount){
        synchronized (this){

            System.out.println("Balance before withdraw : "+ balance);
            balance-=amount;
            System.out.println("Balance after withdraw : "+ balance);
        }
    }*/

    public String getAccountNumber(){
        return accountNumber;
    }

    public void printAccountNumber(){
        System.out.println("Account number : "+accountNumber);
    }

   /* public synchronized void deposit(double amount){
        System.out.println("Balance before deposit : "+ balance);
        balance+=amount;
        System.out.println("Balance after deposit : "+ balance);
    }

    public synchronized void withdraw(double amount){
        System.out.println("Balance before withdraw : "+ balance);
        balance-=amount;
        System.out.println("Balance after withdraw : "+ balance);
    }*/
}
