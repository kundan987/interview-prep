package com.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

public class ProducerConsumerWithArrayBlockingQueue {
    public static void main(String[] args) {
        ArrayBlockingQueue<String> buffer = new ArrayBlockingQueue<>(6);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        MyProducers producer = new MyProducers(buffer);
        MyConsumers consumer1 = new MyConsumers(buffer);
        MyConsumers consumer2 = new MyConsumers(buffer);

        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("I'm printed from callable method");
                return "This is the callable result";
            }
        });

        try{
            System.out.println(future.get());
        } catch (ExecutionException ee){
            System.out.println("Something went wrong");
        } catch (InterruptedException ie){
            System.out.println("Thread got interrupted");
        }

        executorService.shutdown();

/*
        Thread tp = new Thread(producer);
        tp.setName("producer");
        tp.start();*/

        /*MyProducer producer2 = new MyProducer(buffer);
        Thread tp2 = new Thread(producer2);
        tp2.setName("producer2");
        tp2.start();*/

       /* Thread tc1 = new Thread(consumer1);
        tc1.setName("consumer1");
        tc1.start();
        Thread tc2 = new Thread(consumer2);
        tc2.setName("consumer2");
        tc2.start();*/

        // new Thread(producer).start();
       /* new Thread(consumer1).setName("consumer1");
        new Thread(consumer1).start();
        new Thread(consumer2).setName("consumer2");
        new Thread(consumer2).start();*/
    }
}

class MyProducers implements Runnable{
    private ArrayBlockingQueue<String> buffer;

    public MyProducers(ArrayBlockingQueue<String> buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        Random random = new Random();

        String[] nums = {"1", "2", "3", "4", "5"};

        for (String num :nums){
            try{
                System.out.println("Adding.. "+ num + " by "+ Thread.currentThread().getName());
                buffer.put(num);
                /*bufferLock.lock();
                try{
                    buffer.add(num);
                } finally {
                    bufferLock.unlock();
                }*/
                Thread.sleep(random.nextInt(1000));
            }catch (InterruptedException e){

            }
        }
        System.out.println("Adding EOF and Exiting");
        try {
            buffer.put("EOF");
        } catch (InterruptedException e){

        }

        /*bufferLock.lock();
        try{
            buffer.add("EOF");
        } finally {
            bufferLock.unlock();
        }*/
    }
}

class MyConsumers implements Runnable{
    private ArrayBlockingQueue<String> buffer;
    int counter =0;

    public MyConsumers(ArrayBlockingQueue<String> buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (true){
            synchronized (buffer){
                try{
                    if (buffer.isEmpty()){
                        continue;
                    }
                    if (buffer.peek().equals("EOF")){
                        System.out.println("Exiting");
                        break;
                    }else{
                        System.out.println("removed " + buffer.take() + " by "+ Thread.currentThread().getName());
                    }
                } catch (InterruptedException e){

                }
            }

        }

    }
}
