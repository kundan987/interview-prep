package com.sixt;


public class Test {



    public static void main(String[] args) {

        Anagram anagram = new Anagram();

        String lhs = "kundan";
        String rhs = "mishra";

        boolean result = anagram.isAnagram(lhs, rhs);

        if (result)
            System.out.println(lhs + " and "+ rhs +" is an anagram!");
        else
            System.out.println(lhs + " and "+ rhs +" is not an anagram!");

        lhs = "anagram";
        rhs ="nagaram";

        result = anagram.isAnagram(lhs, rhs);

        if (result)
            System.out.println(lhs + " and "+ rhs +" is an anagram!");
        else
            System.out.println(lhs + " and "+ rhs +" is not an anagram!");
    }

}
