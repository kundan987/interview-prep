package com.sixt;

public class Anagram {



    public static boolean isAnagram(String lhs, String rhs){

        if (lhs==null || rhs == null)
            return false;

        int len = lhs.length();

        int[] l = new int[256];

        for(int i =0; i< len; i++){
            l[lhs.charAt(i)]++;
            l[rhs.charAt(i)]--;
        }

        for (int i=0; i< 256; i++){
            if (l[i]!=0)
                return false;
        }

        return true;
    }
}
