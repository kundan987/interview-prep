package com.apple;

public class Matrix {

    public static void main (String[] args)
    {
        int mat1[][] = { { 1, 5, 6 },
                { 8, 10, 11 },
                { 15, 16, 18 } };

        int mat2[][] = { { 2, 4, 7 },
                { 9, 10, 12 },
                { 13, 16, 20 } };

        int n = 3;
        int x = 21;

        System.out.println ( "Count = " + countPairs(mat1, mat2, n, x));

    }

    private static int countPairs(int[][] mat1, int[][] mat2, int n, int x) {
        int count =0;
        int i =0, j=0;
        int k = n-1, l =n-1;
        while (i < n && j < n && k >= 0 && l >= 0 ){

            if (mat1[i][j]+mat2[k][l] == x){
                System.out.println(mat1[i][j] + " " + mat2[k][l]);
                count++;
                if (j == n-1){
                    i++;
                    j=0;
                }else
                    j++;
                if (l<=0){
                    k--;
                    l=n-1;
                }else
                    l--;
            } else if (mat1[i][j]+mat2[k][l] > x){
                if (l<=0){
                    k--;
                    l=n-1;
                }else
                    l--;
            } else {
                if (j == n-1){
                    i++;
                    j=0;
                }else
                    j++;
            }
        }
        return count;
    }
}
