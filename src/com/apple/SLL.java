package com.apple;


public class SLL {

    Node head;

    class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {

        System.out.println("kundan".substring(2,5));
        SLL llist = new SLL();

        // Insert 6.  So linked list becomes 6->NUllist
        llist.append(6);

        // Insert 7 at the beginning. So linked list becomes
        // 7->6->NUllist
        llist.push(7);

        // Insert 1 at the beginning. So linked list becomes
        // 1->7->6->NUllist
        llist.push(1);

        // Insert 4 at the end. So linked list becomes
        // 1->7->6->4->NUllist
        llist.append(4);

        // Insert 8, after 7. So linked list becomes
        // 1->7->8->6->4->NUllist
        llist.insertAfter(llist.head.next, 8);

        llist.printList();
        llist.lengthIterative();
        System.out.println("Recursive Length : "+ llist.lengthRecursive(llist.head));
        llist.searchIterative(8);
        llist.searchRecursive(8);
        llist.nthNode(llist.head, 2);

        llist.delete(8);

        llist.printList();
        llist.lengthIterative();
        System.out.println("Recursive Length : "+ llist.lengthRecursive(llist.head));
        llist.searchIterative(8);
        llist.searchRecursive(8);
        llist.nthNode(llist.head, 2);
        llist.nthNodeFromEnd(llist.head, 2);
       // llist.swapEveryTwoNode();
        llist.printList();
        llist.head = llist.reverse(llist.head);
        llist.printList();
        llist.head = llist.reverseRecursive(llist.head);
        llist.printList();
        System.out.println("before swapping");
        llist.swapNodeWithoutSwappingData(1, 6);
        System.out.println("swapNodeWithoutSwappingData");
        llist.printList();
        llist.swapEveryTwoNode();
        System.out.println("swapEveryTwoNode");
        llist.printList();
        llist.swapEveryTwoNodeRecursively(llist.head);
        System.out.println("swapEveryTwoNodeRecursively");
        llist.printList();
        llist.head = llist.swapEveryTwoNodeByChangingLinks(llist.head);
        System.out.println("swapEveryTwoNodeByChangingLinks");
        llist.printList();
        llist.head = llist.swapEveryTwoNodeByChangingLinksRecursively(llist.head);
        System.out.println("swapEveryTwoNodeByChangingLinksRecursively");
        llist.printList();
        llist.head = llist.addNumbersRespByList(llist.head, llist.head);
        System.out.println("addNumbersRespByList");
        llist.printList();
        llist.deleteGivenNode(llist.head.next);
        System.out.println("deleteGivenNode");
        llist.printList();


    }

    private void deleteGivenNode(Node node) {
        node.data = node.next.data;
        node.next = node.next.next;
    }

    private Node addNumbersRespByList(Node first, Node second) {
        Node res = null;
        Node temp = null;
        Node prev = null;

        int carry = 0, sum;

        while (first!= null || second != null){
            sum = carry + (first!=null? first.data :0 ) + (second!=null? second.data :0 );

            carry = sum>=10? 1: 0;
            sum = sum%10;

            temp = new Node(sum);

            if (res == null)
                res = temp;
            else
                prev.next = temp;

            prev = temp;

            if (first != null)
                first = first.next;

            if (second != null)
                second = second.next;
        }

        if (carry>0)
            prev.next = new Node(carry);

        return res;
    }


    private Node swapEveryTwoNodeByChangingLinksRecursively(Node node) {
        if (node == null || node.next==null)
            return node;

        Node remaining = node.next.next;

        Node newHead = node.next;

        node.next.next = node;

        node.next = swapEveryTwoNodeByChangingLinksRecursively(remaining);

        return newHead;
    }

    private Node swapEveryTwoNodeByChangingLinks(Node node) {
        if (node == null || node.next==null)
            return node;

        Node prev = node;
        Node cur = node.next;
        node = cur;
        while (true){

            Node next = cur.next;
            cur.next = prev;

            if (next == null || next.next==null){
                prev.next =next;
                break;
            }
            prev.next = next.next;
            prev = next;
            cur=prev.next;

        }

        return node;
    }

    private void swapEveryTwoNodeRecursively(Node node) {

        if (node != null && node.next != null){
            int temp = node.data;
            node.data = node.next.data;
            node.next.data = temp;
            swapEveryTwoNodeRecursively(node.next.next);
        }

    }

    private void swapEveryTwoNode() {
        Node node = head;

        while (node != null && node.next != null){

            int temp = node.data;
            node.data = node.next.data;
            node.next.data = temp;
            node = node.next.next;
        }
    }

    private void swapNodeWithoutSwappingData(int x, int y) {

        if (x == y)
            return;
        Node prevX=null , curX=head;
        while(curX != null && curX.data != x){
            prevX = curX;
            curX = curX.next;
        }
        Node prevY=null , curY=head;
        while(curY != null && curY.data != y){
            prevY = curY;
            curY = curY.next;
        }

        if (curX == null || curY == null)
            return;

        if (prevX != null)
            prevX.next = curY;
        else
            head = curY;

        if (prevY != null)
            prevY.next = curX;
        else
            head = curX;

        Node temp = curY.next;
        curY.next = curX.next;
        curX.next = temp;

    }

    private Node reverseRecursive(Node head) {

        if (head == null || head.next == null)
            return head;

        Node rest = reverseRecursive(head.next);
        head.next.next = head;

        head.next = null;

        return rest;
    }

    private Node reverse(Node node) {
        Node cur = node;
        Node next = null;
        Node prev = null;
        while(cur != null){
            next = cur.next;
            cur.next = prev;
            prev = cur;
            cur = next;
        }
        node = prev;
        return node;
    }

    private void nthNodeFromEnd(Node node, int i) {

    }

    private void nthNode(Node node, int i) {
        int len =0;
        while (node != null){
            if (len == i){
                System.out.println("Nth node : "+node.data);
                return;
            }
            len ++;
            node = node.next;
        }
    }

    private void searchRecursive(int i) {
        boolean found = searchRecursive(head, 8);
        if (found)
            System.out.println("Found recursively");
        else
            System.out.println("Not found recursively");
    }

    private boolean searchRecursive(Node node, int i) {
        if (node != null && node.data != i)
            return searchRecursive(node.next,i);
        else if (node != null && node.data==i)
            return true;
        return false;
    }

    private void searchIterative(int i) {
        Node node = head;

        while (node != null){
            if (node.data==i){
                System.out.println("Found");
                return;
            }
            node = node.next;
        }
        System.out.println("Not found");
    }

    private int lengthRecursive(Node node) {
        if (node != null)
            return 1+lengthRecursive(node.next);
        return 0;
    }

    private void lengthIterative() {
        Node cur = head;
        int len=0;

        while(cur != null){
            len++;
            cur = cur.next;
        }
        System.out.println("Length of list : "+ len);
    }

    private void delete(int i) {
        if (head == null){
            System.out.println("list is empty");
            return;
        }
        Node cur = head;
        Node prev = null;
        while(cur.data != i){
            prev = cur;
            cur = cur.next;
        }
        prev.next = cur.next;


    }

    private void insertAfter(Node node, int i) {
        Node curr = new Node(i);
        curr.next = node.next;
        node.next = curr;

    }

    private void printList() {
        Node cur = head;
        while(cur != null){
            System.out.print(cur.data + "->");
            cur = cur.next;
        }
        System.out.println();
    }

    private void push(int i) {
        if ( head== null){
            head = new Node(i);
            return;
        }
        Node node = new Node(i);
        node.next=head;
        head = node;
    }

    private void append(int i) {

        if (head == null){
            head = new Node(i);
            return;
        }

        Node cur = head;
        while (cur.next != null){
            cur = cur.next;
        }
        cur.next = new Node(i);
    }
}
