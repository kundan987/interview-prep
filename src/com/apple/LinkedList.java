package com.apple;

public class LinkedList {

    Node head;

    static class Node{
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        LinkedList ll = new LinkedList();

        ll.head = new Node(1);

        Node second = new Node(2);
        Node third = new Node(3);

        ll.head.next = second;
        second.next = third;

        printList(ll.head);
        ll.head = createList(ll.head, 4);
        printList(ll.head);

    }

    private static Node createList(Node head, int i) {

        Node node = new Node(i);
        node.next = head;
        head=node;
        return head;
    }

    private static void printList(Node node) {

        while(node!= null){
            System.out.println(node.data);
            node = node.next;
        }
    }
}
