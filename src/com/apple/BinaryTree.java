package com.apple;

import java.util.Stack;

class Node {
    int data;
    Node left,right;

    public Node(int data) {
        this.data = data;
        left = right = null;
    }
}

public class BinaryTree {

    Node root = null;

    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.root = new Node(10);
        binaryTree.root.left = new Node(5);
        binaryTree.root.right = new Node(15);
        binaryTree.root.left.left = new Node(2);
        binaryTree.root.left.right = new Node(8);
        binaryTree.root.right.left = new Node(12);
        binaryTree.root.right.right = new Node(17);

        System.out.println("inorder");
        binaryTree.inorder(binaryTree.root);
        System.out.println("\npreorder");
        binaryTree.preorder(binaryTree.root);
        System.out.println("\npostorder");
        binaryTree.postorder(binaryTree.root);
        System.out.println("\nlevelOrder");
        binaryTree.levelOrder(binaryTree.root);
        System.out.println("\nlevelOrderLevelByLevel");
        binaryTree.levelOrderLevelByLevel(binaryTree.root);
        System.out.println("iterativeInorder using stack");
        binaryTree.iterativeInorder(binaryTree.root);


    }

    private void iterativeInorder(Node root) {

        Stack<Node> stack = new Stack<>();
        Node current = root;

        while (current != null || !stack.isEmpty()){
            while (current!=null){
                stack.push(current);
                current=current.left;
            }
            current = stack.pop();
            System.out.print(current.data + " ");
            current = current.right;
        }
    }

    private void levelOrderLevelByLevel(Node root) {

        Node node = root;

        int height = height(node);
        for (int i=0; i < height ; i++){
            printGivenLevel(node, i);
            System.out.println();
        }
    }

    private void levelOrder(Node root) {

        Node node = root;

        int height = height(node);
        for (int i=0; i < height ; i++){
            printGivenLevel(node, i);
        }
    }

    private void printGivenLevel(Node node, int i) {

        if (i>0){
            printGivenLevel(node.left, i-1);
            printGivenLevel(node.right, i-1);
        }else{
            System.out.print(node.data + " ");
        }
    }

    private int height(Node node) {

        if (node != null){
            int lheight = height(node.left);
            int rheight = height(node.right);

            if (lheight> rheight)
                return lheight+1;
            else
                return rheight+1;
        } else
            return 0;

    }

    private void inorder(Node node) {
        if (node == null)
            return;
        inorder(node.left);
        System.out.print(node.data + " ");
        inorder(node.right);
    }
    private void preorder(Node node) {
        if (node == null)
            return;
        System.out.print(node.data + " ");
        preorder(node.left);
        preorder(node.right);
    }
    private void postorder(Node node) {
        if (node == null)
            return;
        postorder(node.left);
        postorder(node.right);
        System.out.print(node.data + " ");
    }
}
