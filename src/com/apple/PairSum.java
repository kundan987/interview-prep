package com.apple;

import java.util.*;
import java.util.LinkedList;

public class PairSum {
    public static void main(String[] args) {
        Integer arr1[] = {3, 1, 5, 7};
        Integer arr2[] = {8, 2, 5, 3};

        // create linked list1 3->1->5->7
        LinkedList<Integer> head1 = new LinkedList<>(Arrays.asList(arr1));

        // create linked list2 8->2->5->3
        LinkedList<Integer> head2 = new LinkedList<>(Arrays.asList(arr2));

        int x = 10;

        System.out.println("Count = " + countPairs(head1, head2, x));
        System.out.println("Count = " + countPairsSorted(head1, head2, x));
        System.out.println("Count = " + countPairsHashing(head1, head2, x));
        System.out.println("Count = " + countPairsProduct(head1, head2, x));
    }

    private static int countPairsProduct(LinkedList<Integer> head1, LinkedList<Integer> head2, int x) {
        int count =0;
        Iterator iterator1 = head1.iterator();

        while (iterator1.hasNext()){
            int i = (int) iterator1.next();
            Iterator iterator2 = head2.iterator();
            while (iterator2.hasNext()){
                int j = (int) iterator2.next();
                if (i*j==x)
                    count++;
            }
        }
        return count;
    }

    private static int countPairsHashing(LinkedList<Integer> head1, LinkedList<Integer> head2, int x) {
        int count=0;
        Iterator iterator1 = head1.iterator();
        Iterator iterator2 = head2.iterator();
        HashMap map = new HashMap();
        while (iterator1.hasNext()){
            map.put(iterator1.next(), 1);
        }
        while (iterator2.hasNext()){
            int rem = x-(int)iterator2.next();
            if (map.containsKey(rem))
                count++;
        }
        return count;
    }

    private static int countPairsSorted(LinkedList<Integer> head1, LinkedList<Integer> head2, int x) {
        int count=0;
        Collections.sort(head1);
        Collections.sort(head2, Collections.reverseOrder());
        int i=0, j=0;
        while (i< head1.size() && j < head2.size()){
            if (head1.get(i)+head2.get(j)==x){
                count++;i++;j++;
            }else if (head1.get(i)+head2.get(j) < x)
                i++;
            else
                j++;
        }
        return count;
    }

    private static int countPairs(LinkedList<Integer> head1, LinkedList<Integer> head2, int x) {
        int count =0;
        Iterator iterator1 = head1.iterator();

        while (iterator1.hasNext()){
            int i = (int) iterator1.next();
            Iterator iterator2 = head2.iterator();
            while (iterator2.hasNext()){
                int j = (int) iterator2.next();
                if (i+j==x)
                    count++;
            }
        }
        return count;
    }
}
