package com.apple;

public class SubArraySumNN {
    public static void main(String[] args) {
        int[] arr = {1, 4, 20, 3, 10, 5, 33};
        int sum = 33;
        subarraySum(arr, sum);

    }

    private static void subarraySum(int[] arr, int sum) {
        int csum = 0, s=0, l=0;
        for (int i=0;i<arr.length;i++){
            csum = arr[i];
            s=i;
            l=i;
            for (int j=i+1; j< arr.length;j++){
                if (csum==sum){
                    l=j-1;
                    break;
                }
                csum = csum +arr[j];

            }
            if (csum==sum){
                System.out.println("s "+s+" l "+l);
            }
        }

    }
}
