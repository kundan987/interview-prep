package com.apple;

import java.util.Arrays;

public class Array3 {

    public static void main(String[] args) {
        int ar1[] = {1, 5, 10, 20, 40, 80};
        int ar2[] = {6, 7, 20, 80, 100};
        int ar3[] = {3, 4, 15, 20, 30, 70, 80, 120};

        System.out.print("Common elements are ");
        findCommon(ar1, ar2, ar3);
        System.out.println("Count = "+findSum(ar1,ar2,30));
    }

    private static int findSum(int[] ar1, int[] ar2, int x) {
        int count=0;
        Arrays.sort(ar1);
        Arrays.sort(ar2);
        int i=0, j=ar2.length-1;
        while (i< ar1.length && j>= 0){
            if (ar1[i]+ar2[j]==x){
                count++;
                i++;
                j--;
            }else if (ar1[i]+ar2[j]< x)
                i++;
            else
                j--;

        }
        return count;
    }

    private static void findCommon(int[] a, int[] b, int[] c) {
        int i=0,j=0,k=0;
        while(i< a.length && j < b.length && k < c.length){
            if (a[i] == b[j] && b[j] == c[k])
                System.out.println(a[i]);
            if (a[i] < b[j])
                i++;
            if (b[j] < c[k])
                j++;
            else
                k++;
        }
    }
}
