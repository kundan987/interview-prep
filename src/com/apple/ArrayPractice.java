package com.apple;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ArrayPractice {

    public static void main(String[] args) {
        int[] a = {1, 2, 3, -2, 5};
        int[] c = {1, 2, 3, 5};
        int[] b = {-1,-2,-3,-4};

        int arr1[] = {1, 5, 9, 10, 15, 20};
        int arr2[] = {2, 3, 8, 13};
        int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int array[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110};

        System.out.println("Max sum of contigous array : "+maxSum1(a));
        System.out.println("Max sum of contigous array : "+maxSum(b));
        System.out.println("Missing number in an array : "+missingNumber(c, 5));
        System.out.println("Merge sorted array : "+mergeSortedArray(arr1, arr2));
        System.out.println("Merge sorted array : "+mergeSortedArrayMap(arr1, arr2));
        int n = array.length;

        System.out.println("Original Array");
        for (int i = 0; i < n; i++)
            System.out.print(array[i] + " ");

        rearrange(array, n);

        System.out.print("\nModified Array\n");
        for (int i = 0; i < n; i++)
            System.out.print(array[i] + " ");
    }

    public static void rearrange(int arr[], int n)
    {
        // initialize index of first minimum and first
        // maximum element
        int max_idx = n - 1, min_idx = 0;

        // store maximum element of array
        int max_elem = arr[n - 1] + 1;

        // traverse array elements
        for (int i = 0; i < n; i++) {
            // at even index : we have to put
            // maximum element
            if (i % 2 == 0) {
                arr[i] += (arr[max_idx] % max_elem) * max_elem;
                max_idx--;
            }

            // at odd index : we have to put minimum element
            else {
                arr[i] += (arr[min_idx] % max_elem) * max_elem;
                min_idx++;
            }
        }

        // array elements back to it's original form
        for (int i = 0; i < n; i++)
            arr[i] = arr[i];
    }

    private static boolean mergeSortedArray(int[] arr1, int[] arr2) {
        int i =0, j=0,k=0;
        int[] res=new int[arr1.length+arr2.length];
        while (i<arr1.length && j<arr2.length){
            if (arr1[i] < arr2[j]){
                res[k++] =arr1[i++];
            }else {
                res[k++] =arr2[j++];
            }
        }
        while (i < arr1.length)
            res[k++] = arr1[i++];

        while (j < arr2.length)
            res[k++] = arr2[j++];

        System.out.println(Arrays.toString(res));
       // System.out.println(Arrays.toString(arr2));
        return true;
    }

    private static boolean mergeSortedArrayMap(int[] arr1, int[] arr2) {
        int i =0, j=0;
        Map<Integer, Boolean> map = new HashMap<>();
        while (i < arr1.length)
            map.put(arr1[i++], true);

        while (j < arr2.length)
            map.put(arr2[j++], true);

        System.out.println(map.keySet());
        // System.out.println(Arrays.toString(arr2));
        return true;
    }

    private static int missingNumber(int[] a, int n) {
        int sumOfNNumber = (n*(n+1))/2;
        int sumOfArray =0;
        for (int i=0; i< a.length ; i++)
            sumOfArray+=a[i];

        return sumOfNNumber-sumOfArray;

    }

    private static int maxSum(int[] a) {
        int maxHere=0 , maxSoFar=Integer.MIN_VALUE;

        for (int i=0; i< a.length ; i++){
            maxHere = Math.max(a[i], maxHere+a[i]);
            maxSoFar = Math.max(maxHere, maxSoFar);
        }
        return maxSoFar;
    }

    private static int maxSum1(int[] a) {
        int maxHere=0 , maxSoFar=Integer.MIN_VALUE;

        for (int i=0; i< a.length ; i++){
            maxHere = maxHere+a[i];
            if (maxHere < 0)
                maxHere=0;
            if (maxSoFar < maxHere)
                maxSoFar = maxHere;
        }
        return maxSoFar;
    }
}
