package com.apple;

import java.util.Stack;

public class NGE {

    public static void main(String[] args) {
        int[] a = {11, 13, 21, 3 };
        int[] b = {10, 5, 11, 6, 20, 12};
       // nge(a);

       // usingStack(a);

        cge(b);
    }

    private static void cge(int[] a) {
        for (int i=0; i<a.length;i++) {
            boolean found = false;
            int value = -1;
            for (int j = 0; j < a.length; j++) {
                if (!found && a[j] > a[i]) {
                    found = true;
                    value = a[j];
                    if (found && value > a[j])
                        value = a[j];
                }
            }
            System.out.println(value);
        }
    }

    private static void nge(int[] a) {
        boolean flag = false;

        for (int i=0; i<a.length;i++){
            for (int j=i+1; j< a.length;j++){
                if (a[j]>a[i]){
                    System.out.println(a[i]+" -> " + a[j]);
                    flag=true;
                    break;
                }
            }
            if (!flag)
                System.out.println(a[i]+" -> -1");
            flag=false;
        }
    }

    private static void usingStack(int[] a) {
        System.out.println("stack method calling");
        Stack<Integer> stack = new Stack();
        stack.push(a[0]);
        int i=1;
        while(!stack.isEmpty() && i<a.length){
            if (a[i] > stack.peek()){
                while (!stack.isEmpty())
                    System.out.println(stack.pop() +" -> " + a[i]);

            }
            stack.push(a[i]);
            i++;
        }
        while (!stack.isEmpty())
            System.out.println(stack.pop() +" -> " + -1);
    }
}
