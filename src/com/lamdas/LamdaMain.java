package com.lamdas;

import java.util.*;
import java.util.function.*;

public class LamdaMain {

    public static void main(String[] args) {


        Employee jhon = new Employee("John Doe", 21);
        Employee kundan = new Employee("Kundan Mishra", 24);
        Employee jack = new Employee("Jack Dickson", 22);
        Employee snow = new Employee("Snow Mike", 32);
        Employee shraddha = new Employee("Shradha Ahlawat", 31);
        Employee prashuk = new Employee("Prashuk Ajmera", 29);
        Employee nehal = new Employee("Nehal Gupta", 27);

        List<Employee> employees = new ArrayList<>();
        employees.add(jhon);
        employees.add(kundan);
        employees.add(jack);
        employees.add(snow);
        employees.add(shraddha);
        employees.add(prashuk);
        employees.add(nehal);

      //  printEmployeeByAge(employees, "Employees over 30", employee -> employee.getAge() > 30);
       // printEmployeeByAge(employees, "Employees 30 and under", employee -> employee.getAge() <=30);

        Function<Employee, String> getFirstName = employee -> {
            return employee.getName().substring(0, employee.getName().indexOf(' '));
        };

        Function<Employee, String> getLastName = employee -> {
            return employee.getName().substring(employee.getName().indexOf(' ')+1);
        };

        for (Employee employee : employees){
         //   System.out.println(getFirstName.apply(employee));
        }

        Random random = new Random();
        for (Employee employee : employees){
            if (random.nextBoolean()){
                System.out.println("First Name : "+getAName(getFirstName, employee));
            }else{
                System.out.println("Last Name : "+ getAName(getLastName, employee));
            }
        }

        IntPredicate intPredicate = value -> value> 15;
        IntPredicate intPredicate1 = value -> value<=100;

        System.out.println(intPredicate.and(intPredicate1).test(50));

        Supplier<Integer> supplier = () -> random.nextInt(1000);
        for (int i = 0; i < 10; i++) {
            System.out.println(supplier.get());
        }

        System.out.println("Supplier " + supplier.get());

       /* System.out.println("====Employee over 30 ========");
        employees.forEach(employee -> {
            if (employee.getAge()>30)
             System.out.println(employee.getName());
        });

        System.out.println("=====Employee below and equal 30======");
        employees.forEach(employee -> {
            if (employee.getAge()<=30)
                System.out.println(employee.getName());
        });*/

       Function<Employee, String> upperCase = employee -> employee.getName().toUpperCase();
       Function<String, String> firstName = name -> name.substring(0, name.indexOf(' '));
       Function chainedFunction = upperCase.andThen(firstName);
       System.out.println("Chained Function : " +chainedFunction.apply(employees.get(0)));

        BiFunction<String, Employee, String> concateAge = (name, employee) -> name.concat(" "+employee.getAge());

        String upperName = upperCase.apply(employees.get(0));
        System.out.println(concateAge.apply(upperName,employees.get(0)));

        IntUnaryOperator incBy5 = operand -> operand+5;
        System.out.println(incBy5.applyAsInt(10));

        IntBinaryOperator sum = (left, right) -> (int) Math.pow(left,right);
        System.out.println(sum.applyAsInt(2,5));

        Consumer<String> c1 = s -> s.toUpperCase();
        Consumer<String> c2 = s -> System.out.println(s);
        c1.andThen(c2).accept("Kundan Mishra");

    }

    private static String getAName(Function<Employee, String> getName , Employee employee){
        return getName.apply(employee);
    }

    public static void printEmployeeByAge(List<Employee> employees, String ageText, Predicate<Employee> ageCondition){

        System.out.println(ageText);
        System.out.println("================");
        for(Employee employee : employees){
            if (ageCondition.test(employee))
                System.out.println(employee.getName());
        }

    }
}

