package com.lamdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Streams {
    public static void main(String[] args) {

        List<String> someBingoNumbers = Arrays.asList(
                "N40", "N26",
                "B12", "B6",
                "G53", "G49", "G60", "G50","g64",
                "I26", "I17", "I29",
                "O71");

        /*List<String> gNumbers = new ArrayList<>();

        someBingoNumbers.forEach(number -> {
            if (number.toUpperCase().startsWith("G")){
                gNumbers.add(number);
               // System.out.println(number);
            }

        });

        gNumbers.sort((s1, s2) -> s1.compareTo(s2) );
        gNumbers.forEach(s -> System.out.println(s));*/

        someBingoNumbers
                .stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("G"))
                .sorted()
                .forEach(s -> System.out.println(s));
        System.out.println("=============");
        someBingoNumbers.stream().forEach(System.out::println);


        Stream<String> ioNumber = Stream.of("I26", "I17", "I29", "O71");
        Stream<String> inNumber = Stream.of("N40", "N26","O71", "I26", "I17", "I29" );
        Stream<String> concatStream = Stream.concat(ioNumber, inNumber);
        System.out.println("-----------------------");
        System.out.println(concatStream.distinct().peek(System.out::println).count());

    }
}
