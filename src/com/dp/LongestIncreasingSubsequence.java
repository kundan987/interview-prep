package com.dp;

public class LongestIncreasingSubsequence {

    public static void main(String[] args) {
        int[] a = {3,4,-1,7,6,2,0};

        System.out.println("Length : " + longestIncreasingSubsequence(a));
    }

    private static int longestIncreasingSubsequence(int[] a) {

        int l = a.length;
        int[] result = new int[l];
        for (int i =0; i<l;i++)
            result[i] = 1;

        for (int i=1; i<l;i++){
            int j=0;
            while(j<i){
                if (a[j]<a[i])
                    result[i]=Math.max(result[i],result[j]+1);
                j++;
            }
        }
        int max =0;
        for (int i =0; i<l;i++){
            System.out.print(result[i] + ", ");
            if (result[i]>max)
                max = result[i];
        }

        return max;
    }
}
