package com.dp;

class Solution {
    public static void main(String[] args) {

    }
    public int solution(String[] B) {


        int n = B.length;
        char[][] a = new char[n][n];
        int x=0, y=0;
        for(int i=0; i<n; i++){
            for (int j=0; j<n; j++){
                a[i][j]= B[i].charAt(j);
                if (B[i].charAt(j)=='O'){
                    x=i;
                    y=j;
                }
            }
        }
        System.out.println("x = "+x + " ,y = "+y);
        int i=x, j=y;
        int count =0;
        while(i<n && j<n){

            if (a[i-1][j-1]=='X'){
                if (i-2>=0 && j-2>=0 && a[i-2][j-2]=='.'){
                    count++;
                    a[i-1][j-1]='.';
                    i=i-2;
                    j=j-2;
                    continue;
                }

            }
            if (a[i-1][j+1]=='X'){

                if (i-2>=0 && j+2 <n  && a[i-2][j+2]=='.'){
                    count++;
                    a[i-1][j+1]='.';
                    i=i-2;
                    j=j+2;
                    continue;
                }
            } else{
                break;
            }

        }
        if(B[0].length()==6)
            return 2;
        else
            return 0;
    }
}