package com.dp;

public class CoinProblem {
    public static void main(String[] args) {
       int[] coins = {1,2,3};

        System.out.println(noOfWays(coins, 5));
    }

    private static int noOfWays(int[] coins, int n) {

        int[][] T = new int[coins.length+1][n+1];
        for (int i=0; i<=coins.length;i++)
            T[i][0]=1;

        for (int i=1; i<= coins.length; i++){
            for (int j=1; j<= n; j++){
                if (j>=coins[i-1])
                    T[i][j] = T[i-1][j]+T[i][j-coins[i-1]];
                else
                    T[i][j] = T[i-1][j];
            }
        }

        return T[coins.length][n];
    }
}
