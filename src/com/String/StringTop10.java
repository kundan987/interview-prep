package com.String;

import java.util.regex.Pattern;
import java.util.stream.Stream;

public class StringTop10 {

    public static void main(String[] args) {
        String s = "i like this program very much";
        System.out.println(reverseWordsRegex(s));
    }

    static String reverseWordsRegex(String str)
    {

        Pattern pattern = Pattern.compile("\\s");

        String[] temp = pattern.split(str);
        String result = "";

        // Iterate over the temp array and store
        // the string in reverse order.
        for (int i = 0; i < temp.length; i++) {
            if (i == temp.length - 1)
                result = temp[i] + result;
            else
                result = " " + temp[i] + result;
        }
        return result;
    }

    private static String reverseWords(String s) {

       String[] words = s.split(" ");
        String result = "";
        for (int i = 0; i < words.length; i++) {
            if (i == words.length - 1)
                result = words[i] + result;
            else
                result = " " + words[i] + result;
        }

//        for (int i=0 ; i<words.length;i++){
////            words[i] = reverseWord(words[i]);
////            result = result +words[i]+" ";
////       }
////
////        result = result.substring(0,result.length()-1);
////
////       return reverseWord(result);

        return result;
    }

    private static String reverseWord(String wordString){
        char[] word = wordString.toCharArray();
        int start =0, end = word.length-1;

        while(start<end){
            char c = word[start];
            word[start]= word[end];
            word[end]=c;
            start++;
            end--;
        }
        return String.valueOf(word);
    }
}
