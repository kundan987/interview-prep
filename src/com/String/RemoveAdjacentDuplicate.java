package com.String;

public class RemoveAdjacentDuplicate {

    public static void main(String[] args) {
        String s = "azxxzy";
        s = remove(s);

        System.out.println(s);
    }

    private static String remove(String s) {
        int l = s.length();
        String t="";
        int i=0,j=0;
        while(i<l && j<l){
            if (s.charAt(i)==s.charAt(j)){
                j++;
            }else{
                if (i!=j){
                    t = s.substring(0,i-1)+s.substring(j+1, l-1);
                    s=t;
                    l=s.length();
                }
                i++;
                j++;



            }

        }
        return s;
    }
}
