package com.array;

import java.util.Arrays;

public class Leet {

    public static void main(String[] args) {
        int[] a = {0,0,1};
        String s = "";
        Solution solution = new Solution();
      //  solution.moveZeroes(a);
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(a));
    }
}

class Solution {
    public void moveZeroes(int[] nums) {
        int n = nums.length;
        int i=0;
        while(i<n){
            if(nums[i]==0){
                int j=i;
                while(j<n-1){
                    nums[j]=nums[j+1];
                    j++;
                }
                nums[j]=0;
               // i--;
            }else
                i++;
        }
    }
}
