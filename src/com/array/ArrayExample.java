package com.array;

public class ArrayExample {

    public static void main(String[] args) {
        int[] a = {1,2,3,4,5,6,7};
       // rotateArray(a,2,a.length);
      //  rotateArrayOpt(a,2,a.length);
       // jugglingArray(a,2,a.length);
       // reversalArray(a,3,a.length);
      //  leftRotate(a,3);
      //  rotateCyclic(a,a.length);
        pivotedBinarySearch(a,a.length,3);
        printArray(a);
    }


    private static void pivotedBinarySearch(int[] a, int n, int key) {
    }

    static int binarySearch(int[] a, int left, int right, int key){
        if (right< left)
            return -1;
        int mid = (left+right)/2;
        if (a[mid]==key)
            return mid;

        if (a[mid]> key )
            return binarySearch(a,left, mid-1, key);
        return binarySearch(a,mid+1, right,key);
    }


    static void rotateCyclic(int[] a, int n){
        int temp = a[n-1];
        for (int i=n-1; i>0; i--){
            a[i]=a[i-1];
        }
        a[0]=temp;
    }

    static void reversalArray(int[] a, int d, int n){

        int i=0,j=d, k=0,s=n;
        while(i<d){
            int temp = a[i];
            a[i] = a[d-1];
            a[d-1] = temp;
            i++;
            d--;
        }
        printArray(a);
        while(j<n){
            int temp = a[j];
            a[j]= a[n-1];
            a[n-1]=temp;
            j++;
            n--;
        }
        printArray(a);
        while(k<s){
            int temp = a[k];
            a[k]= a[s-1];
            a[s-1]=temp;
            k++;
            s--;
        }
    }

    static void leftRotate(int arr[], int d)
    {
        int n = arr.length;
        rvereseArray(arr, 0, d-1);
        rvereseArray(arr, d, n-1);
        rvereseArray(arr, 0, n-1);
    }

    static void rvereseArray(int arr[], int start, int end)
    {
        int temp;
        while (start < end)
        {
            temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }
    }

    static void jugglingArray(int[] a, int d, int n){
        int temp,j=0,k=0,i=0;
        for (i=0; i < gcd(d,n);i++){
            temp=a[i];
            j=i;
            while (true){
                k=j+d;
                if (k>=n)
                    k=k-n;
                if (k==i)
                    break;

                a[j]=a[k];
                j=k;
            }
            a[j]=temp;
        }
    }

    private static int gcd(int a, int b) {
        if (b==0)
            return a;
        else
            return gcd(b,a%b);
    }

    static void rotateArray(int[] a, int d, int n) {
        int[] temp = new int[d];
        int j=0;
        for (int i=0; i< d;i++)
            temp[i]=a[i];
        for (int i=0;i<n-d;i++)
            a[i]=a[i+d];
        for (int i=n-d;i<n;i++)
            a[i]=temp[j++];
    }

    static void rotateArrayOpt(int[] a, int d, int n){
        for (int i=0; i< d;i++)
            rotateArrayOneByOne(a,n);

    }

    static void rotateArrayOneByOne(int[] a, int n) {
        int temp =a[0];
        for (int i=0;i<n-1;i++)
            a[i]=a[i+1];
        a[n-1]=temp;
    }

    static void printArray(int[] a){
        for (int i=0; i< a.length;i++)
            System.out.print(a[i]+" ");
        System.out.println();
    }
}
