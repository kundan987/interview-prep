package com.array;

public class UrbanClap {

    public static void main(String[] args) {
        int sum =0;
        System.out.println("Total sum : "+fun(2));
     //   System.out.println(sum);
    }

    private static int fun(int i) {
        if(i<=1)
            return 1;
        int data = fun(i/2) + fun(i/3)+fun(i/4);
        return i > data ? i : data;
    }
}
