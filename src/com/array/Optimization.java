package com.array;

public class Optimization {
    public static void main(String[] args) {
        int [] a = {-2, -3, 4, -1, -2, 1, 5, -3};
        System.out.println("Maximum contiguous sum is " + maxSubArraySum(a));

        int max_sum = maxSubArrayProduct(a, a.length);
        System.out.println("Maximum contiguous product is " + max_sum);

        int  price[] = {10, 22, 5, 75, 65, 80};
        System.out.println("Max profit : "+ maxProfit(price));
    }

    private static int maxProfit(int[] price) {
        int l = price.length;

        int max_profit=0, diff=0;
        boolean buy=true;
        boolean sell=false;
        int count=0;

        for (int i=0; i< l-1; i++){
            if (buy){
                if (price[i]<price[i+1]){
                    diff=price[i];
                    buy=false;
                    sell=true;
                    count++;
                }
            }

            if (sell){
                if (price[i]>price[i+1]){
                    diff=price[i]-diff;
                    max_profit=max_profit+diff;
                    buy=true;
                    sell=false;
                }
            }

        }

        return max_profit;
    }

    static int maxSubArrayProduct(int a[], int size)
    {
        int max_so_far = a[0];
        int curr_max = a[0];

        for (int i = 1; i < size; i++)
        {
            curr_max = Math.max(a[i], curr_max*a[i]);
            max_so_far = Math.max(max_so_far, curr_max);
        }
        return max_so_far;
    }

    private static int maxSubArraySum(int[] a) {

        int len = a.length;
        int max_so_far=Integer.MIN_VALUE;
        int max_ending_here =0;
        int start=0,end=0,s=0;
        for (int i=0;i<len;i++){
            max_ending_here=max_ending_here+a[i];
            if (max_so_far<max_ending_here){
                max_so_far=max_ending_here;
                start=s;
                end=i;
            }
            if (max_ending_here<0){
                max_ending_here=0;
                s=i+1;
            }
        }

        return max_so_far;
    }
}
