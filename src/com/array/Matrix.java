package com.array;

import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Math.max;

public class Matrix {

    public static int N = 4, M = 6;

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> a = new ArrayList<>();

        int[][] arr =  {{1,2,3,4},
                        {5,6,7,8},
                        {9,10,11,12}};

        int[][] result = new int[arr[0].length][arr.length];

        for (int r = 0; r< arr.length; r++){
            for (int c =0; c<arr[0].length; c++){
                result[c][r] = arr[arr.length-r-1][c];
            }
        }

        for (int r = 0; r< result.length; r++){
            for (int c =0; c<result[0].length; c++){
         //       System.out.print(result[r][c]+" ");
            }
            System.out.println();
        }

        int mat[][] =
                {
                        {1, 2, 3, 4},
                        {5, 6, 7, 8},
                        {9, 10, 11, 12},
                        {13, 14, 15, 16}
                };

       // rotateMatrixAC(4,mat);
       // displayMatrix(4,mat);

        int matrix[][] = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

      //  rotateMatrixC(4,mat);
      //  displayMatrix(4,mat);

        int mat2[][] = { { 10, 10, 2, 0, 20, 4 },
                         { 1, 50, 0, 30, 2, 5 },
                        { 0, 10, 4, 0, 2, 0 },
                        { 1, 0, 2, 20, 0, 400 }
        };

        System.out.println(findMaxPath(mat2));



    }

    static int findMaxPath(int mat[][])
    {
        // To find max val in first row
        int res = -1;
        for (int i = 0; i < M; i++)
            res = max(res, mat[0][i]);
        System.out.println("Max in 1st row : "+res);

        for (int i = 1; i < N; i++)
        {
            res = -1;
            for (int j = 0; j < M; j++)
            {
                // When all paths are possible
                if (j > 0 && j < M - 1)
                    mat[i][j] += max(mat[i - 1][j],
                            max(mat[i - 1][j - 1],
                                    mat[i - 1][j + 1]));

                    // When diagonal right is not possible
                else if (j > 0)
                    mat[i][j] += max(mat[i - 1][j],
                            mat[i - 1][j - 1]);

                    // When diagonal left is not possible
                else if (j < M - 1)
                    mat[i][j] += max(mat[i - 1][j],
                            mat[i - 1][j + 1]);

                // Store max path sum
                res = max(mat[i][j], res);
            }
        }
        return res;
    }


    static void rotateMatrixAC(int N, int mat[][]){
        for (int x = 0; x< N/2;x++){
            for (int y = x; y<N-x-1; y++){

                int temp =mat[x][y];
                mat[x][y] = mat[y][N-x-1];
                mat[y][N-x-1] = mat[N-x-1][N-y-1];
                mat[N-x-1][N-y-1] = mat[N-y-1][x];
                mat[N-y-1][x] = temp;

            }
        }

    }

    static void rotateMatrixC(int N, int mat[][]){
        for (int x = 0; x< N/2;x++){
            for (int y = x; y<N-x-1; y++){

                int temp =mat[x][y];
                mat[x][y] = mat[N-y-1][x];
                mat[N-y-1][x] = mat[N-x-1][N-y-1];
                mat[N-x-1][N-y-1] = mat[y][N-x-1];
                mat[y][N-x-1] = temp;

            }
        }

    }

    static void displayMatrix(int N, int mat[][])
    {
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                System.out.print(" " + mat[i][j]);

            System.out.print("\n");
        }
        System.out.print("\n");
    }
}
