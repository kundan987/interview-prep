package com.array;

import java.util.ArrayList;
import java.util.List;

public class Test {

    //Input n will be an integer.You have to write a production code which returns sum of first n natural numbers

    public static void main(String[] args) {
        List<Integer> result = new ArrayList<>();
        result = convertIntoArray(12);
        for (Integer num : result)
            System.out.println(num);

        for(int i=11;i>0;i++) {
            result = addToList(result, convertIntoArray(i));

        }
        for (Integer num : result)
            System.out.println(num);
    }

        static List<Integer> addToList(List<Integer> result, List<Integer> list) {
            int i = 0;
            for (Integer digit : list) {

                int newDigit = result.get(i) + digit;
                if (newDigit > 9)
                    result.set(i + 1, result.get(i + 1) + newDigit%10);
                else
                    result.set(i, newDigit);
                i++;

            }
            return result;
        }

            static List<Integer> convertIntoArray(int m){
                List<Integer> list = new ArrayList<>();
                int r;
                while(m>0){
                    r=m%10;
                    m=m/10;
                    list.add(r);

                }
                return list;
            }

}
