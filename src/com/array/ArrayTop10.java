package com.array;

import java.util.*;
import java.util.stream.Collectors;

public class ArrayTop10 {

    public static void main(String[] args) {
        int[] a = {-1, -2, 2, -4};//{-2, -3, 4, -1, -2, 1, 5, -3};

       // System.out.println("largestSumContiguousSubArray : " + largestSumContiguousSubArray(a));

        int[] b = {1,2,3,4,6,7,8};
      //  System.out.println("Missing number : "+ missingNumber(b,8));

        int[] c = {5, 2, 4, 8, 9, 5, 10, 23};
       // generateAllSubArrays(c,33);
       // subArraySum(c,c.length,24);
        int[] d = {10, 2, -2, -20, 10};
       // subArraySumModified(d,d.length,-10);

        int[] e = {0,1,2,0,0,2,2,1,0,1};

       // sort012(e);

        ArrayList<String> list = new ArrayList<>();
        list.add("54");
        list.add("546");
        list.add("548");
        list.add("60");
        list.add("0");
        list.add("00");
      //  printLargest(list);

        int arr[] = {2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};
        sortElementsByFrequency(arr);



    }

    private static void sortElementsByFrequency(int[] arr) {

        HashMap<Integer,Integer> map = new HashMap<>();
        for(int a : arr){
            if (map.containsKey(a)){
                map.put(a,map.get(a)+1);
            }else{
                map.put(a, 1);
            }
        }


      //  Map<Integer, Integer> sortedMap = sortHashMapByValues(map);

        Map<Integer, Integer> sortedMap =
                map.entrySet().stream()
                        .sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                        .collect(Collectors.toMap(o -> o.getKey(), o -> o.getKey(),
                                (e1, e2) -> e1, () -> new LinkedHashMap<>()));

       map.entrySet().stream().sorted(Map.Entry.<Integer, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (integer, integer2) -> integer, LinkedHashMap::new));

      //  Map<Integer, Integer> sortedMap = sortByComparator(map,false);
       printMap(sortedMap);


       // Collections.sort(map);
    }

    private static Map<Integer, Integer> sortByComparator(Map<Integer, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<Integer, Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, (o1, o2) -> {
            if (order)
            {
                return o1.getValue().compareTo(o2.getValue());
            }
            else
            {
                return o2.getValue().compareTo(o1.getValue());

            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap<Integer, Integer> sortedMap =
                new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }

    public static void printMap(Map<Integer, Integer> map)
    {
        for (Map.Entry<Integer, Integer> entry : map.entrySet())
        {
            int val = entry.getValue();
            int key = entry.getKey();
            while(val>0){
                System.out.print(key +" ");
                val--;
            }

           // System.out.println("Key : " + entry.getKey() + " Value : "+ entry.getValue());
        }
    }

    private static void printArray(int arr[], int arr_size)
    {
        int i;
        for (i = 0; i < arr_size; i++)
            System.out.print(arr[i]+" ");
        System.out.println("");
    }

    private static void printLargest(ArrayList<String> list) {

        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String X, String Y) {
                String XY = X+Y;
                String YX = Y+X;
                return XY.compareTo(YX)>0? -1:1;
            }
        });

        Iterator it = list.iterator();

        while(it.hasNext())
            System.out.println(it.next());

    }

    private static void sort012(int[] a) {

        int lo = 0;
        int hi = a.length-1;
        int mid =0, temp=0;

        while(mid<hi){


            switch (a[mid]){
                case 0: {
                    temp = a[lo];
                    a[lo] = a[mid];
                    a[mid] = temp;
                    lo++;
                    mid++;
                    break;
                }
                case  1: {
                    mid++;
                    break;
                }
                case  2: {
                    temp = a[hi];
                    a[hi] = a[mid];
                    a[mid]=temp;
                    hi--;
                    break;
                }
            }
        }

        printArray(a,a.length);

    }


    private static void subArraySumModified(int[] a, int n, int sum) {

        HashMap<Integer, Integer> map = new HashMap<>();
        int curSum = 0,start=0;

        for (int i=0; i<n; i++){
            curSum+=a[i];

            if (curSum==sum){
                System.out.println("Sum == found between index "+ start+ " and "+i);
                return;
            }

            if (map.containsKey(curSum-sum)){
                System.out.println("Sum found between index "+ map.get(curSum-sum)+ " and "+i);
                return;
            }else{
                map.put(curSum-sum, i);
            }
        }

        System.out.println("No sub array with given sum exists");
    }

    private static int subArraySum(int[] a, int n, int sum) {

        int curSum = a[0], start =0;

        for (int i=1; i<n; i++){

            if (curSum==sum){
                System.out.println("Sum found between index "+ start+ " and "+(i-1));
                return 1;
            }

            if (curSum<sum)
                curSum+=a[i];

            while(curSum> sum && start<i){
                curSum-=a[start];
                start++;
            }
        }
        return 0;
    }

    private static int generateAllSubArrays(int[] a, int x) {
        int n = a.length;
        String sa;
        int sum;
        for (int i=0; i< n; i++){
            sa = "";
            sum=0;
            for (int j=i; j<n;j++){
                sum+=a[j];

                sa = sa+a[j];
                System.out.println(sa);
                if (sum==x){
                    System.out.println("mil gaya index between : " +i+" and "+j );
                    //return 1;
                }
            }
        }
        return 0;

    }

    private static int missingNumber(int[] b, int n) {

        int sum = n*(n+1)/2;

        for (int i=0; i< b.length; i++)
            sum = sum - b[i];
        return sum;
    }

    private static int largestSumContiguousSubArray(int[] a) {
        int maxSoFar=Integer.MIN_VALUE, maxEndingHere=0;
        for (int i=0; i<a.length;i++){
            maxEndingHere=maxEndingHere+a[i];
            if (maxEndingHere<0)
                maxEndingHere=0;
            if (maxSoFar<maxEndingHere)
                maxSoFar=maxEndingHere;
        }

        return maxSoFar;
    }


}
