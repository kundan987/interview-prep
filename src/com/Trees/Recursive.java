package com.Trees;

import java.util.ArrayList;
import java.util.List;

public class Recursive {
    Node root = null;

    public static void main(String[] args) {
        Recursive recursive = new Recursive();
        recursive.root = new Node(10);
        recursive.root.left = new Node(5);
        recursive.root.right = new Node(15);
        recursive.root.left.left = new Node(2);
        recursive.root.left.right = new Node(8);
        recursive.root.right.left = new Node(12);
        recursive.root.right.right = new Node(17);

        System.out.println("is BST : "+ recursive.isBST(recursive.root, Integer.MIN_VALUE, Integer.MAX_VALUE));
        System.out.println("is same : "+ recursive.isSame(recursive.root,recursive.root));

        System.out.println("Size of Binary tree : "+recursive.size(recursive.root));
        System.out.println("Height of Binary tree : "+recursive.height(recursive.root));
        List<Integer> result = new ArrayList<>();
        System.out.println("Is Root to leaf sum of Binary tree is 37 : "+recursive.rootToLeafSum(recursive.root, 37, result));
        System.out.println(result);
        System.out.println("LCA is : "+recursive.lcaBST(recursive.root, recursive.root.right.left, recursive.root.left.left).data);

    }

    private Node lcaBST(Node root, Node n1, Node n2) {
        if (root.data > Math.max(n1.data,n2.data))
            return lcaBST(root.left, n1, n2);
        if (root.data < Math.min(n1.data,n2.data))
            return lcaBST(root.right, n1, n2);
        else
            return root;
    }

    private boolean rootToLeafSum(Node root, int sum, List<Integer> result) {
        if (root==null)
            return false;
        if (root.left== null && root.right == null){
            if (root.data==sum){
                result.add(root.data);
                return true;
            }else
                return false;
        }
        if (rootToLeafSum(root.left, sum-root.data,result)){
            result.add(root.data);
            return true;
        }
        if (rootToLeafSum(root.right, sum-root.data,result)){
            result.add(root.data);
            return true;
        }
        return false;
    }

    private int height(Node root) {
        if (root== null)
            return 0;
        int lheight = height(root.left);
        int rheight = height(root.right);
        return 1 + Math.max(lheight,rheight);
    }

    private int size(Node root) {
        if (root== null)
            return 0;

        int lsize = size(root.left);
        int rsize = size(root.right);

        return lsize+rsize+1;
    }

    private boolean isSame(Node n1, Node n2) {
        if (n1== null && n2== null)
            return true;
        if (n1==null || n2==null)
            return false;
        return n1.data==n2.data && isSame(n1.left, n2.left) && isSame(n1.right, n2.right);
    }

    private boolean isBST(Node root, int min, int max) {
        if (root == null)
            return true;
        if (root.data <= min || root.data > max)
            return false;
        return isBST(root.left, min, root.data) && isBST(root.right, root.data, max);
    }

}
