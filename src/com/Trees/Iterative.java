package com.Trees;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Iterative {

    Node root = null;

    public static void main(String[] args) {
        Iterative iterative = new Iterative();
        iterative.root = new Node(1);
        iterative.root.left = new Node(2);
        iterative.root.right = new Node(3);
        iterative.root.left.left = new Node(4);
        iterative.root.left.right = new Node(5);
        iterative.root.right.left = new Node(6);
        iterative.root.right.right = new Node(7);
        iterative.root.left.right.left = new Node(8);
        iterative.root.right.right.left = new Node(9);

       // iterative.iterativeInorder(iterative.root);
       iterative.iterativePostorder(iterative.root);
      //  iterative.iterativePreorder(iterative.root);

       // iterative.levelOrder(iterative.root);
       // iterative.levelOrderLevelByLevel(iterative.root);
      //  iterative.levelByLevelUsingQueueAndDelimiter(iterative.root);
      //  iterative.reverseLevelOrder(iterative.root);
       // iterative.spiralLevelOrder(iterative.root);
       // iterative.spiralUsingDeque(iterative.root);
       // iterative.leftView(iterative.root);
        System.out.println("LCA of BT is "+iterative.lcaBinaryTree(iterative.root, iterative.root.right.right.left, iterative.root.right).data);
        iterative.iterativePostorderUsingOneStack(iterative.root);


    }

    private void iterativePostorderUsingOneStack(Node root) {
        Node current = root;
        Stack<Node> s = new Stack<>();
        while (current!= null || !s.isEmpty()){
            if (current!= null){
                s.push(current);
                current=current.left;
            }else{
                Node temp = s.peek().right;
                if (temp== null){
                    temp = s.pop();
                    System.out.print(temp.data+" ");
                    while(temp!= null && temp==s.peek().right ){
                        temp = s.pop();
                        System.out.print(temp.data+" ");
                    }
                }else{
                    current= temp;
                }
            }
        }
    }

    private Node lcaBinaryTree(Node root, Node n1, Node n2) {

        if (root == null)
            return  null;
        if (root==n1 || root==n2)
            return root;
        Node left = lcaBinaryTree(root.left, n1, n2);
        Node right = lcaBinaryTree(root.right, n1, n2);
        if (left != null && right!= null)
            return root;
        if (left == null && right== null)
            return null;
        return left!=null? left:right;
    }

    private void leftView(Node root) {
        if (root==null) return;
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        q.add(null);
        boolean flag = true;
        while (!q.isEmpty()){
            root = q.poll();
            if (root==null){
                flag=true;
                if (!q.isEmpty())
                    q.add(null);
            }
            else{
                if (flag)
                    System.out.print(root.data+" ");
                flag=false;
                if (root.left!=null)
                    q.add(root.left);
                if (root.right!=null)
                    q.add(root.right);
            }
        }
    }

    private void spiralUsingDeque(Node root) {
        if (root==null) return;
        Deque<Node> q = new LinkedList<>();
        q.offer(null);
        q.offerFirst(root);

        while(q.size()>1){

            root = q.peekFirst();

            while (root!=null){
                root = q.pollFirst();
                System.out.print(root.data+" ");
                if (root.left!=null)
                    q.offerLast(root.left);
                if (root.right!=null)
                    q.offerLast(root.right);

                root = q.peekFirst();

            }

            root = q.peekLast();

            while (root!=null){
                root = q.pollLast();
                System.out.print(root.data+" ");
                if (root.right!=null)
                    q.offerFirst(root.right);
                if (root.left!=null)
                    q.offerFirst(root.left);

                root = q.peekLast();
            }

        }
    }

    private void spiralLevelOrder(Node root) {
        if (root==null) return;
        Stack<Node> s1 = new Stack<>();
        Stack<Node> s2 = new Stack<>();
        s1.push(root);
        while (!s1.isEmpty() || !s2.isEmpty()){

            while (!s1.isEmpty()){
                root = s1.pop();
                System.out.print(root.data+" ");
                if (root.left!=null)
                    s2.add(root.left);
                if (root.right!=null)
                    s2.add(root.right);
            }
            System.out.println();
            while (!s2.isEmpty()){
                root = s2.pop();
                System.out.print(root.data+" ");
                if (root.right!=null)
                    s1.add(root.right);
                if (root.left!=null)
                    s1.add(root.left);
            }
            System.out.println();
        }

    }

    private void reverseLevelOrder(Node root) {
        if (root==null) return;
        Stack<Node> s = new Stack<>();
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()){
            root = q.poll();
            s.push(root);
            if (root.right!=null)
                q.add(root.right);
            if (root.left!=null)
                q.add(root.left);
        }

        while (!s.isEmpty())
            System.out.print(s.pop().data+" ");

    }

    private void levelByLevelUsingQueueAndDelimiter(Node root) {
        if (root==null) return;
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        q.add(null);

        while (!q.isEmpty()){
            root = q.poll();
            if (root==null){
                System.out.println();
                if (!q.isEmpty())
                    q.add(null);
            }
            else{
                System.out.print(root.data+" ");
                if (root.left!=null)
                    q.add(root.left);
                if (root.right!=null)
                    q.add(root.right);
            }
        }
    }

    private void levelOrderLevelByLevel(Node root) {
        if (root==null) return;
        Queue<Node> q1 = new LinkedList<>();
        Queue<Node> q2 = new LinkedList<>();
        q1.add(root);
        while (!q1.isEmpty() || !q2.isEmpty()){

            while (!q1.isEmpty()){
                root = q1.poll();
                System.out.print(root.data+" ");
                if (root.left!=null)
                    q2.add(root.left);
                if (root.right!=null)
                    q2.add(root.right);

            }
            System.out.println();
            while (!q2.isEmpty()){
                root = q2.poll();
                System.out.print(root.data+" ");
                if (root.left!=null)
                    q1.add(root.left);
                if (root.right!=null)
                    q1.add(root.right);

            }
            System.out.println();

        }
    }

    private void levelOrder(Node root) {
        if (root==null) return;
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()){
            root=q.poll();
            System.out.println(root.data);
            if (root.left!=null)
                q.add(root.left);
            if (root.right!=null)
                q.add(root.right);
        }

    }

    private void iterativePostorder(Node root) {
        if (root==null) return;
        Stack<Node> s1 = new Stack<>();
        Stack<Node> s2 = new Stack<>();
        s1.push(root);
        while(!s1.isEmpty()){
            root=s1.pop();
            s2.push(root);
            if (root.left!= null)
                s1.push(root.left);
            if (root.right!= null)
                s1.push(root.right);
        }

        while (!s2.isEmpty())
            System.out.print(s2.pop().data+" ");
    }

    private void iterativePreorder(Node root) {
        if (root==null) return;
        Stack<Node> s = new Stack<>();
        s.push(root);
        while(!s.isEmpty()){
            root = s.pop();
            System.out.println(root.data);
            if (root.right != null)
                s.push(root.right);
            if (root.left != null)
                s.push(root.left);
        }

    }

    private void iterativeInorder(Node root) {
        if (root==null) return;
        Stack<Node> s = new Stack<>();
        while(true){
            if (root!= null){
                s.push(root);
                root = root.left;

            }else{
                if (s.isEmpty()) break;
                else{
                    root=s.pop();
                    System.out.println(root.data);
                    root=root.right;
                }
            }
        }
    }
}
