package com.LinkedList;

import java.util.LinkedList;

public class LinkedListInsertion {

    Node head;

    public static void main(String[] args) {

        LinkedListInsertion ll = new LinkedListInsertion();

        ll.head = new Node(3);
        ll.head.next = new Node(4);
        ll.head.next.next = new Node(5);

        ll.printList();
        ll.insertStart(1);
        ll.printList();
        ll.insertEnd(6);
        ll.printList();
        ll.insertAfter(ll.head.next, 2);
        ll.printList();
        ll.deleteKey(2);
        ll.printList();
        ll.deleteAtPosition(2);
        ll.printList();
        ll.lengthOfLinkedList();
        System.out.println("Length of linked list Recursive: " + ll.lengthOfLinkedListRecursive(ll.head));
        boolean flag = ll.searchElementInList(6);
        if (flag)
            System.out.println("Element is present in list");
        else
            System.out.println("Element is not present in list");

        flag = ll.searchElementInListRecursive(ll.head, 3);

        if (flag)
            System.out.println("Element is present in list recursive");
        else
            System.out.println("Element is not present in list recursive");

        System.out.println("Nth node in linked list : "+ll.nthNodeInList(2));

        System.out.println("Nth node from last in linked list : "+ll.nthNodeFromLastInList(2));

        System.out.println("Nth node from last in linked list using two pointer : "+ll.nthNodeFromLastInListUsingTwoPointer(3));

        System.out.println("Middle node in the list "+ ll.middleNodeInList());

        ll.head.next.next.next.next = ll.head;

        flag = ll.detectLoop();
        if (flag)
            System.out.println("Loop present");
        else
            System.out.println("Loop Not present");
        System.out.println("Length of loop "+lengthOfLoop(ll.head));
        ll.detectAndRemoveLoop();
        ll.printList();
        ll.insertEnd(6);
        ll.insertEnd(6);
        ll.insertEnd(8);
        ll.insertEnd(10);
        ll.insertEnd(10);
        ll.insertEnd(12);
        ll.printList();
        ll.removeDuplicatesSortedList();
        ll.printList();

    }

    private void removeDuplicatesSortedList() {
        Node curr = head;
        while(curr!=null && curr.next!=null){
            if (curr.data==curr.next.data){
                curr.next=curr.next.next;
            }else
                curr=curr.next;
        }

    }

    static private int lengthOfLoop(Node node) {
        Node fast=node;
        Node slow=node;
        Node ptr1 = null;
        int count =0;
        while(fast!=null && fast.next!=null){

            slow=slow.next;
            fast=fast.next.next;
            if (slow==fast){
                ptr1=slow;
                while(ptr1.next!= slow){
                    ptr1=ptr1.next;
                    count++;
                }
                return count+1;
            }

        }

        return 0;
    }

    private void detectAndRemoveLoop() {
        Node fast=head;
        Node slow=head;
        while(fast!=null && fast.next!=null){

            slow=slow.next;
            fast=fast.next.next;
            if (slow==fast){
                removeLoop(slow);
                return;
            }

        }
    }

    private void removeLoop(Node loop) {
        Node ptr1 = null;
        Node ptr2 = head;

        while(true){
            ptr1 = loop;
            while(ptr1.next!= loop && ptr1.next!=ptr2)
                ptr1=ptr1.next;

            if (ptr1.next==ptr2)
                break;
            ptr2=ptr2.next;
        }
        ptr1.next=null;

    }

    private boolean detectLoop() {
        Node fast=head;
        Node slow=head;
        while(fast!=null && fast.next!=null){

            slow=slow.next;
            fast=fast.next.next;
            if (fast==slow)
                return true;
        }
        return false;
    }

    private int middleNodeInList() {
        Node fast=head;
        Node slow=head;
        while(fast!=null && fast.next!=null){
            slow=slow.next;
            fast=fast.next.next;
        }

        return slow.data;
    }

    private int nthNodeFromLastInListUsingTwoPointer(int n) {

        Node ref = head;
        Node curr = head;
        int count =0;
        while(ref!=null && count!=n){
            ref=ref.next;
            count++;
        }
        while(curr!=null && ref!=null){
            curr = curr.next;
            ref=ref.next;
        }
        return curr.data;

    }

    private int nthNodeFromLastInList(int nth) {
        Node temp = head;
        int len =0;
        while(temp!= null){
            len++;
            temp=temp.next;
        }
        int count =0;
        Node curr = head;
        while(curr!= null && count!=len-nth){
            count++;
            curr=curr.next;
        }
        return curr.data;

    }

    private int nthNodeInList(int index) {
        Node curr = head;
        int count =0;
        while(curr != null && count!= index){
            curr= curr.next;
            count++;
        }
        if (curr==null)
            return 0;
        return curr.data;
    }

    private boolean searchElementInListRecursive(Node node, int key) {
        if (node == null)
            return false;
        if (node.data==key)
            return true;
        else
            return searchElementInListRecursive(node.next,key);
    }

    private boolean searchElementInList(int key) {
        if (head== null){
            System.out.println("List is empty");
            return false;
        }

        Node temp = head;
        while(temp!= null && temp.data != key)
            temp= temp.next;

        if (temp==null)
            return false;
        else
            return true;
    }

    private int lengthOfLinkedListRecursive(Node node) {

        if (node==null)
            return 0;
        else
         return 1+lengthOfLinkedListRecursive(node.next);
    }

    private void lengthOfLinkedList() {
        Node temp = head;
        int count =0;
        while(temp!=null){
            temp=temp.next;
            count++;
        }
        System.out.println("Length of linked list : "+count);
    }

    private void deleteAtPosition(int pos) {

        if (head== null){
            System.out.println("List is empty");
            return;
        }

        int count=0;
        Node temp = head;
        Node prev = null;
        while(temp!= null && count!= pos){
            prev = temp;
            temp=temp.next;
            count++;
        }
        if (temp==null){
            System.out.println("position is greater than length of list");
            return;
        }else{
            if (prev==null)
                head = temp.next;
            else
                prev.next = temp.next;
        }


    }

    private void deleteKey(int key) {
        if (head == null){
            System.out.println("List is null");
            return;
        }

        Node curr = head;
        Node prev = null;
        while(curr != null && curr.data!= key){
            prev = curr;
            curr = curr.next;
        }

        if (curr == null)
            System.out.println("Key not present");
        else{
            if (prev==null)
                head = curr.next;
            else
                prev.next = curr.next;
        }

    }

    private void insertAfter(Node prevNode, int key) {
        Node newNode = new Node(key);
        if (prevNode == null)
            System.out.println("prev node can't be null.");
        else{
            newNode.next = prevNode.next;
            prevNode.next = newNode;
        }

    }

    private void insertEnd(int key) {
        Node node = new Node(key);
        if (head == null )
            head=node;
        else{
            Node curr = head;
            while(curr.next!= null)
                curr = curr.next;
            curr.next = node;
        }
    }

    private void insertStart(int key) {

        Node node = new Node(key);

        if (head == null )
            head=node;

        else{
            node.next = head;
            head = node;
        }

    }

    private void printList() {

        Node node = head;

        while(node != null){
            System.out.print(node.data+" -> ");
            node = node.next;
        }
        System.out.println("null ");
    }
}
