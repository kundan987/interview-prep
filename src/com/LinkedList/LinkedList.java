package com.LinkedList;

public class LinkedList {

    Node head;

    public static void main(String[] args) {

        LinkedList ll = new LinkedList();

        ll.insertEnd(1);
        ll.insertEnd(3);
        ll.insertEnd(4);
        ll.insertEnd(3);
        ll.insertEnd(2);
        ll.insertEnd(1);
        ll.insertEnd(5);

        ll.printList();
        ll.removeDuplicateUnsorted();
        ll.printList();
        ll.swapNodes(1, 6);
        ll.printList();
        ll.pairWiseSwap();
        ll.printList();
      //  ll.reverse();
        ll.head = ll.reverse(ll.head, 3);
        ll.printList();

    }

    private void reverse() {
        Node prev = null;
        Node curr = head;
        Node next ;
        while(curr!= null){
            next=curr.next;
            curr.next=prev;
            prev=curr;
            curr=next;
        }
        System.out.println(head.data);
        head=prev;
        System.out.println(head.data);
    }

    Node reverse(Node head, int k)
    {
        Node current = head;
        Node next = null;
        Node prev = null;

        int count = 0;

        while (count < k && current != null)
        {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
            count++;
        }

        if (next != null)
            head.next = reverse(next, k);

        return prev;
    }

    void pairWiseSwap()
    {
        Node temp = head;

        while (temp != null && temp.next != null) {

            int k = temp.data;
            temp.data = temp.next.data;
            temp.next.data = k;
            temp = temp.next.next;
        }
    }

    private void swapNodes(int x, int y) {
        if (x==y)
            return;
        Node curr = head;
        Node t1 = null;
        Node t2 = null;
        Node xx = null;
        Node yy = null;
        while(curr!= null && curr.data!=x){

            if (curr.data==x){
                xx=curr;
                break;
            }
            t1=curr;
            curr=curr.next;
        }
        curr=head;
        while(curr!= null){

            if (curr.data==y){
                yy=curr;
                break;
            }
            t2=curr;
            curr=curr.next;
        }
     //   System.out.println("x = "+xx.data+", y = "+yy.data);
      //  System.out.println("t1 = "+t1.data+", t2 = "+t2.data);
        if (xx==null){
            System.out.println(x + " is not present");
            return;
        }
        if (yy==null){
            System.out.println(y + " is not present");
            return;
        }

        if (t1!=null)
            t1.next=yy;
        else
            head=yy;
        if (t2!=null)
            t2.next=xx;
        else
            head=xx;
        Node temp = xx.next;
        xx.next = yy.next;
        yy.next=temp;

    }

    private void removeDuplicateUnsorted() {
        Node out = head;
        Node in = out.next;

        while(out!=null){
            Node prev = out;
            while(in!=null){
                if (out.data==in.data){

                    if (in.next!=null)
                        prev.next=in.next;
                    else
                        prev.next=null;
                }
                prev=in;
                in=in.next;

            }
            in=out.next;
            out=out.next;
        }
    }

    private void insertEnd(int key) {
        Node node = new Node(key);
        if (head == null )
            head=node;
        else{
            Node curr = head;
            while(curr.next!= null)
                curr = curr.next;
            curr.next = node;
        }
    }

    private void printList() {

        Node node = head;

        while(node != null){
            System.out.print(node.data+" -> ");
            node = node.next;
        }
        System.out.println("null ");
    }
}
