package com.LinkedList;

public class LinkedListTest {

    Node head;

    public static void main(String[] args) {

        LinkedListTest ll = new LinkedListTest();

        ll.insertEnd(1);
        ll.insertEnd(2);
        ll.insertEnd(3);
        ll.insertEnd(4);
        ll.insertEnd(5);
        ll.insertEnd(6);
        ll.insertEnd(7);
        ll.insertEnd(8);
        ll.insertEnd(9);

        ll.printList();

     //   ll.head = ll.reverse(ll.head, 3);

     //   ll.printList();

     //   ll.removeKthNode(4);

       // ll.removeNthNodeAfterMthNode(1,3);

        ll.rearrangeOddEven();
        ll.printList();
    }

    private void rearrangeOddEven(){
        Node curr = head;
        Node odd = head;
        Node oddFirst = odd;
        Node even = curr.next;
        Node evenFirst = even;
        boolean flag=true;
        curr = curr.next.next;
        while(curr!= null){
            if (flag){
                odd.next=curr;
                odd=odd.next;
                flag=false;
            }else{
                even.next=curr;
                even=even.next;
                flag=true;
            }
            curr=curr.next;
        }
        even.next=null;
        odd.next=evenFirst;
        head=oddFirst;
    }

    private void removeNthNodeAfterMthNode(int n, int m) {

        Node curr = head;
        Node prev = null;
        int cn = 0, cm=0;
        while(curr != null){

            while(cm < m && curr!= null){
                cm++;
                prev = curr;
                curr = curr.next;
            }
            while(cn<n && curr!=null){
                cn++;
                curr=curr.next;
            }
            if (cn==n){
                prev.next=curr;
                cn=0;
                cm=0;
            }else{
                prev.next=null;
            }
        }
    }

    private void removeKthNode(int k) {
        if (k==1){
            head=null;
            return;
        }
        Node curr = head;
        Node prev = null;
        int count = 0;
        while(curr != null){
            count++;
            if (count==k){
                prev.next=curr.next;
                count=0;
            }
                prev = curr;
                curr = curr.next;
        }
    }

    private Node reverse(Node node, int k) {

        Node prev = null;
        Node curr = node;
        Node next = null;
        int count =0;
        while(curr != null && count <k){
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
            count++;
        }

        if (next != null)
            node.next = reverse(next,k);
        return prev;
    }

    private void insertEnd(int key) {
        Node node = new Node(key);
        if (head == null )
            head=node;
        else{
            Node curr = head;
            while(curr.next!= null)
                curr = curr.next;
            curr.next = node;
        }
    }

    private void printList() {

        Node node = head;

        while(node != null){
            System.out.print(node.data+" -> ");
            node = node.next;
        }
        System.out.println("null ");
    }
}
