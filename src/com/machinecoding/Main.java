package com.machinecoding;

import java.util.Arrays;
import java.util.Base64;

public class Main {
    public static void main(String[] args) {
        String str = "PT1TVU0oMSsxKSxTVU0oMSsyKSxTVU0oMSszKSxIWVBFUkxJTksoIltCbG9ja2VkVVJMXSIpLFNVTSgxKzUpLC09U1VNKDErNiksU1VNKDErNyksU1VNKDErOCksKz1TVU0oMSs5KSxTVU0oMSsxMCksQC1TVU0oMSsxMSksQD1TVU0oMSsxMiksQCtTVU0oMSsxMyksU1VNKDErMTQpLFNVTSgxKzE1KSxTVU0oMSsxNikg";
        byte[] bytearray = Base64.getDecoder().decode(str);
        System.out.println(Arrays.toString(bytearray));
        System.out.println(Base64.getEncoder().encodeToString(bytearray));
        System.out.println("my name is khan");
    }

    /*Given two strings A and B, return whether or not A can be shifted some number of times to get B.

    For example, if A is abcde and B is cdeab, return true. If A is abc and B is acb, return false.*/


}
