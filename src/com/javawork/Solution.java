package com.javawork;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = Integer.valueOf(sc.nextLine());
        String s = sc.nextLine();
        int i=0,count=1;
        String res = "";
        while(i <n){
            res = res + s.charAt(i);
            i=i*count;
        }
        System.out.println(res);
    }
}
