package com.javawork;

import java.util.ArrayList;
import java.util.Arrays;

public class Test {
    private static ArrayList<String> list = new ArrayList<>();

    public static void main(String args[]){
        list.add("Kundan");
        list.add("Kundan");
        list.add("Kundan");
        list.add("Kundan");

        System.out.println(list.toArray());
        System.out.println(Arrays.toString(list.toArray()));

        System.out.println(isNthRoot(625,4));


    }

    private static boolean isNthRoot(int value, int n) {
        double a = Math.pow(value, 1.0 / n);
        System.out.println(a);
        return Math.pow(Math.round(a), n) == value;

    }
}

