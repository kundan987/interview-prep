package com.javawork;

public class SearchBST {

    Node insert(int data){

        return null;
    }

    public static void main(String[] args){
        Node root = null;
        root = new Node(9);
        root.left = new Node(7);
        root.right = new Node(11);
        root.left.right = new Node(8);
        root.left.left = new Node(6);
        root.right.right = new Node(12);
        root.right.left = new Node(10);
        boolean flag = search(root, 4);
        if(flag)
            System.out.println("Key Present");
        else
            System.out.println("Not Present");

    }

    private static boolean search(Node node , int key) {
        if(node== null)
            return false;
        if(node.data==key)
            return true;
        else
            if (node.data>key)
                return search(node.left,key);
        else
           return  search(node.right,key);
    }
}
