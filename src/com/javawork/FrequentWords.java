package com.javawork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FrequentWords {

    Map<String, Integer> map = new HashMap<>();
    int max =0;

    public static void main(String[] args){
        FrequentWords words = new FrequentWords();
        words.prepareData();
        words.getFrequentWords(5);
      //  System.out.print(words.map);
    }

    private void getFrequentWords(int n) {
        for(Map.Entry<String, Integer> ma:map.entrySet())
            if(ma.getValue()>max)
                max = ma.getValue();
        List[] arr = new List[max+1];
        for(Map.Entry<String, Integer> ma:map.entrySet()){
            if(arr[ma.getValue()]!=null){
                List list = arr[ma.getValue()];
                list.add(ma.getKey());
                arr[ma.getValue()]= list;
            }else{
                List list = new ArrayList();
                list.add(ma.getKey());
                arr[ma.getValue()]= list;
            }

        }
        int i=0;
        int j=max;
        int count =0;
        while(i<n){
            if(arr[j]!=null){
                count += arr[j].size();
                System.out.println(arr[j]);
                if(count<n)
                    i+=count;
                j--;
            }
        }
        //System.out.print(arr[max]);

    }

    private void prepareData() {
        map.put("kundan", 5);
        map.put("kumar", 2);
        map.put("mishra", 3);
        map.put("this", 6);
        map.put("kim", 6);
        map.put("sim", 6);
        map.put("rim", 6);
        map.put("dim", 6);
        map.put("is", 2);

    }
}
