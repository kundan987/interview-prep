package com.javawork;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class DepthFirstTraversal {

    Node root = null;

    public DepthFirstTraversal(Node root) {
        this.root = root;
    }

    public DepthFirstTraversal() {
    }

    public static void main(String[] args){
        DepthFirstTraversal traversal = new DepthFirstTraversal();
        traversal.root = new Node(1);
        traversal.root.left = new Node(2);
        traversal.root.right = new Node(3);
        traversal.root.left.left = new Node(4);
        traversal.root.left.right = new Node(5);
        System.out.println("Inorder traversal");
       // traversal.inorder();
        System.out.println("Preorder traversal");
       // traversal.preorder();
        System.out.println("PostOrder traversal");
        //traversal.postorder();
        System.out.println("insertInBinaryTree");
        traversal.insertInBinaryTree(6);
        System.out.println("After insertion inorder");
        traversal.inorder();
    }

    private void insertInBinaryTree(int key) {
        insertInBinaryTree(root,key);
    }

    private void insertInBinaryTree(Node node, int key) {

        Queue<Node> queue = new LinkedList<>();
        queue.add(node);
        while (!queue.isEmpty()){
            node = queue.peek();
            queue.remove();
            if (node.left==null){
                node.left= new Node(key);
                break;
            }else{
                queue.add(node.left);
            }
            if (node.right==null){
                node.right= new Node(key);
                break;
            }else{
                queue.add(node.right);
            }
        }
    }

    private void inorder() {
        //inorder(root);
        inorderIterative(root);
    }

    private void inorderIterative(Node root) {

        Stack<Node> stack = new Stack();
        Node node = root;

        while(node != null){
            stack.push(node);
            node=node.left;
        }
        while (!stack.empty()){
            node = stack.pop();
            System.out.print(node.data+" ");
            if (node.right!=null) {
                node=node.right;
                while(node != null){
                    stack.push(node);
                    node=node.left;
                }
            }

        }
    }


    private void inorder(Node node) {
        if(node!=null){
            inorder(node.left);
            System.out.print(" "+node.data);
            inorder(node.right);
        }

    }

    private void preorder() {
        preorder(root);
    }

    private void preorder(Node node) {
        if(node!=null){
            System.out.print(" "+node.data);
            preorder(node.left);
            preorder(node.right);
        }

    }

    private void postorder() {
        postorder(root);
    }

    private void postorder(Node node) {
        if(node!=null){
            postorder(node.left);
            postorder(node.right);
            System.out.print(" "+node.data);
        }

    }
}
