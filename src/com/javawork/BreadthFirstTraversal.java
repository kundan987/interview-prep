package com.javawork;

public class BreadthFirstTraversal {

    Node root;
    public BreadthFirstTraversal() {
        root=null;
    }
    public static void main(String[] args){
        BreadthFirstTraversal traversal = new BreadthFirstTraversal();
        traversal.root = new Node(1);
        traversal.root.left = new Node(2);
        traversal.root.right = new Node(3);
        traversal.root.left.left = new Node(4);
        traversal.root.left.right = new Node(5);
        traversal.printLevelOrder();
    }

    private void printLevelOrder() {

        int h = height(root);
        for (int i =1 ; i<= h; i++){
            printGivenLevel(root,i);
        }
    }

    private void printGivenLevel(Node root, int level) {
        if (root==null)
            return;
        if (level ==1)
            System.out.println(root.data);
        else if (level >1 ){
            printGivenLevel(root.left,level-1);
            printGivenLevel(root.right,level-1);
        }
    }

    private int height(Node node) {

        if (node == null)
            return 0;
        else{
            int lheight = height(node.left);
            int rheight = height(node.right);

            if (lheight>rheight)
                return lheight+1;
            else
                return rheight+1;
        }
    }
}
