package com.javawork;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimpleThreadPool {

    public static void main(String[] args){
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        ExecutorService executor = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 10; i++) {
            Runnable runnable = new WorkerThread(""+i);
            executorService.execute(runnable);
            executor.execute(runnable);
        }
        executorService.shutdown();
        executor.shutdownNow();
        while (!executorService.isTerminated()) {
        }

        System.out.println("Finished all threads");
    }
}
