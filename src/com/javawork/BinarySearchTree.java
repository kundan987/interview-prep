package com.javawork;

public class BinarySearchTree {

    Node root;

    public BinarySearchTree() {
        root = null;
    }

    public static void main(String[] args){
        BinarySearchTree tree = new BinarySearchTree();
        tree.insert(50);
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);
        System.out.println("Inorder traversal of the given tree");
        tree.inorder();

        System.out.println("\nDelete 20");
        tree.deleteKey(20);
        System.out.println("Inorder traversal of the modified tree");
        tree.inorder();

        System.out.println("\nDelete 30");
        tree.deleteKey(30);
        System.out.println("Inorder traversal of the modified tree");
        tree.inorder();

        System.out.println("\nDelete 50");
        tree.deleteKey(50);
        System.out.println("Inorder traversal of the modified tree");
        tree.inorder();
    }

    private void insert(int data) {
        root = insertRec(root, data);
    }

    private Node insertRec(Node root, int data) {

        if(root== null){
            return new Node(data);
        }

        if(data< root.data)
            root.left = insertRec(root.left,data);
        else if(data > root.data)
            root.right = insertRec(root.right,data);

        return root;
    }

    private void inorder(){
        inorder(root);
    }

    private void inorder(Node root) {
        if (root!=null){
            inorder(root.left);
            System.out.print(root.data+" ");
            inorder(root.right);
        }
    }

    private void deleteKey(int key){
        root = deleteRec(root,key);
    }

    private Node deleteRec(Node root, int key) {

        if(root==null) return root;

        if(key<root.data)
            root.left = deleteRec(root.left,key);
        else if (key>root.data)
            root.right = deleteRec(root.right,key);
        else{
            if (root.left == null)
                return root.right;
            else if (root.right==null)
                return root.left;

            root.data = minValue(root.right);

            root.right = deleteRec(root.right,root.data);
        }
        return root;
    }

    private int minValue(Node root) {

        int minv = root.data;
        while(root.left !=null){
            minv = root.left.data;
            root = root.left;
        }
        return minv;

    }


    private Node deleteNode(Node root, int key){

        if(root == null) return root;

        if(key<root.data)
            root.left = deleteNode(root.left,key);
        else if(key>root.data)
            root.right = deleteNode(root.right,key);

        else{
            if (root.left == null)
                return root.right;
            else if (root.right==null)
                return root.left;


            root.data = minValueIn(root.right);

            root.right = deleteNode(root.right,root.data);
        }
        return root;
    }

    private int minValueIn(Node node) {
        int minValue = node.data;
        while(node.left!= null){
            minValue = node.left.data;
            node = node.left;
        }
        return minValue;
    }
}
