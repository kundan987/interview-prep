package com.javawork;

import java.lang.reflect.Array;
import java.util.*;

public class PrintSpiral {

    public static int[] spiralOrder(final int[][] A) {
        int m = A.length;
        int n = A[0].length;
        int[] B = new int[m*n];
        int t=0, b=m-1, l=0, r=n-1,k=0,dir=0;
        while (t<=b && l<=r){
            if (dir==0){
                for (int i=l;i<=r; i++){
                    B[k]=A[t][i];
                    k++;
                }
                t++;
                dir=1;
            }else if (dir==1){
                for (int i=t; i<=b;i++){
                    B[k]=A[i][r];
                    k++;
                }
                r--;
                dir=2;
            }else if (dir==2){
                for (int i=r; i>=l;i--)
                    B[k++]=A[b][i];
                b--;
                dir=3;
            }else if (dir==3){
                for (int i=b;i>=t;i-- )
                    B[k++]=A[i][l];
                l++;
                dir=0;
            }
           // dir = (dir+1)%4;

        }

        return B;
    }

    public int coverPoints(int[] A, int[] B) {
        int x = A[0], y = B[0], step=0;
        for(int i=1; i< A.length; i++){
            step = step + Math.max(Math.abs(A[i]-x) ,Math.abs(B[i]-y));
            x= A[i]-x ; y = B[i]-y;
        }
        return step;
    }

    public static int[] plusOne(int[] A) {

        ArrayList<Long> array = new ArrayList<Long>();
        String s = "";
        for (int i=0; i<A.length;i++)
                s=s+A[i];
       // System.out.println(s.toString());
        long i = Integer.valueOf(s);
        i++;
        while(i>0){
            array.add(i%10);
            i=i/10;
        }
        int[] B = new int[array.size()];
        Object[] objects = array.toArray();
        int n = array.size();
        // Printing array of objects
        long ll=0;
        for (Object obj : objects)
             ll = (long)obj;
            B[n--] = (int)ll;
        return B;

    }

    public static int[] plusOneMod(int[] A) {

        int n = A.length;
        int count =0;
        for (int k=0;k<n;k++){
            if (A[k]==0){
                count++;
            }else{
                break;
            }
        }
        int[] mA = new int[n-count];
        int k =0;
        if (count>0){


            if (count != n){
                for (int j=count;j<n;j++){
                    mA[k++] = A[j];
                    // System.out.print(B[j]);
                }
                n=n-count;
            }else{
                mA =A;
            }

        }else{
            mA = A;
        }
        int x=0, i=0;
        for (i=n-1; i>=0;i--){
            x = mA[i]+1;
            if (x>9){
                mA[i]=0;

            }else{
                mA[i]=x;
                break;
            }
        }
        if (i<0 && x>9){
            int[] B = new int[n+1];
            B[0] = 1;
            for (int j=1;j<=n;j++){
                B[j] = mA[j-1];
               // System.out.print(B[j]);
            }
            return B;
        }else
            return mA;



       /* ArrayList<Long> array = new ArrayList<Long>();
        String s = "";

          //  s=s+A[i];
        // System.out.println(s.toString());
        long i = Integer.valueOf(s);
        i++;
        while(i>0){
            array.add(i%10);
            i=i/10;
        }
        int[] B = new int[array.size()];
        Object[] objects = array.toArray();
        int n = array.size();
        // Printing array of objects
        long ll=0;
        for (Object obj : objects)
            ll = (long)obj;
        B[n--] = (int)ll;
        return B;*/

    }

    public static int[] wave(int[] A) {
        Arrays.sort(A);
        int temp = 0;
        for(int i =0; i<A.length;i=i+2){
            temp=A[i];
            A[i]=A[i+1];
            A[i+1]=temp;
        }

        return A;

    }

    public static String largestNumber(final int[] A) {
        Arrays.sort(A);
        String s = ""+A[0];

        for (int i=1; i< A.length;i++){
            if ((A[i]+s).compareTo(s+A[i])>0){
                s= A[i]+s;
            }else{
                s=s+A[i];
            }
        }
        System.out.println(s.toCharArray()[0]);
        if (s.toCharArray()[0]=='0'){
            s = "0";
        }

        return s;
    }

    public static String largestNum(int[] A){

        ArrayList<String> arr = new ArrayList<>();
        for (int i=0; i<A.length;i++)
            arr.add(""+A[i]);
        Collections.sort(arr, new Comparator<String>(){

            @Override
            public int compare(String X, String Y) {

                String XY=X + Y;

                String YX=Y + X;

                return XY.compareTo(YX) > 0 ? -1:1;
            }
        });
        String s = "";
        Iterator it = arr.iterator();
        boolean flag = true;

        while(it.hasNext()){
            String temp = (String) it.next();
            s=s+temp;
            if(flag && temp.equalsIgnoreCase("0")){
                break;
            }else
                flag = false;
        }

        String listString = "";

        for (String ss : arr)
        {
            s += ss;
            if(flag && ss.equalsIgnoreCase("0")){
                break;
            }else
                flag = false;
        }

        if(flag)
            return "0";
        else return s;
    }

    static void printLargest(Vector<String> arr){

        Collections.sort(arr, new Comparator<String>(){

            // A comparison function which is used by
            // sort() in printLargest()
            @Override
            public int compare(String X, String Y) {

                // first append Y at the end of X
                String XY=X + Y;

                // then append X at the end of Y
                String YX=Y + X;

                // Now see which of the two formed numbers
                // is greater
                return XY.compareTo(YX) > 0 ? -1:1;
            }
        });

        Iterator it = arr.iterator();

        while(it.hasNext())
            System.out.print(it.next());

    }
    public static int[] primesum(int A) {
        ArrayList<Integer> list = new ArrayList<>();
        int n = A;
        int[] result = new int[2];
        list.add(2);
        boolean flag = true;
        for(int i=2; i<=n; i++){
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (j < i && i % j == 0) {
                    flag=false;
                    break;
                }
            }
            if (flag){
                list.add(i);
                System.out.print(i+" ");
            }
            flag=true;

        }
      //  System.out.println(list.iterator());
        for (int i=0; i<list.size();i++){
            int x = list.get(i);
            System.out.println(x);
            int y = A-x;
            System.out.println(y);
            if (list.contains(y)){
                result[0]=x;
                result[1]=y;
                break;
            }
        }
        return result;
    }

    public static int[] primeSum(int A) {
        int n = A+1;
        int[] prime = new int[n];
        for (int i=0;i<n;i++){
            prime[i]=1;
        }
        prime[0]=0;prime[1]=0;
        for (int i=2;i<Math.sqrt(n);i++){
            if(prime[i]==1){
                for (int j=2; i*j<=n;j++){
                    prime[i*j]=0;
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {

        int[] d = new int[] {0, 0, 0, 0, 0}; // 34 , 30, 9, 5, 3
        //String s = largestNum(d);
       // System.out.println(s);
        int[] res = primesum(1048574);
        System.out.println(res[0]+" "+res[1]);
        Vector<String> arr;
        arr = new Vector<>();

        //output should be 6054854654
        arr.add("0");
        arr.add("0");
        arr.add("0");
        arr.add("0");
        arr.add("0");
     //   printLargest(arr);

     /*   int[][] A = {{1, 2, 3},{4, 5, 6}};

        System.out.println(A.length);
        System.out.println(A[0].length);
        int m = A.length;
        int n = A[0].length;

        for (int i=0; i< A.length;i++){
            for (int j =0 ; j< A[0].length;j++)
                System.out.print(A[i][j]+" ");
            System.out.println();
        }
        int[] B = spiralOrder(A);
        for (int i=0; i<m*n;i++)
            System.out.print(B[i]+ " ");
        */

      //  int[] C = wave(new int[] {9, 99, 999, 9999, 9998});
        //for (int i=0; i<C.length;i++)
          //  System.out.print(C[i]);


    }
}
