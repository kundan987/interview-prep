package com.porter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //Given an array of sorted integers, find the frequency of given integer k

        int ar[] = {1,2,3,3,3,4,5,6,7,8};
        int k = 3;
       // int count = count(ar, k);
       // System.out.println("Count = "+ count);

        try {
            sendGet();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static void openAndClosePrices(String firstDate, String lastDate, String weekDay) {

        DateFormat df = new SimpleDateFormat("d-MMMM-yyyy");
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Sunday",0);
        map.put("Monday",1);
        map.put("Tuesday",2);
        map.put("Wednesday",3);
        map.put("Thursday",4);
        map.put("Friday",5);
        map.put("Saturday",6);
        Date inDate = new Date(firstDate);
        Date outDate = new Date(lastDate);
        int day = map.get(weekDay);


    }

    private static void sendGet() throws Exception {

        String url = "https://jsonmock.hackerrank.com/api/stocks";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        System.out.println(con.getResponseMessage());


        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
           // response.append(inputLine);
        }
        in.close();
      /*  Map jsonJavaRootObject = new Gson().fromJson(response.toString(), Map.class);
        System.out.println(
                ((List)
                        (
                                (Map)
                                        (
                                                javaRootMapObject.get("data")
                                        )
                        ).get("translations")
                ).get(0)
        );
        Gson
        JSONObject myResponse = new JSONObject(response.toString());*/

        int responseCode = con.getResponseCode();
     //   System.out.println("\nSending 'GET' request to URL : " + url);
      //  System.out.println("Response Code : " + responseCode);
        //print result
        System.out.println(response.toString());

    }


    static int differentTeams(String skills) {

        HashMap<Character, Integer> map = new HashMap<>();
        for(int i=0; i < skills.length();i++){
            if (map.containsKey(skills.charAt(i))){
                map.put(skills.charAt(i), map.get(skills.charAt(i))+1);
            }else{
                map.put(skills.charAt(i), 1);
            }
        }
        int p = map.containsKey('p')? map.get('p') : 0;
        int c = map.containsKey('c')? map.get('c') : 0;
        int m = map.containsKey('m')? map.get('m') : 0;
        int b = map.containsKey('b')? map.get('b') : 0;
        int z = map.containsKey('z')? map.get('z') : 0;
        int res =0;
        res = Math.min(p,c);
        res = Math.min(m, res);
        res = Math.min(b, res);
        res = Math.min(z, res);

        return res;

    }

    private static int count(int[] ar, int k) {

        int i , j;
        int n = ar.length;

        i =  funLeft(ar, n, 0, n-1, k);
        j = funRight(ar, n, 0, n-1, k);
        return j-i+1;
    }

    private static int funLeft(int[] ar, int n, int l, int r, int k) {
        if (r>=l){
            int mid = (r+l)/2;
            System.out.println(mid);
            if(ar[mid]> k){
                 return funLeft(ar, n, l, mid-1,k);
            } else if(ar[mid] < k){
                return funLeft(ar, n, mid+1, r,k);
            } else {
                if (mid == 0 || k > ar[mid-1])
                return mid;
                else
                    return funLeft(ar, n, l, mid-1,k);
            }
        }
        return -1;
    }

    private static int funRight(int[] ar, int n, int l, int r, int k) {
        if (r>=l){
            int mid = (r+l)/2;
            System.out.println(mid);
            if(ar[mid]> k){
                return funRight(ar, n, l, mid-1,k);
            } else if(ar[mid] < k){
                return funRight(ar, n, mid+1, r,k);
            } else {
                if (mid == n-1 || k < ar[mid+1])
                    return mid;
                else
                    return funRight(ar, n, mid+1, r,k);
            }
        }
        return -1;
    }
}
