package practice;

class Solution {

    public static void main(String[] args) {
        Solution solution = new Solution();
        //System.out.println(solution.minOperations(new int[]{123456789}));
       // System.out.println(solution.minOperations(new int[]{987654321}));
        System.out.println(solution.minOperations(new int[]{1000000000}));
        System.out.println(solution.minOperations(new int[]{123456789, 987654321}));
    }
    public int minOperations(int[] nums) {
        int mulMax =0, addMax=0;
        for(int num : nums){
            if(num==1)
                addMax++;
            else {
                int n = num;
                if(n%2==1){
                    n=n-1;
                    addMax++;
                }
                int c=0;
                while(n>1){
                    if(n%2==1){
                        n=n-1;
                        addMax++;
                    }
                    System.out.println(n);
                    c++;
                    n/=2;
                }
                if(n==1)
                    addMax++;
                System.out.println(mulMax);
                mulMax = Math.max(mulMax, c);
            }
        }
        return mulMax+addMax;
    }
}
