package practice;

public class BacktrackingRatMaze {

    static int N;
    public static void main(String[] args) {
        BacktrackingRatMaze rat = new BacktrackingRatMaze();
        int maze[][] = {{ 1, 0, 0, 0 },
                        { 1, 1, 0, 1 },
                        { 0, 1, 1, 1 },
                        { 1, 1, 0, 1 } };

        N = maze.length;
        rat.ratMaze(maze);
    }
    public void ratMaze(int[][] maze){
        int[][] output = new int[N][N];

        if(!ratMazeUtil(maze,output,0,0))
            System.out.println("Not possible");
        else
            for(int i=0; i<N;i++){
                for(int j=0; j<N; j++){
                    System.out.print(output[i][j] + " ");
                }
                System.out.println();
            }
    }

    private boolean ratMazeUtil(int[][] maze, int[][] output, int x, int y) {

        if (x==N-1 && y==N-1 && maze[x][y]==1){
            output[x][y]=1;
            return true;
        }

        if (isSafe(maze,x,y)){
            output[x][y] =1;
            if (ratMazeUtil(maze,output,x+1,y)) //down
                return true;
            /*if (ratMazeUtil(maze,output,x,y-1)) // left
                return true;*/
            if (ratMazeUtil(maze,output,x,y+1)) //right
                return true;
            /*if (ratMazeUtil(maze,output,x-1,y)) //up
                return true;*/

            output[x][y]=0;
        }

        return false;
    }

    private boolean isSafe(int[][] maze, int x, int y) {
        return (x>=0 && x<N && y>=0 && y<N && maze[x][y]==1);
    }
}
