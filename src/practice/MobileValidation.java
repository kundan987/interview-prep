package practice;

import com.google.common.base.CharMatcher;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobileValidation {

    public static void main(String[] args) {
      /*  System.out.println("112 : " + getTruncatedMobileNumber("112"));
        System.out.println("18001034141 : " + getTruncatedMobileNumber("18001034141"));
        System.out.println("180010341412 : " + getTruncatedMobileNumber("180010341412"));
        System.out.println("1800118887 : " + getTruncatedMobileNumber("1800118887"));
        System.out.println("1800118887098 : " + getTruncatedMobileNumber("1800118887098"));
        System.out.println("+1800-300-1947 : " + getTruncatedMobileNumber("+1800-300-1947"));
        System.out.println("98765 43210 : " + getTruncatedMobileNumber("98765 43210"));
        System.out.println("+917619630838 : " + getTruncatedMobileNumber("+917619630838"));
        System.out.println("7619630838 : " + getTruncatedMobileNumber("7619630838"));
        System.out.println("+07619630838 : " + getTruncatedMobileNumber("+07619630838"));
        System.out.println("+0 7619630838 : " + getTruncatedMobileNumber("+0 7619630838"));
        System.out.println("+ 07619630838 : " + getTruncatedMobileNumber("+ 07619630838"));
        System.out.println("07619630838 : " + getTruncatedMobileNumber("07619630838"));
        System.out.println("0 7619630838 : " + getTruncatedMobileNumber("0 7619630838"));
        System.out.println("761-963-0838 : " + getTruncatedMobileNumber("761-963-0838"));
        System.out.println("(761)-963-0838 : " + getTruncatedMobileNumber("(761)-963-0838"));
        System.out.println("(761) 963-0838 : " + getTruncatedMobileNumber("(761) 963-0838"));
        System.out.println("(+91)761-963-0838 : " + getTruncatedMobileNumber("(+91)761-963-0838"));
        System.out.println("+91761-963-0838 : " + getTruncatedMobileNumber("+91761-963-0838"));
        System.out.println("+91 761-963-0838 : " + getTruncatedMobileNumber("+91 761-963-0838"));
        System.out.println("+91 761 963 0838 : " + getTruncatedMobileNumber("+91 761 963 0838"));
        System.out.println("+91 76196 30838 : " + getTruncatedMobileNumber("+91 76196 30838"));
        System.out.println("076-196-30838 : " + getTruncatedMobileNumber("076-196-30838"));*/
        System.out.println("--------------------------------------------------");
        System.out.println("--------------------Invalid Mobiles -----------------------");
        System.out.println("");
        System.out.println("Empty               : " + getTruncatedMobile(""));
        System.out.println("112                 : " + getTruncatedMobile("112"));
        System.out.println("123456789           : " + getTruncatedMobile("123456789"));
        System.out.println("180010341412        : " + getTruncatedMobile("180010341412"));
        System.out.println("1800118887098       : " + getTruncatedMobile("1800118887098"));
        System.out.println("+ 07619630838       : " + getTruncatedMobile("+ 07619630838"));
        System.out.println("(+91)761-963-0838   : " + getTruncatedMobile("(+91)761-963-0838"));
        System.out.println("--------------------Valid Mobiles -----------------------");
        System.out.println("");
        System.out.println("1800118887          : " + getTruncatedMobile("1800118887"));
        System.out.println("18001034141         : " + getTruncatedMobile("18001034141"));
        System.out.println("+1800-300-1947      : " + getTruncatedMobile("+1800-300-1947"));
        System.out.println("+917619630838       : " + getTruncatedMobile("+917619630838"));
        System.out.println("7619630838          : " + getTruncatedMobile("7619630838"));
        System.out.println("+07619630838        : " + getTruncatedMobile("+07619630838"));
        System.out.println("+0 7619630838       : " + getTruncatedMobile("+0 7619630838"));
        System.out.println("761-963-0838        : " + getTruncatedMobile("761-963-0838"));
        System.out.println("(761)-963-0838      : " + getTruncatedMobile("(761)-963-0838"));
        System.out.println("(761) 963-0838      : " + getTruncatedMobile("(761) 963-0838"));
        System.out.println("+91761-963-0838     : " + getTruncatedMobile("+91761-963-0838"));
        System.out.println("+91 761-963-0838    : " + getTruncatedMobile("+91 761-963-0838"));
        System.out.println("+91 761 963 0838    : " + getTruncatedMobile("+91 761 963 0838"));
        System.out.println("+91 76196 30838     : " + getTruncatedMobile("+91 76196 30838"));
        System.out.println("076-196-30838       : " + getTruncatedMobile("076-196-30838"));
        System.out.println("07619630838         : " + getTruncatedMobile("07619630838"));
        System.out.println("0 7619630838        : " + getTruncatedMobile("0 7619630838"));
        System.out.println("98765 43210         : " + getTruncatedMobile("98765 43210"));
        System.out.println("+91 (761) 963-0838   : " + getTruncatedMobile("+91 (761) 963-0838"));
        System.out.println("--------------------------------------------------");

    }

    static boolean isMobileValid(String mobile){
        String patterns
                = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?){2}\\d{3}$"
                + "|^(\\+\\d{1,2}( )?)?(\\d{5}[ ]?)\\d{5}$"
                + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?)(\\d{2}[ ]?){2}\\d{2}$"
                + "|^(\\d{5}[- .]?)\\d{5}$"
                + "|^(\\d[- .]?)\\d{10}$"
                + "|^\\d{3}[- .]?\\d{3}[- .]?\\d{5}$";
        Pattern pattern = Pattern.compile(patterns);
        Matcher matcher = pattern.matcher(mobile);

        return matcher.matches();

    }
    static String getTruncatedMobile(String mobile) {
        String truncatedMobile = "Not Valid";
        if (isMobileValid(mobile)){
            CharMatcher matcher = CharMatcher.javaDigit();
            String temp = matcher.retainFrom(mobile);
            if (!"1800".equals(temp.substring(0,4)) && temp.length()>=10)
                truncatedMobile = temp.substring(temp.length()-10);
        }

        return truncatedMobile;
    }


    static boolean isMobileNumberValid(String mobileNumber) {
        String mobileNumberRegex = "((\\+*)((0[ -]+)*|(91 )*)(\\d{12}+|\\d{10}+))|\\d{5}([- ]*)\\d{6}";
        mobileNumber = StringUtils.deleteWhitespace(mobileNumber);
        boolean result = false;
        if(mobileNumber.isEmpty())
            return false;

        if(mobileNumber.matches(mobileNumberRegex))
            result =  true;
        System.out.println(mobileNumber +" :"+result);
        return result;
    }

    static String getTruncatedMobileNumber(String mobileNumber) {
        String truncatedMobileNumber;
        if(isMobileNumberValid(mobileNumber)){
            truncatedMobileNumber = StringUtils.deleteWhitespace(mobileNumber);
            truncatedMobileNumber = truncatedMobileNumber.replaceAll("[+-]", "");
            return truncatedMobileNumber.length()>=10? truncatedMobileNumber.substring(truncatedMobileNumber.length()-10) : null;
        }
        return null;
    }

    static private String getCacheName(String serviceUrl) {

        if (serviceUrl.contains("/api/")) {
            return getCacheNameForGetRequest(serviceUrl);
        }
        return serviceUrl.substring(serviceUrl.indexOf("/service"), serviceUrl.lastIndexOf('/') + 1);
    }

    static private String getCacheNameForGetRequest(String serviceUrl) {
        int index = serviceUrl.indexOf("/api");
        if (index < 0) {
            return null;
        }

        serviceUrl = serviceUrl.substring(index);

        String[] splits = serviceUrl.split("/");
        if (splits == null || splits.length < 4) {
            return null;
        }
        return new StringBuilder("/").append(splits[1]).append("/").append(splits[2]).append("/").append(splits[3]).append("/").toString();
    }


}
