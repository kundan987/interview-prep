package practice;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Palindrome {

    public static void main(String[] args) {
        String s = " 35+52 / 2 ";
        s = s.trim().replaceAll(" ","");
        String [] nums = s.split("[\\+\\-\\*\\/]+");
        String [] ops = s.split ("[\\d]+");
        System.out.println(Arrays.asList(nums));
        System.out.println(Arrays.asList(ops));
        Palindrome palindrome = new Palindrome();
        System.out.println(palindrome.palindrome("kundnuk"));
        System.out.println(palindrome.findAllPalindromesUsingBruteForceApproach("kundnuk"));
        System.out.println(palindrome.findAllPalindromesUsingBruteForceApproach("kundan"));
        System.out.println(palindrome.findAllPalindromesUsingBruteForceApproach("andn"));
        System.out.println(palindrome.findAllPalindromesUsingBruteForceApproach("abba"));
        System.out.println(palindrome.findAllPalindromesUsingCenter("kundnuk"));
        System.out.println(palindrome.findAllPalindromesUsingCenter("kundan"));
        System.out.println(palindrome.findAllPalindromesUsingCenter("andn"));
        System.out.println(palindrome.findAllPalindromesUsingCenter("abba"));
    }

    public Set<String> palindrome(String input){

        String formattedInput = "@" + input + "#";
        char inputCharArr[] = formattedInput.toCharArray();

        int radius[][] = new int[2][input.length() + 1];

        Set<String> palindromes = new HashSet<>();
        int max;
        for (int j = 0; j <= 1; j++) {
            radius[j][0] = max = 0;
            int i = 1;
            while (i <= input.length()) {
                palindromes.add(Character.toString(inputCharArr[i]));
                while (inputCharArr[i - max - 1] == inputCharArr[i + j + max])
                    max++;
                radius[j][i] = max;
                int k = 1;
                while ((radius[j][i - k] != max - k) && (k < max)) {
                    radius[j][i + k] = Math.min(radius[j][i - k], max - k);
                    k++;
                }
                max = Math.max(max - k, 0);
                i += k;
            }
        }
        for (int i = 1; i <= input.length(); i++) {
            for (int j = 0; j <= 1; j++) {
                for (max = radius[j][i]; max > 0; max--) {
                    palindromes.add(input.substring(i - max - 1, max + j + i - 1));
                }
            }
        }
        return palindromes;
    }


    public Set<String> findAllPalindromesUsingCenter(String input) {
        Set<String> palindromes = new HashSet<>();
        for (int i = 0; i < input.length(); i++) {
            palindromes.addAll(findPalindromes(input, i, i + 1));
            palindromes.addAll(findPalindromes(input, i, i));
        }
        return palindromes;
    }

    private Set<String> findPalindromes(String input, int low, int high) {
        Set<String> result = new HashSet<>();
        while (low >= 0 && high < input.length() && input.charAt(low) == input.charAt(high)) {
            result.add(input.substring(low, high + 1));
            low--;
            high++;
        }
        return result;
    }

    public Set<String> findAllPalindromesUsingBruteForceApproach(String input) {
        Set<String> palindromes = new HashSet<>();
        for (int i = 0; i < input.length(); i++) {
            for (int j = i + 1; j <= input.length(); j++) {
                if (isPalindrome(input.substring(i, j))) {
                    palindromes.add(input.substring(i, j));
                }
            }
        }
        return palindromes;
    }

    private boolean isPalindrome(String input) {
        StringBuilder plain = new StringBuilder(input);
        StringBuilder reverse = plain.reverse();
        return (reverse.toString()).equals(input);
    }
}
