package practice;

import java.util.*;

public class AccountsMerge {

    public static void main(String[] args) {

        Character.isLetter('c');
        AccountsMerge accountsMerge = new AccountsMerge();
        System.out.println(accountsMerge.accountsMerge(Arrays.asList(
                Arrays.asList("John", "johnsmith@mail.com", "john00@mail.com"),
                Arrays.asList("John", "johnnybravo@mail.com"),
                Arrays.asList("John", "johnsmith@mail.com", "john_newyork@mail.com"),
                Arrays.asList("Mary", "mary@mail.com"))));
    }
    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, String> emailToName = new HashMap();
        Map<String, ArrayList<String>> graph = new HashMap();
        for (List<String> account: accounts) {
            String name = "";
            System.out.println("----------for entered-----------");
            for (String email: account) {
                if (name == "") {
                    name = email;
                    continue;
                }

                graph.computeIfAbsent(email, x-> new ArrayList<>()).add(account.get(1));
                System.out.println(graph);
                System.out.println("--------------------------");
                graph.computeIfAbsent(account.get(1), x-> new ArrayList<>()).add(email);
                System.out.println(graph);
                System.out.println("--------------------------");
                emailToName.put(email, name);

            }
            System.out.println("----------for exited-----------");
        }

        Set<String> seen = new HashSet();
        List<List<String>> ans = new ArrayList();
        for (String email: graph.keySet()) {
            if (!seen.contains(email)) {
                seen.add(email);
                Stack<String> stack = new Stack();
                stack.push(email);
                List<String> component = new ArrayList();
                while (!stack.empty()) {
                    String node = stack.pop();
                    component.add(node);
                    for (String nei: graph.get(node)) {
                        if (!seen.contains(nei)) {
                            seen.add(nei);
                            stack.push(nei);
                        }
                    }
                }
                Collections.sort(component);
                component.add(0, emailToName.get(email));
                ans.add(component);
            }
        }
        System.out.println(emailToName);

        return ans;
    }
}
