package practice;

class LCCCycle {
    int n=0,m=0;

    public static void main(String[] args) {
        LCCCycle cycle = new LCCCycle();
        char[][] grid = new char[][]{{'a','a','a','a'},{'a','b','b','a'},{'a','b','b','a'},{'a','a','a','a'}};
        System.out.println(cycle.containsCycle(grid));
    }
    public boolean containsCycle(char[][] grid) {
        n = grid.length;
        m = grid[0].length;
        char ss = grid[0][0];
        grid[0][0] ='#';
        return cycleUtil(grid, ss,  0,0,0,0,true);
    }
    private boolean cycleUtil(char[][] grid, char ch, int x, int y, int X, int Y, boolean flag) {

        if (!flag && x==X && y==Y && grid[x][y]=='#'){
            return true;
        }

        if (isSafe(grid, ch, x,y)){
            char c = grid[x][y];
            grid[x][y] ='*';
            flag = false;
            if (cycleUtil(grid, ch,x+1,y, X, Y, flag)) //down
                return true;
            if (cycleUtil(grid, ch, x,y-1, X, Y, flag)) // left
                return true;
            if (cycleUtil(grid, ch, x,y+1, X, Y, flag)) //right
                return true;
            if (cycleUtil(grid, ch, x-1,y, X, Y, flag)) //up
                return true;

            grid[x][y]=c;
        }

        return false;
    }
    private boolean isSafe(char[][] grid, char ch, int x, int y) {
        return (x>=0 && x<n && y>=0 && y<m && grid[x][y]==ch);
    }

}