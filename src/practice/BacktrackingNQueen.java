package practice;

public class BacktrackingNQueen {
    
    static int N =4;
    public static void main(String[] args) {
        solveNQueen();
    }

    private static void solveNQueen() {

        int board[][] = {{ 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 }};

        if(!solveNQueenUtil(board,0))
            System.out.println("Not possible");

    }

    static void printSolution(int[][] board){
        for(int i=0; i<N;i++){
            for(int j=0; j<N; j++){
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("-----------------------");
    }

    private static boolean solveNQueenUtil(int[][] board, int col) {

        if (col==N){
            printSolution(board);
            return true;
        }

        boolean res = false;
        for(int i=0; i<N; i++){

            if (isSafe(board,col,i)){
                board[i][col] = 1;
                res = (solveNQueenUtil(board, col+1)) || res;
                board[i][col] = 0;
            }
        }
        return res;

    }

    private static boolean isSafe(int[][] board, int col, int row) {

        for(int i=0; i< col; i++)
            if(board[row][i] == 1)
                return false;

        for(int i=row,j=col; i>=0 && j>=0; i--,j--)
            if(board[i][j]==1)
                return false;

        for(int i=row,j=col; i<N && j>=0; i++,j--)
            if(board[i][j]==1)
                return false;

        return true;
    }
}
