package practice;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

class LRUCache{

    public static void main(String[] args) {
        LRUCache cache = new LRUCache(2);

        cache.put(1,1);
        cache.put(2,2);
        System.out.println(cache.get(1));
        cache.put(3,3);
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
    }
    private int capacity;
    HashMap<Integer, DLNode> map;
    DoublyLinkedList dll;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>();
        dll = new DoublyLinkedList();
    }

    public int get(int key) {
        if(map.containsKey(key)){
            DLNode node = map.get(key);
            updateList(node);
            return node.val;
        } else
            return -1;

    }

    private void updateList(DLNode node) {
        dll.remove(node);
        dll.addFirst(node);
    }

    public void put(int key, int value) {
        DLNode node = new DLNode(key, value);
        if(!map.containsKey(key)){
            dll.addFirst(node);
        } else{
            node = map.get(key);
            if(capacity>=map.size()){
                dll.removeLast();
                dll.addFirst(node);
            } else {
                updateList(node);
            }
        }
        map.put(key, node);
    }
}

class DoublyLinkedList{
    DLNode head;
    DLNode tail;

    public DoublyLinkedList() {
        head = new DLNode();
        tail = new DLNode();
        head.next = tail;
        tail.prev = head;
    }

    public void addFirst(DLNode node){
        node.next = head.next;
        node.prev = head;
        head.next.prev = node;
        head.next = node;

    }

    public void remove(DLNode node){
        node.next.prev = node.prev;
        node.prev.next = node.next;
    }

    public int removeLast(){
        DLNode node = tail.prev;
        int key = node.key;
        remove(node);
        return key;
    }
}

class DLNode {
    int val;
    int key;
    DLNode next;
    DLNode prev;

    public DLNode() {
    }

    public DLNode(int val, int key) {
        this.val = val;
        this.key = key;
    }
}

