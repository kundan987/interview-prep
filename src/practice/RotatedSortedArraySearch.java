package practice;

public class RotatedSortedArraySearch {
    public static void main(String[] args) {
        RotatedSortedArraySearch search = new RotatedSortedArraySearch();
        /*System.out.println(search.searchEffective(new int[]{4,5,6,7,0,1,2}, 0));
        System.out.println(search.searchEffective(new int[]{}, 5));
        System.out.println(search.searchEffective(new int[]{5,4,3}, 5));
        System.out.println(search.searchEffective(new int[]{6,5,4}, 5));
        System.out.println(search.searchEffective(new int[]{1,2,3,5}, 5));
        System.out.println(search.searchEffective(new int[]{1,2,3,5,6}, 5));

        System.out.println(search.searchDuplicate(new int[]{5,4,3}, 6));
        System.out.println(search.searchDuplicate(new int[]{6,5,4}, 6));
        System.out.println(search.searchDuplicate(new int[]{1,2,3,5}, 6));
        System.out.println(search.searchDuplicate(new int[]{1,2,3,5,6}, 6));*/

        System.out.println(9>>2);
        System.out.println(8<<1);
    }
    public int search(int[] nums, int target) {
        if(nums.length == 0)
            return -1;
        if(nums.length == 1){
            if(nums[0] != target)
                return -1;
            else
                return 0;
        }

        int index = -1;
        int pivot = pivot(nums,0,nums.length-1);
        if(nums[pivot] == target)
            return pivot;
        if(pivot==0)
            return binarySearch(nums,0,nums.length-1,target);
        index = binarySearch(nums,0,pivot,target);
        if(index == -1)
            index = binarySearch(nums,pivot, nums.length-1, target);
        return index;
    }

    public int binarySearch(int[] nums, int left, int right, int target){
        while(left<=right){
            int mid = left + (right-left)/2;
            if(nums[mid] == target)
                return mid;
            else if(nums[mid] > target)
                right = mid -1;
            else
                left = mid +1;
        }
        return -1;
    }

    public int pivot(int[] nums, int left, int right){
        if(nums[left] < nums[right])
            return 0;

        while(left<=right){
            int mid = left + (right-left)/2;
            if(nums[mid] > nums[mid+1])
                return mid+1;
            else if (nums[mid] < nums[left])
                right = mid-1;
            else
                left = mid +1;
        }
        return 0;
    }
    public int searchEffective(int[] nums, int target) {
        int start = 0, end = nums.length - 1;
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (nums[mid] == target) return mid;
            else if (nums[mid] >= nums[start]) {
                if (target >= nums[start] && target < nums[mid]) end = mid - 1;
                else start = mid + 1;
            }
            else {
                if (target <= nums[end] && target > nums[mid]) start = mid + 1;
                else end = mid - 1;
            }
        }
        return -1;
    }

    public boolean searchDuplicate(int[] nums, int target) {
        int start =0;
        int end = nums.length-1;
        while(start<=end){
            int mid = start + (end-start)/2;
            if(nums[mid]==target)
                return true;
            else if(nums[mid]> nums[end]){
                if( target >= nums[start] && target < nums[mid])
                    end = mid;
                else
                    start = mid +1;
            } else if(nums[mid] < nums[end]){
                if( target <= nums[end] && target > nums[mid])
                    start = mid +1;
                else
                    end = mid;
            } else{
                end--;
            }
        }
        return false;
    }
}
