package practice;
import java.io.*;
public class NCoinsMPeoplesMaxC {
    public static void main(String[] args) throws IOException {
        /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter wr = new PrintWriter(System.out);
        int T = Integer.parseInt(br.readLine().trim());
        for(int i=0; i<T; i++) {
            String[] inp = br.readLine().split(" ");
            int N = Integer.parseInt(inp[0]);
            int M = Integer.parseInt(inp[1]);
            int C = Integer.parseInt(inp[2]);
            int out_ = solve(N, M, C);
            System.out.println(out_);
        }
        wr.close();
        br.close();*/

        System.out.println(fun(10,6,5));
        System.out.println(solve(10,6,5));
    }

    static int binomialCoeff(int n, int k) {
        if(n<0||k<0||k>n)return 0;
        int res = 1;

        if (k > n - k)
            k = n - k;

        for (int i = 0; i < k; ++i) {
            res *= (n - i);
            res /= (i + 1);
        }
        return res;
    }
    static int fun(int n, int m, int c) {
        int ans = binomialCoeff(n + m - 1, m - 1);
        for (int i = 1; i <= m; i++)
            ans +=(-1)*i*binomialCoeff(m, i) * binomialCoeff(n - (c + 1) * i + m - 1, m - 1);
        return ans;
    }

        static int solve(int N, int M ,int C){
        int result = 0;
        int resArr[][] = new int[M + 1][N + 1];
        resArr[0][0] = 0;
        for(int i=1; i <= N; i++)
            resArr[0][i] = -1;

        for(int i=1; i <= M; i++)
            resArr[i][0] = 1;

        for(int i=1; i <= M; i++){
            for(int j=1; j <= N; j++){
                if(i == 1){
                    if(j > C){
                        resArr[i][j] = -1;
                        continue;
                    } else {
                        resArr[i][j] = 1;
                    }
                }

                for(int k=j; k >= 0; k--) {
                    if(k > C) {
                        continue;
                    }
                    int tmp = resArr[i - 1][j -k] % (1000000000 + 7);
                    if(tmp != -1)
                        resArr[i][j] += tmp % (1000000000 + 7);
                }
            }
        }
        return resArr[M][N];
    }

}
