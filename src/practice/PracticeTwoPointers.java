package practice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PracticeTwoPointers {

    public static void main(String[] args) {
        System.out.println(numberOfSubarrays(new int[]{1,1,2,1,1}, 3));
        System.out.println(numberOfSubarrays(new int[]{2,4,6}, 1));
        System.out.println(numberOfSubarrays(new int[]{2,2,2,1,2,2,1,2,2,2}, 1));
        System.out.println(numberOfSubarrays(new int[]{2,2,2,1,2,2,1,2,2,2}, 2));
        System.out.println(numberOfSubarrays(new int[]{2,2,2,1,2,2,1}, 2));
        System.out.println(numberOfSubarrays(new int[]{2,2,2,1,2,2,1,2,2,2}, 3));
        System.out.println(numberOfSubarrays1(new int[]{1,1,2,1,1}, 3));
    }

    public int findRadius(int[] houses, int[] hs) {
        Arrays.sort(houses);
        Arrays.sort(hs);
        int res = 0, i = 0;
        for (int h : houses) {
            while (i < hs.length - 1 && hs[i] <= h) i++;
            res = Math.max(res, Math.min(Math.abs(h - (i > 0 ? hs[i - 1] : hs[0])), Math.abs(hs[i] - h)));
        }
        return res;
    }

    public static int numberOfSubarrays(int[] nums, int k) {
        int res = 0;
        int[] dp = new int[nums.length];
        Map<Integer,Integer> count = new HashMap<>();
        count.put(0,1);
        for(int i=0; i<nums.length; i++){
            int key = nums[i];
            dp[i] = (i-1>=0?dp[i-1]:0) + key%2;
            System.out.print(dp[i]+ " ");
            if(count.containsKey(dp[i]-k)){
                res += count.get(dp[i]-k);
            }
            count.put(dp[i],count.getOrDefault(dp[i],0)+1);
        }
        System.out.println(count);
        return res;
    }

    public static int numberOfSubarrays1(int[] nums, int k) {
        int pre = 0, i = 0, j = 0, res = 0;
        while (j < nums.length) {
            k = nums[j++] % 2 == 0 ? k : k - 1;
            if (k == 0) {
                int counter = 0;
                while (i < nums.length && k == 0) {
                    counter++;
                    k = nums[i++] % 2 == 0 ? k : k + 1;
                }
                pre = counter;
                res += pre;
            } else {
                res += pre;
            }
        }
        return res;
    }
}
