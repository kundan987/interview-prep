package practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class TrieNode{

    TrieNode[] links;

    boolean isEnd;

    private final int R=26;

    public TrieNode() {
        this.links = new TrieNode[R];
    }

    public boolean containsKey(char ch){
        return links[ch-'a'] != null;
    }

    public TrieNode get(char ch){
        return links[ch-'a'];
    }

    public void put(char ch, TrieNode node){
        links[ch-'a'] = node;
    }

    public void setEnd() {
        isEnd = true;
    }

    public boolean isEnd() {
        return isEnd;
    }
}
public class Trie {

    TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }

    public static void main(String[] args) {
        String sent = "My name is khan";
        String[] words = sent.split(" ");

       // List<String> words = Arrays.asList(sent.split(" "));
    }

    public void insert(String word) {
        TrieNode node = root;

        for(int i=0; i<word.length(); i++){
            char ch = word.charAt(i);
            if (!node.containsKey(ch)){
                node.put(ch, new TrieNode());
            }
            node = node.get(ch);
        }
        node.setEnd();
    }

    public boolean search(String word) {

        TrieNode node = searchPrefix(word);
        return node != null && node.isEnd;
    }

    public boolean startsWith(String prefix) {
        TrieNode node = searchPrefix(prefix);
        return node != null;
    }

    private TrieNode searchPrefix(String word) {
        TrieNode node = root;
        for (int i =0; i< word.length(); i++){
            char ch = word.charAt(i);
            if (node.containsKey(ch))
                node = node.get(ch);
            else
                return null;
        }
        return node;
    }
}
