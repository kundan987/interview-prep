package practice;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class BacktrackingWordSearch {

    public static void main(String[] args) {
        BacktrackingWordSearch wordSearch = new BacktrackingWordSearch();
        char[][] board = new char[][]
                {
                {'A','B','C','E'},
                {'S','F','C','S'},
                {'A','D','E','E'}
                };
        char[][] copy = board.clone();
        wordSearch.printBoard(copy, copy.length, copy[0].length);
        board[0][0] = '*';
        wordSearch.printBoard(copy, copy.length, copy[0].length);

           /*System.out.println(wordSearch.exist(board, "ABCCED"));
        System.out.println(wordSearch.exist(board, "SEE"));
     System.out.println(wordSearch.exist(board, "ABCB"));
        System.out.println(wordSearch.exist(board, "BFCC"));
        */
        Collections.sort(null, Collections.reverseOrder());


    }

    private boolean exist(char[][] board, String word) {
        boolean flag = false;
        for(int i=0; i< board.length; i++)
            for (int j=0;j<board[i].length;j++){
                if(word.charAt(0) == board[i][j]){
                    char[][] cp = board;
                    flag = wordSearchUtil(Arrays.copyOf(board, board.length), word, i,j, board.length, board[0].length, 0, "");
                    System.out.println(word + " " + flag);
                    printBoard(board, board.length, board[0].length);
                    if (flag){

                        return flag;
                    }
                }
            }

        return flag;
    }

    void printBoard(char[][] board, int row, int col){
        for(int i=0; i<row;i++){
            for(int j=0; j<col; j++){
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    private boolean wordSearchUtil(char[][] board, String word, int x, int y, int row, int col, int index, String current) {
        if(word.equals(current)){
            return true;
        }

        if (index < word.length() && isSafe(board,x,y,row,col, word.charAt(index))){
            char ch = board[x][y];
            board[x][y] ='*';
            if (wordSearchUtil(board,word,x+1,y, row, col, index+1, current+word.charAt(index))) //down
                return true;

            if (wordSearchUtil(board,word,x,y-1, row, col, index+1, current+word.charAt(index))) // left
                return true;

            if (wordSearchUtil(board, word, x,y+1, row, col, index+1, current+word.charAt(index))) //right
                return true;

            if (wordSearchUtil(board,word,x-1,y, row, col, index+1, current+word.charAt(index))) //up
                return true;

            board[x][y]=ch;
        }
        return false;
    }

    private boolean isSafe(char[][] board, int x, int y, int row, int col, char ch) {
        return (x>=0 && x<row && y>=0 && y<col && board[x][y]==ch);
    }
}
