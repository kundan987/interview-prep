package practice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Practice {
    public static void main(String[] args) {
        int[] a = {0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1};
        int[] b = {0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1};
        String s = "bcdxabcdy";
        String pattern = "bcdyabcdx";

       // System.out.println("Length of longest ones : " + longestLengthOnes(a,2));
        //System.out.println("Length of longest ones : " + longestLengthOnes(b,3));
        System.out.println("Pattern matched ? "+patternMatched(s, pattern));
        System.out.println("Pattern matched ? "+patternMatched("abbac", "ad"));
        System.out.println("find String Anagrams  "+findStringAnagrams("abbcabc", "abc"));
    }

    private static List<Integer> findStringAnagrams(String s, String pattern) {
        Map<Character, Integer> map = new HashMap<>();
        for (Character c : pattern.toCharArray())
            map.put(c, map.getOrDefault(c,0)+1);
        int matched =0, start=0;
        List<Integer> result = new ArrayList<>();
        for (int i=0; i< s.length(); i++){
            char rightChar = s.charAt(i);
            if (map.containsKey(rightChar)){
                map.put(rightChar, map.get(rightChar)-1);

                if (map.get(rightChar) == 0)
                    matched++;
            }

            if (matched == map.size()){
                System.out.println("matched final "+matched);
                result.add(start);}

            if (i >= pattern.length()-1){
                Character leftChar = s.charAt(start++);
                if (map.containsKey(leftChar)){
                    if (map.get(leftChar)==0)
                        matched--;
                    System.out.println(matched);
                    map.put(leftChar, map.get(leftChar)+1);
                }
            }
        }
        return result;
    }

    private static boolean patternMatched(String s, String pattern) {
        Map<Character, Integer> map = new HashMap<>();
        for (Character c : pattern.toCharArray())
            map.put(c, map.getOrDefault(c,0)+1);
        int matched =0, start=0;
        for (int i=0; i< s.length(); i++){
            char rightChar = s.charAt(i);
            if (map.containsKey(rightChar)){
                map.put(rightChar, map.get(rightChar)-1);

                if (map.get(rightChar) == 0)
                    matched++;
            }

            if (matched == map.size())
                return true;

            if (i+1 > pattern.length()){
                Character leftChar = s.charAt(start++);
                if (map.containsKey(leftChar)){
                    if (map.get(leftChar)==0)
                        matched--;
                    map.put(leftChar, map.get(leftChar)+1);
                }
            }
        }
        return false;
    }

    private static int longestLengthOnes(int[] a, int k) {
        int maxCount=0, count=0, start=0;
        for (int i=0; i< a.length; i++){
            System.out.println("i - "+i);
            maxCount = Math.max(count,maxCount);
            if (a[i] != 1 && k==0){

                System.out.println("i - "+i);
                System.out.println("maxCount - "+maxCount);
                while (a[start] !=0){
                    start++;
                    count--;
                }
                k++;
                count--;
                start++;
                System.out.println("start - "+start);
                System.out.println("Count - "+count);
            }
            if (a[i]==0)
                k--;
            count++;

        }
        return maxCount;
    }
}
