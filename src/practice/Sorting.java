package practice;

import java.util.Arrays;

public class Sorting {
    public static void main(String[] args) {
        int[] a = {9,4,8,1,3,6,2,5};
        int[] b = {9,4,8,1,3,6,2,5};
        int[] c = {9,4,8,1,3,6,2,5};
        int[] d = {9,4,8,1,3,6,2,5};

        System.out.println(Arrays.toString(bubbleSort(a)));
        System.out.println(Arrays.toString(selectionSort(b)));
        System.out.println(Arrays.toString(insertionSort(c)));
        mergeSort(d, 0, d.length-1);
        System.out.println(Arrays.toString(d));
    }

    private static void mergeSort(int[] a, int start, int end) {
        if (start<end){
            int mid = (end+start)/2;
            mergeSort(a,start,mid);
            mergeSort(a,mid+1,end);
            merge(a, start, mid, end);
        }
    }

    private static void merge(int[] a, int start, int mid, int end) {
        int ln = mid-start+1;
        int rn = end-mid;
        int[] left = new int[ln];
        int[] right = new int[rn];
        for (int i=0; i< ln; i++)
            left[i] = a[start+i];
        for (int i=0; i< rn; i++)
            right[i] = a[mid+1+i];
        int i =0, j=0, k=start;
        System.out.println(Arrays.toString(left));
        System.out.println(Arrays.toString(right));
        while (i< ln && j < rn){
            if (left[i]<right[j]){
                a[k] = left[i];
                k++;
                i++;
            }else {
                a[k] = right[j];
                k++;
                j++;
            }
        }
        while (i<ln)
            a[k++] = left[i++];
        while (j<rn)
            a[k++] = right[j++];

    }

    private static int[] insertionSort(int[] a) {
        int iteration=0;
        for (int i=1;i< a.length;i++){
            int j=i-1;
            int current = a[i];
            while (j>=0 && a[j]>current){
                iteration++;
                a[j+1] = a[j];
                j--;
            }
            a[j+1]=current;
        }
        System.out.println("insertionSort iteration : "+iteration);
        return a;
    }

    private static int[] selectionSort(int[] a) {
        int iteration=0;
        for (int i=0;i< a.length;i++){
            int minIdx = i;
            for (int j=i+1; j< a.length; j++){
                if (a[j]<a[minIdx]){
                    iteration++;
                    minIdx =j;
                }

            }
            int temp = a[i];
            a[i] = a[minIdx];
            a[minIdx] = temp;
        }
        System.out.println("selectionSort iteration : "+iteration);
        return a;
    }

    private static int[] bubbleSort(int[] a) {
        int iteration=0;
        for (int i=0;i< a.length-1;i++){
            for (int j=0; j< a.length-1-i;j++){
                if (a[j] > a[j+1]){
                    iteration++;
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        System.out.println("bubbleSort iteration : "+iteration);
        return a;
    }
}
