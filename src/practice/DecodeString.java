package practice;

public class DecodeString {

    public String decodeString(String s) {
        return dfs(s, 0)[0];
    }

    private String[] dfs(String s, int i){
        StringBuilder res = new StringBuilder();
        int times = 0;
        while(i<s.length()){
            if(s.charAt(i) >= '0' && s.charAt(i) <= '9')
                times = times *10 + Integer.parseInt(String.valueOf(s.charAt(i)));
            else if(s.charAt(i) == '['){
                // dfs at a new level
                String[] temp = dfs(s, i+1);
                i = Integer.parseInt(temp[0]);
                while(times>0){
                    res.append(temp[1]);
                    times--;
                }
            }
            else if(s.charAt(i) == ']')//return res and index
                return new String[]{String.valueOf(i), res.toString()};
            else
                res.append(String.valueOf(s.charAt(i)));
            i++;
        }
        return new String[] {res.toString()};
    }
}
