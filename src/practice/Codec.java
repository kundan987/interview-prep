package practice;

import java.util.ArrayList;
import java.util.List;

/**
 * Definition for a binary tree node.
 *
 */

class TreeNode {
     int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}
public class Codec {
    int preIndex;

    public Codec(){
        this.preIndex=0;
    }

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root == null)
            return null;
        TreeNode in = root;
        TreeNode pre = root;
        StringBuilder sb = new StringBuilder();
        List<Integer> inorder = new ArrayList<>();
        inorder(in,inorder);
        List<Integer> preorder = new ArrayList<>();
        preorder(pre,preorder);
        for(Integer num : inorder)
            sb.append(num).append(",");
        sb.append(",");
        for(Integer num : preorder)
            sb.append(num).append(",");
        return new String(sb);
    }

    public void inorder(TreeNode root, List<Integer> inorder){
        if(root != null){
            inorder(root.left, inorder);
            inorder.add(root.val);
            inorder(root.right, inorder);
        }
    }

    public void preorder(TreeNode root, List<Integer> preorder){
        if(root != null){
            preorder.add(root.val);
            preorder(root.left, preorder);
            preorder(root.right, preorder);
        }
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if(data == null)
            return null;
        String[] array = data.substring(0,data.length()-1).split(",,");
        String[] in = array[0].split(",");
        String[] pre = array[1].split(",");
        int[] inorder = new int[in.length];
        int[] preorder = new int[pre.length];
        for(int i=0;i< in.length;i++)
            inorder[i] = Integer.valueOf(in[i]);
        for(int i=0;i< pre.length;i++)
            preorder[i] = Integer.valueOf(pre[i]);
        return buildTree(preorder, inorder,0, inorder.length-1);

    }
    public TreeNode buildTree(int[] pre, int[] in, int inStart, int inEnd){
        if(inStart>inEnd)
            return null;
        TreeNode node = new TreeNode(pre[preIndex++]);
        if(inStart==inEnd)
            return node;
        int index = search(in, inStart, inEnd, node.val);
        node.left = buildTree(pre, in, inStart, index-1);
        node.right = buildTree(pre, in, index+1, inEnd);
        return node;
    }

    public int search(int[] in, int start, int end, int key){
        for(int i= start;i<=end;i++){
            if(in[i]==key)
                return i;
        }
        return start;
    }

    public static void main(String[] args) {
        Codec codec = new Codec();
      //  codec.deserialize(codec.serialize(root));
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));
