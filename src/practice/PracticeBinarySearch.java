package practice;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PracticeBinarySearch {

    public static void main(String[] args) {
        System.out.println(findClosestElements(Arrays.asList(1,2,3,3,4,4,5),3,3 ));

        System.out.println(Collections.binarySearch(Arrays.asList(1,3,5,8,11), 12));
    }

    public static List<Integer> findClosestElements(List<Integer> arr, int k, int x) {

        Collections.sort(arr, (a,b) -> a == b ? a-b : Math.abs(a-x) - Math.abs(b-x));
        System.out.println(arr);
        arr = arr.subList(0, k);
        //Collections.sort(arr);
        return arr;
    }
}
