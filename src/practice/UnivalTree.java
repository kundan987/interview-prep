package practice;

public class UnivalTree {

    TreeNode root;

    static class TreeNode{
        int data;
        TreeNode left;
        TreeNode right;

        public TreeNode(int data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        UnivalTree tree = new UnivalTree();
        tree.root = new TreeNode(0);
        tree.root.left = new TreeNode(1);
        tree.root.right = new TreeNode(0);
        tree.root.right.right = new TreeNode(0);
        tree.root.right.left = new TreeNode(1);
        tree.root.right.left.left = new TreeNode(1);
        tree.root.right.left.right = new TreeNode(1);

        int count = tree.countUnival(tree.root);
        System.out.println(count);
    }

    private int countUnival(TreeNode node) {

        if (node==null)
            return 0;
        else{
            int lcount = countUnival(node.left);
            int rcount = countUnival(node.right);
            if ((node.left == null || node.left.data == node.data) && (node.right == null || node.right.data == node.data) )
                return lcount+rcount+1;
            else
                return lcount+rcount;
        }
    }
}
