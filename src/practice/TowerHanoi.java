package practice;

public class TowerHanoi {
    public static void main(String[] args) {
        move(4,'A', 'C', 'B');
    }


    static void move(int n, char from, char to, char inter){

        if (n==1)
            System.out.println("moved disc 1 from "+ from + " to " + to);
        else {
            move(n-1, from, inter, to);
            System.out.println("moved disc " + n + " from "+ from + " to " + to);
            move(n-1, inter, to, from);
        }
    }
}
