package practice;

import java.util.Arrays;

public class LogDataStructure {

    public int maxSize;
    public  String name;
    private int[] circularBuffer;
    private int currentIndex;

    public LogDataStructure(int maxSize) {
        this.maxSize = maxSize;
        this.circularBuffer = new int[maxSize];
        this.currentIndex = 0;
    }

    public void record(int orderId){
        circularBuffer[currentIndex] = orderId;
        currentIndex = (currentIndex+1)%maxSize;
    }

    public int getLast(int i){
        System.out.println(Arrays.toString(circularBuffer));
        return circularBuffer[(currentIndex-i+maxSize)%maxSize];
    }

    public static void main(String[] args) {
        LogDataStructure logDataStructure = new LogDataStructure(5);
        logDataStructure.record(1);
        logDataStructure.record(2);
        logDataStructure.record(3);
        logDataStructure.record(4);
        logDataStructure.record(5);
        logDataStructure.record(6);
        logDataStructure.record(7);
        System.out.println(logDataStructure.getLast(3));
    }
}
