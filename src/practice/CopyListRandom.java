package practice;
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}
public class CopyListRandom {

// create a new node after each node. which is our deep copy
// list will become twice the size
// Now we have all new nodes, list is twice the size now.
// Assign the random nodes properly.
// now we have randoms for new list assigned well.
// create a new list and break it up.

    public Node copyRandomList(Node head) {

        if(head == null) return head;

        Node cur = head;

        // create a new node after each node. which is our deep copy
        // list will become twice the size
        while(cur!=null)
        {
            Node next = cur.next;

            cur.next = new Node(cur.val);
            cur.next.next = next;
            cur = next;
        }

        // Now we have all new nodes, list is twice the size now.
        // Assign the random nodes properly.

        cur = head;

        while(cur!=null)
        {
            cur.next.random = cur.random!=null? cur.random.next : null;
            cur = cur.next.next;
        }

        // now we have randoms for new list assigned well.
        // create a new list and break it up.

        Node res = head.next;

        cur = head;
        Node curCopy = res;

        while(cur!=null)
        {
            cur.next = curCopy.next;
            cur = cur.next;
            curCopy.next = cur!=null ? cur.next : null;
            curCopy = curCopy.next;
        }

        return res;
    }
}
