package practice;

import java.util.*;

public class BacktrackingPermutation {

    public static void main(String[] args) {

        BacktrackingPermutation backtrackingPermutation = new BacktrackingPermutation();
        System.out.println(backtrackingPermutation.permuteUnique(new int [] {1,1,2}));
        System.out.println(backtrackingPermutation.permuteUniqueWithoutSet(new int [] {1,2,1}));
        System.out.println(backtrackingPermutation.generateParenthesis(3));
    }

    public List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        helper(0,n,new StringBuilder(),0,0,result);
        return result;
    }

    private void helper(int index, int n, StringBuilder string, int openCount, int closeCount, List<String> result){
        if(openCount==n && closeCount==n){
            result.add(string.toString());
            return;
        } else {

            for(int i=index; i<n; i++){
                if(openCount< n){
                    string.append("(");
                    helper(index,n,string,openCount+1,closeCount, result);

                }
                if(closeCount< openCount){
                    string.append(")");
                    helper(index,n,string,openCount,closeCount+1, result);

                }
                string.substring(0, string.length()-1);
            }
        }
    }


    public List<List<Integer>> permuteUniqueWithoutSet(int[] nums) {
        List<List<Integer>> res= new ArrayList<>();
        if(nums==null || nums.length==0) return res;
        Arrays.sort(nums);
        boolean[] used= new boolean[nums.length];
        List<Integer> permutation = new ArrayList<>();
        helper(used, nums, permutation, res);
        return res;
    }
    private static void helper(boolean[] used, int[] nums, List<Integer> permutation, List<List<Integer>> res) {
        if(permutation.size()==nums.length)
        {
            res.add(new ArrayList<>(permutation));
            return;
        }

        for(int i=0; i<nums.length; i++)
        {
            if(used[i]) continue;
            used[i]=true;
            permutation.add(nums[i]);
            helper(used, nums, permutation, res);
            permutation.remove(permutation.size()-1);
            used[i]=false;
            /*Removing duplicates*/
            while(i+1<nums.length && nums[i]==nums[i+1]) ++i;
        }
    }

    Set<List<Integer>> list;
    Set<Integer> s;

    public List<List<Integer>> permuteUnique(int[] nums) {
        list = new HashSet();
        s = new HashSet();
        track(nums, new ArrayList());
        return new ArrayList(list);
    }

    private void track(int[] nums, List<Integer> l) {

        if (l.size() == nums.length) {
            list.add(new ArrayList(l));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (!s.contains(i)) {
                s.add(i);
                l.add(nums[i]);
                track(nums, l);
                s.remove(i);
                l.remove(l.size() - 1);
            }

        }
    }
}
