package practice;

import java.util.*;
import java.util.LinkedList;

public class BinaryTreeLeetCode {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        new ArrayList<>(set);
    }

        // Encodes a tree to a single string.
        public String serialize(TreeNode root) {
            StringBuilder sb = new StringBuilder();
            Queue<TreeNode> queue = new LinkedList<>();
            queue.add(root);

            while(!queue.isEmpty()){
                int levelSize = queue.size();
                for(int i=0; i< levelSize;i++){
                    TreeNode node = queue.poll();
                    sb.append(node.val).append(",");
                    if(node.left != null)
                        queue.add(node.left);
                    if(node.right != null)
                        queue.add(node.right);
                    if(node.left == null)
                        sb.append("null").append(",");
                    if(node.right == null)
                        sb.append("null").append(",");
                }
            }
            System.out.println(new String(sb));
            return new String(sb);
        }

        // Decodes your encoded data to tree.
        public TreeNode deserialize(String data) {
            return null;
        }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));

