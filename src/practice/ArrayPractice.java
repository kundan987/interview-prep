package practice;

import java.util.*;

public class ArrayPractice {

    public static void main(String[] args) {
        /*ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(-1);
        System.out.println("Maximum Absolute Difference : " + maxArr(new int[]{1,3,-1}));
        System.out.println("Maximum Absolute Difference : " + maxArr(list));
        System.out.println("Length " + longestOnes(new int[]{1,1,1,0,0,0,1,1,1,1,0}, 2));
        System.out.println("length : " +lengthOfLongestSubstring("abcabcbb"));
        System.out.println("length : " +lengthOfLongestSubstring("pwwkew"));
        System.out.println("length : " +lengthOfLongestSubstring("bbbbb"));
        System.out.println(Arrays.toString(maxSlidingWindowNew(new int[]{1,3,-1,-3,5,3,6,7}, 3)));
        System.out.println(Arrays.toString(maxSlidingWindowNew(new int[]{3,1,-1,-3,1,2,6,7}, 3)));
        System.out.println(Arrays.toString(maxSlidingWindowNew(new int[]{9,10,9,-7,-4,-8,2,-6}, 5)));
        System.out.println("Min Window " + minWindow("ADOBECODEBANC", "ABC"));

        System.out.println(isPalindrome("0P"));
        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
        System.out.println(isPalindrome("race a car"));*/

  /*      System.out.println(isValid("()"));
        System.out.println(isValid("()[]{}"));
        System.out.println(isValid("(]"));
        System.out.println(isValid("([)]"));
        System.out.println(isValid("{[]}"));
        System.out.println(isValid("]"));
        List<List<Integer>> result = new ArrayList<>();
        result.add(new ArrayList<>(Arrays.asList(1)));
        System.out.println(result);*//*

        Set<Integer> set = new HashSet<>();
        System.out.println(set.add(1));
        System.out.println(set.add(2));
        System.out.println(set.add(3));
        List<Integer> list = new ArrayList<>(set);
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));
        System.out.println(getRandomNumber(0,list.size()));*/
/*
        String s = "kundan";

        char[] ar = s.toCharArray();
        Arrays.sort(ar);
        Map<String, List<String>> map = new HashMap<>();
        List list = new ArrayList<>(map.values());

        System.out.println((char)(Integer.valueOf('a')+'a'-1));
        System.out.println((char)('a'+1));
        System.out.println(Integer.parseInt("1"));*//*
        int n = 123456789;
        int c=0;
        while(n>1){
            c++;
            n/=2;
        }
        System.out.println(c);*/

        String a= "50";
        String b = "51";
        System.out.println(b.compareTo(a));



    }
    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public static boolean isValid(String s) {
        Stack<Character> open = new Stack<>();
        for(Character ch : s.toCharArray()){
            if(ch == '(' || ch == '{' || ch == '[')
                open.push(ch);
            else if(!open.isEmpty() && (ch == ')' || ch == '}' || ch == ']')){
                Character openChar = open.peek();
                if(openChar == '(' && ch != ')')
                    return false;
                else if(openChar == '{' && ch != '}')
                    return false;
                else if(openChar == '[' && ch != ']')
                    return false;
                else
                    open.pop();
            } else
                return false;
        }
        return true;
    }
    public static boolean isPalindrome(String s) {
        int j = s.length()-1;
        int i=0;
        s = s.toLowerCase();
        while(i<j){
            System.out.println(s.charAt(i));
            System.out.println(s.charAt(j));
            if(Character.isLetterOrDigit(s.charAt(i)) && Character.isLetterOrDigit(s.charAt(j)) && s.charAt(i) != s.charAt(j)){

                return false;
            }else if(!Character.isLetterOrDigit(s.charAt(i)))
                i++;
            else if(!Character.isLetterOrDigit(s.charAt(j)))
                j--;
            else{
                i++;
                j--;
            }
        }
        return true;
    }

    public static String minWindow(String s, String t) {
        HashMap<Character, Integer> frequencyMap = new HashMap<>();
        for (Character c : t.toCharArray())
            frequencyMap.put(c, frequencyMap.getOrDefault(c,0)+1);

        int minLength=s.length()+1, subStrStart=0;
        int start =0, pattern=0;
        for (int end=0; end < s.length(); end++){
            char rightChar = s.charAt(end);

            if (frequencyMap.containsKey(rightChar)){
                frequencyMap.put(rightChar, frequencyMap.get(rightChar)-1);
                System.out.println(frequencyMap);

                if (frequencyMap.get(rightChar) == 0)
                    pattern++;
            }

            while (pattern == frequencyMap.size()){
                if (end-start+1 < minLength){
                    minLength = end-start+1;
                    subStrStart = start;
                    System.out.println(s.substring(subStrStart, subStrStart+minLength));
                }

                char leftChar = s.charAt(start++);
                if (frequencyMap.containsKey(leftChar)){
                    if (frequencyMap.get(leftChar)==0)
                        pattern--;
                    frequencyMap.put(leftChar, frequencyMap.get(leftChar)+1);
                }
            }

        }

        return minLength>s.length() ? "" : s.substring(subStrStart, subStrStart+minLength);
    }

    public static int[] maxSlidingWindowNew(int[] nums, int k) {
        int[] result = new int[nums.length-k+1];
        int max=Integer.MIN_VALUE;
        for(int i=0; i<k; i++)
            max = Math.max(max, nums[i]);
        result[0] = max;
        int l=1, start=0;
        for(int i=k; i<nums.length; i++){
            if (max == nums[start]){
                max = nums[start+1];
                for (int j=start+1; j<=i;j++)
                    max = Math.max(max, nums[j]);
            } else {
                max = Math.max(max, nums[i]);
            }
            start++;
            result[l++] = max;
        }
        return result;
    }

    public static int[] maxSlidingWindow(int[] nums, int k) {
        int[] result = new int[nums.length-k+1];
        int max=Integer.MIN_VALUE,secondMax=Integer.MIN_VALUE, j=0, maxIndex=-1, secondIndex=-1;
        for(int i=0; i<nums.length; i++){
            if (i<k-1){
                if (max <= nums[i]){
                    secondMax = max;
                    max= nums[i];
                    secondIndex=maxIndex;
                    maxIndex=i;
                }else if (secondMax <= nums[i]){
                    secondMax = nums[i];
                    secondIndex=i;
                }
            } else {
                if (maxIndex==i-k){
                    max=secondMax;
                    maxIndex=secondIndex;
                    secondMax=Integer.MIN_VALUE;
                }
                if (max <= nums[i]){
                    secondMax = max;
                    max= nums[i];
                    secondIndex=maxIndex;
                    maxIndex=i;
                }else if (secondMax <= nums[i]){
                    secondMax = nums[i];
                    secondIndex=i;
                }


                result[j++]=max;
            }
        }
        return result;
    }

    public static int lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> frequencyMap = new HashMap<>();
        int start=0, maxLength=0;
        for (int end=0; end< s.length(); end++){
            char rightChar = s.charAt(end);
            frequencyMap.put(rightChar, frequencyMap.getOrDefault(rightChar, 0)+1);
            while (frequencyMap.get(rightChar)>1){
                char leftChar = s.charAt(start++);
                frequencyMap.put(leftChar, frequencyMap.get(leftChar)-1);
                if (frequencyMap.get(leftChar)==0)
                    frequencyMap.remove(leftChar);
            }
            maxLength = Math.max(maxLength, end-start+1);
        }
        return maxLength;
    }

    public static int longestOnes(int[] A, int K) {
        int result=0, countOnes=0, start=0;
        for(int i=0; i<A.length; i++){
            if (A[i] == 1)
                countOnes++;
            if (i-start+1-countOnes>K){
                if (A[start] == 1)
                    countOnes--;
                start++;

            }
            result = Math.max(result, i-start+1);
        }
        return result;
    }

    public static int maxArr(int[] A) {
        int max1 =Integer.MIN_VALUE;
        int min1 =Integer.MAX_VALUE;
        int max2 =Integer.MIN_VALUE;
        int min2 =Integer.MAX_VALUE;
        for(int i=0; i<A.length;i++){
            max1 = Math.max(max1, A[i]+1);
            min1 = Math.min(min1, A[i]+1);
            max2 = Math.max(max2, A[i]-1);
            min2 = Math.min(min2, A[i]-1);
        }
        return Math.max((max1-min1), (max2-min2));
    }

    public static int maxArr(ArrayList<Integer> A) {
        int max1=Integer.MIN_VALUE;
        int min1=Integer.MAX_VALUE;
        int max2=Integer.MIN_VALUE;
        int min2=Integer.MAX_VALUE;
        for(int i=0;i<A.size();i++)
        {
            max1=Math.max(max1,A.get(i)+i);
            min1=Math.min(min1,A.get(i)+i);
            max2=Math.max(max2,A.get(i)-i);
            min2=Math.min(min2,A.get(i)-i);
        }
        return Math.max((max1-min1),(max2-min2));
    }
}

class GFG {

    // Function to count the no of ways
    static int countways(ArrayList<Integer> B, int A)
    {
        int cnt[] = new int[B.size()];
        int s = 0;

        for (int i = 0 ; i < A ; i++)
            s += B.get(i);

        if (s % 3 != 0)
            return 0;

        s /= 3;

        int ss = 0;

        for (int i = A-1; i >= 0 ; i--)
        {
            ss += B.get(i);
            if (ss == s)
                cnt[i] = 1;
        }

        for (int i = A-2 ; i >= 0 ; i--)
            cnt[i] += cnt[i + 1];

        int ans = 0;
        ss = 0;

        for (int i = 0 ; i+2 < A ; i++)
        {
            ss += B.get(i);
            if (ss == s)
                ans += cnt[i + 2];
        }
        return ans;
    }


}

