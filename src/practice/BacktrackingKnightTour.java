package practice;

public class BacktrackingKnightTour {
    static int N = 8;

    public static void main(String[] args) {
        knightTour();
    }
    static void knightTour(){
        int[][] board = new int[N][N];

        for(int i=0; i<N;i++)
            for(int j=0; j<N; j++)
                board[i][j]=-1;

        int[] xMove = {2,1,-1,-2,-2,-1,1,2};
        int[] yMove = {1,2,2,1,-1,-2,-2,-1};

        board[0][0] =0;

        if(!knightTourUtil(0,0,1,board,xMove,yMove))
            System.out.println("Not possible");
        else
            for(int i=0; i<N;i++){
                for(int j=0; j<N; j++){
                    System.out.print(board[i][j] + " ");
                }
                System.out.println();
            }
    }

    private static boolean knightTourUtil(int x, int y, int moveCount, int[][] board, int[] xMove, int[] yMove) {

        if (moveCount == N*N)
            return true;

        for(int i=0; i<N; i++){
            int nextX = x + xMove[i];
            int nextY = y + yMove[i];
            if(safe(nextX, nextY, board)){
                board[nextX][nextY] = moveCount;
                if (knightTourUtil(nextX,nextY,moveCount+1,board,xMove,yMove))
                    return true;
                else
                    board[nextX][nextY] = -1;
            }
        }
        return false;
    }

    private static boolean safe(int nextX, int nextY, int[][] board) {
        return  (nextX>=0 && nextX < N && nextY>=0 && nextY<N && board[nextX][nextY] == -1);
    }
}
