package practice;

import java.util.Stack;

class MinStack {
    Stack<Integer> stack;
    Stack<Integer> stackCopy;
    Stack<Integer> minStack;
    /** initialize your data structure here. */
    public MinStack() {
        stack = new Stack<>();
        minStack = new Stack<>();
        stackCopy = new Stack<>();
    }

    public void push(int x) {
        this.stack.push(x);
        if(!minStack.isEmpty() && minStack.peek() < x){
            while(!minStack.isEmpty() && minStack.peek()< x){
                stackCopy.push(minStack.pop());
            }
        }
        this.minStack.push(x);
        while(!stackCopy.isEmpty())
            minStack.push(stackCopy.pop());

    }

    public void pop() {

        while(!minStack.isEmpty() && !stack.isEmpty() && minStack.peek() != stack.peek()){
            stackCopy.push(minStack.pop());
        }
        stack.pop();
        minStack.pop();
        while(!stackCopy.isEmpty())
            minStack.push(stackCopy.pop());
    }

    public int top() {
        return this.stack.peek();
    }

    public int getMin() {
        return minStack.peek();
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
