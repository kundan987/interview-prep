package design;

public class Base62 {

    public static void main(String[] args) {
        System.out.println(encode(100000));
    }

    private static String encode(int n) {
        String s = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String hash = "";
        while(n>0){
            hash = s.charAt(n%62)+hash;
            n/=62;
        }
        return hash;
    }
}
