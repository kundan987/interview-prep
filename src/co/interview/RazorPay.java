package co.interview;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class RazorPay {

    public static void main(String[] args) {
        //System.out.println(maxSumPair(new int[]{2,5,4,8,7}));
        System.out.println(maxSumPair(new Integer[]{6 , 3, 9, 7, 4, 8}));
        System.out.println(maxSumPair(new Integer[]{6 , 3, 9, 7, 4, 8,1}));
        System.out.println(maxSumPair(new Integer[]{6, 3, 9, -5}));
        System.out.println(maxSumPair(new Integer[]{5, 6, -5, -6, -7}));
        /*
        5, 6, 0, -5, -6, -7
        6, 5, 0, -5, -6, -7
        n = 6-1 = 5
        i=0, j =5
        0<=5
        6*5 ->i=2
        30+42
        * */

    }

    static int maxSumPair(Integer[] nums){
        Arrays.sort(nums, Collections.reverseOrder());
        int n = nums.length-1;
        int i =0, j=n;
        int result =0;
        while (i<=j){ // i =0, j =5 ,, i=2 , j=3
            if (i<n && nums[i] >0 && nums[i+1]>0){ // even positive num
                result+= nums[i]*nums[i+1]; // result = 30
                i=i+2; // i=2
            } else if(nums[i] == 0){ // if i have zeros in array // pass
                if (nums[j] < 0){ //if any negative number //pass
                    j--; //j = 2
                }
                i++;
            } else if(nums[i] > 0){ //
                result+=nums[i]; // result = 72+
                i++;
            }

            if (j>= 1 && nums[j] <0 && nums[j-1]<0){
                result+= nums[j]*nums[j-1];// result = 30 + 42 = 72
                j=j-2; // j = 5-2 = 3
            } else if (nums[j] < 0){
                result+=nums[j];
                j--;
            }

        }
        return result;
    }
}
