package co.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class ONode<T> {
    T val;
    List<ONode> children;

    public ONode(T val) {
        this.val = val;
        this.children = new ArrayList<>();
    }
}
class BNode<T> {
    T val;
    BNode left;
    BNode right;

    public BNode(T val) {
        this.val = val;
        this.left = null;
        this.right = null;
    }
}
public class OracleCoding {

    BNode root;


    public static void main(String[] args) {
        OracleCoding coding = new OracleCoding();
        coding.root = new BNode(1);
        coding.root.left = new BNode(2);
        coding.root.right = new BNode(3);
        coding.root.right.right = new BNode(5);
        coding.root.left.left = new BNode(4);

        System.out.println(coding.serialize(coding.root));
       // System.out.println(coding.deserialize());

    }

    //serialize input = root -> output string
    public String serialize(BNode root){
        StringBuilder sb = new StringBuilder();
        dfs(root, sb);
        return sb.toString();
    }

    public void dfs(BNode node, StringBuilder sb){
        if(node == null){
            sb.append("null").append(",");
            return;
        }
        sb.append(node.val);
        sb.append(",");
        dfs(node.left, sb);
        dfs(node.right, sb);
    }

    public BNode deserialize(String input){
        Deque<String> nodes =  new LinkedList<>();
        nodes.addAll(Arrays.asList(input.split(",")));
        return deserialize(nodes);
    }


    private BNode deserialize(Deque<String> nodes) {
        String value = nodes.remove();
        if("null".equals(value)) return null;
        else{
            BNode node = new BNode(Integer.valueOf(value));
            node.left = deserialize(nodes);
            node.right = deserialize(nodes);
            return node;
        }
    }


}
