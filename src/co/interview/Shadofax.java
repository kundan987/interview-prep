package co.interview;

import java.util.Arrays;
import java.util.List;

public class Shadofax {
    public static void main(String[] args) {
        int[] ar = new int[]{8,3,4,5,10,7};
        printArray(ar);
        quickSort(ar, 0, ar.length-1);
        printArray(ar);

        int[] array = new int[]{8,3,4,5,10,7};
        System.out.println(findTarget(array, 5));
    }

    private static List<Integer> findTarget(int[] ar, int key) {
        int start = 0, end = ar.length-1;
        int first =-1, last =-1;
        boolean lastFlag = true, firstFlag = true;
        while (start<end){
            if(ar[start]==key && ar[end]==key)
                return Arrays.asList(start, end);
            else if(firstFlag && ar[start] == key){
                first = start;
                firstFlag = !firstFlag;
                end--;
            } else if(lastFlag && ar[end] == key){
                last = end;
                lastFlag = !lastFlag;
                start++;
            } else {
                start++;
                end--;
            }
        }
        if(first == -1 && last != -1)
            return Arrays.asList(last, -1);

        return Arrays.asList(first, last);
    }

    public static void printArray(int[] ar){
        for (int i=0; i< ar.length; i++)
            System.out.print(ar[i] + " ");
        System.out.println();
    }

    public static void quickSort(int[] ar, int left, int right){

        if(left<right){
            int pivot = pivot(ar, left, right);

            quickSort(ar, left, pivot-1);
            quickSort(ar, pivot+1, right);
        }

    }

    private static int pivot(int[] ar, int left, int right) {

        int p = ar[right];
        int l = left-1;
        for(int i=left; i< right; i++){
            if(ar[i] <=p){
                l++;
                int t = ar[l];
                ar[l] = ar[i];
                ar[i] = t;
            }
        }

        int t = ar[l+1];
        ar[l+1] = ar[right];
        ar[right] = t;
        return l;
    }
}
