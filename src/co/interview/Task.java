package co.interview;

public class Task {
    public synchronized void m1() {
    }
    public synchronized void m2() {
        m1();
    }
    public static void main(String[] args) {
        Task s = new Task();
        s.m2();
        System.out.println("Done");

        System.out.println(Integer.toBinaryString(10));
        char ch = '1';
        int a = ch-'0';
        System.out.println( a);
    }
}
