package co.interview;

import java.util.HashMap;
import java.util.Map;

class Zeta {

    public static void main(String[] args) {
        Zeta zeta = new Zeta();
        System.out.println("Time : " + zeta.largestTimeFromDigits(new int[]{0,2,6,6}));
        System.out.println("Time : " + zeta.largestTimeFromDigits(new int[]{0,2,5,6}));
    }
    public String largestTimeFromDigits(int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int a :A)
            map.put(a, map.getOrDefault(a,0)+1);

        StringBuilder time = new StringBuilder();
        System.out.println(map);
        for(int i=23;i>=0; i--){
            if(i>9){
                int r = i%10;
                int q = i/10;
                if(map.containsKey(r) && map.containsKey(q)){
                    if(r==q && map.get(r)<2)
                        continue;
                    map.put(r, map.get(r)-1);
                    if(map.get(r)==0)
                        map.remove(r);
                    map.put(q, map.get(q)-1);
                    if(map.get(q)==0)
                        map.remove(q);
                    time.append(q).append(r);
                    break;
                }
            } else {
                if(map.containsKey(i)  && map.containsKey(0)){
                    map.put(i, map.get(i)-1);
                    if(map.get(i)==0)
                        map.remove(i);
                    map.put(0, map.get(0)-1);
                    if(map.get(0)==0)
                        map.remove(0);
                    time.append(0).append(i);
                    break;
                }
            }
        }
        time.append(":");
        System.out.println(map);
        for(int i=59;i>=0; i--){
            if(i>9){
                int r = i%10;
                int q = i/10;
                if(map.containsKey(r) && map.containsKey(q)){
                    if(r==q && map.get(r)<2)
                        continue;
                    map.put(r, map.get(r)-1);
                    if(map.get(r)==0)
                        map.remove(r);
                    map.put(q, map.get(q)-1);
                    if(map.get(q)==0)
                        map.remove(q);
                    time.append(q).append(r);
                    break;
                }
            } else {
                if(map.containsKey(i) && map.containsKey(0)){
                    map.put(i, map.get(i)-1);
                    if(map.get(i)==0)
                        map.remove(i);
                    map.put(0, map.get(0)-1);
                    if(map.get(0)==0)
                        map.remove(0);
                    time.append(0).append(i);
                    break;
                }
            }
        }
        System.out.println(map);
        if(map.size()>0)
            return "";
        return time.toString();
    }
}
