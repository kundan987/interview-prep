package co.interview;/*
 * Save this in a file called Main.java and compile it. To test it 
 * create the file `input.txt` in the workspace / Working Directory
 * (For Eclipse/VisualStudio, by default this is the top level project folder)
 */

/* 
   Example file showing how to write a program that reads
   input from `input.txt` in the current directory
   and writes output to `output.txt` in the current directory
*/

/* Do not add a package declaration */
import java.util.*;
import java.io.*;
import java.util.stream.Collectors;

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE */
/* You may add any imports here, if you wish, but only from the 
   standard library */

/* Do not add a namespace declaration */

public class Main {
    public static void main (String[] args) {
        Map<String, Map<Integer, List<String>>> apiVsVersionVsApps = new HashMap<>();
        Set<String> appResult = new HashSet<>();
        int numLines = 0;
        try {
            Scanner in = new Scanner(new BufferedReader(new FileReader("input.txt")));

            /* Here we can read in the input file */
            /* In this example, we're reading all the lines of file
               `input.txt` and then ignoring them. 
               You should modify this part of the
               program to read and process the input as desired */
            while(in.hasNextLine()) {
                String line = in.nextLine();
                if (!line.isEmpty()){
                    String[] ar = line.split(", ");
                    String appName = ar[0];
                    String apiName = ar[1];
                    Integer apiVersion = Integer.valueOf(ar[2].substring(1));
                    if(apiVsVersionVsApps.containsKey(apiName)){
                        Map<Integer, List<String>> versionVsApps = apiVsVersionVsApps.get(apiName);
                        List<String> apps = versionVsApps.getOrDefault(apiVersion, new ArrayList<>());
                        apps.add(appName);
                        versionVsApps.put(apiVersion, apps);
                        apiVsVersionVsApps.put(apiName, versionVsApps);
                    } else {
                        Map<Integer, List<String>> versionVsApps = new HashMap<>();
                        List<String> apps = new ArrayList<>();
                        apps.add(appName);
                        versionVsApps.put(apiVersion, apps);
                        apiVsVersionVsApps.put(apiName, versionVsApps);
                    }
                }
            }
            //System.out.println(apiVsVersionVsApps);
            for(Map.Entry<String, Map<Integer, List<String>>> map : apiVsVersionVsApps.entrySet()){
                List<Integer> sortedList = new ArrayList<>(map.getValue().keySet());
                if(sortedList.size()>1){
                    Collections.sort(sortedList);
                    for (Map.Entry<Integer, List<String>> m : map.getValue().entrySet()){
                        if(m.getKey() == sortedList.get(0)){
                            for (String app : m.getValue())
                            appResult.add(app);
                        }
                    }
                }
            }
            //System.out.println(appResult);
            /* In this example, we're writing `num_lines` to
               the file `output.txt`.
               You should modify this part of the
               program to write the desired output */
            PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("output.txt")));
            for(String app : appResult)
                output.println(app);
            output.close();
        } catch (IOException e) {
            System.out.println("IO error in input.txt or output.txt");
        }
    }
}
