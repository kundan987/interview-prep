package co.interview;

// Modify this such that the following happens.
// 1. The output is like below:
// PING
// PONG
// PING
// PONG
// PING
// PONG
// PING
// PONG
// PING
// PONG
// ....
// ....
//
// 2. "PING" should always be printed first.
class PingPongPrinter{

    boolean isPingPrinted = false;

    synchronized void printPing() {
        while (isPingPrinted) {
            try {
                wait();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("PING");

        isPingPrinted = !isPingPrinted;

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        notify();
    }

    synchronized void printPong() {
        while (!isPingPrinted) {
            try {
                wait();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("PONG");

        isPingPrinted = !isPingPrinted;

        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        notify();
    }
}

class PingThread extends Thread {

    PingPongPrinter printer;

    public PingThread(PingPongPrinter printer) {
        this.printer = printer;
    }

    public void run() {
        while (true) {
            printer.printPing();
        }
    }
}

class PongThread extends Thread {

    PingPongPrinter printer;

    public PongThread(PingPongPrinter printer) {
        this.printer = printer;
    }

    public void run() {
        while (true) {
            printer.printPong();
        }
    }
}

class PingPongTest {
    public static void main(String[] args) throws InterruptedException {

        PingPongPrinter printer = new PingPongPrinter();
        PingThread ping = new PingThread(printer);
        PongThread pong = new PongThread(printer);

        ping.start();
        pong.start();

        ping.join();
        pong.join();
    }
}
