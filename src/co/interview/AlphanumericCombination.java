package co.interview;

public class AlphanumericCombination {
    static int count;

    public static void main(String[] args) {
        System.out.println(calculatePossibleCombinations("314241414346644541652433556252665423311365641144551513243152623366656365516344432643125444163114545266353646436155611516162544434342253552516212225664435126342612366162364456225214466124311533435356363"));
    }

    public static long calculatePossibleCombinations(String inputStr) {
        System.out.println(inputStr);
        char[] ar = inputStr.toCharArray();
        char[] res = new char[402];
        count =0;
        recursion(ar, 0, res, 0);
        return count;
    }

    static void recursion(char[] ar, int i, char[] res, int n){
        if(ar.length == i){
            String s = String.valueOf(res).trim();
            System.out.println(s);
            String[] sc = s.split(" ");
            for(String si : sc)
                if(si.charAt(0)=='0' || si.length()>2 || Integer.valueOf(si)>26)
                    return;

            count++;
            return;
        }
        res[n] = ar[i];
        res[n+1] = ' ';
        recursion(ar, i+1, res, n+2);
        if(ar.length != i+1)
            recursion(ar, i+1, res, n+1);
    }
}
