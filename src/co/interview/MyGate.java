package co.interview;

import java.util.ArrayList;
import java.util.List;

public class MyGate {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(5);
        list.add(4);
        list.add(6);
        list.add(8);
        list.add(4);
        list.add(5);
        list.add(7);
        list.add(4);

        System.out.println("Maximum of local minimum set : " + fun(3, list));
        System.out.println("Maximum of local minimum set : " + fun(1, list));
    }

    private static int fun(int x, List<Integer> list) {

        int result = 0, first=Integer.MAX_VALUE, second=Integer.MAX_VALUE, i=0, j=0;

        while (j<x && i< list.size()){

            if (list.get(i) < first){
                second = first;
                first = list.get(i);
            } else if(list.get(i) < second){
                second = list.get(i);
            }
            result = Math.max(result, first);
            i++;
            j++;
            if (j==x){
                first = second;
                j=0;
            }
        }
        return result;
    }
}
