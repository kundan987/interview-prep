package co.interview;

import java.io.IOException;
import java.util.*;

class UniqueImages {
    public static class Image {
        private String filename;
        private int width;
        private int height;
        public Image(String filename, int width, int height) {
            this.filename = filename;
            this.width = width;
            this.height = height;
        }
        /**
         * Two Images are considered equal if they have
         * the same filename (without the extension), and the
         * same number of pixels.
         * Thus, flag.jpg with width=60 height=40 is
         * equal to flag.gif with width=40 and height=60
         */
        public boolean equals(Object other) {
            Image o = (Image)other;
            if (filename == null || o.filename == null)
                return false;
            String[] components = filename.split("\\.");
            String[] ocomponents = o.filename.split("\\.");
            return components[0].equals(ocomponents[0]) &&
                    width == o.width  && height == o.height;
        }
        public String toString() {
            return "Image: filename=" + filename + " Size=" + width*height;
        }
    }

    public static void printImages(Set<Image> images) {
        for(Image image: images) {
            for(Image img: images) {
                if(img != image && img.equals(image))
                    System.out.println(image);
            }

        }
    }

    public static void main(String[] args) {
        Image[] images = {new Image("flag.jpg", 40, 60),
                new Image("flag.gif", 40, 60),
                new Image("smile.gif", 100, 200),
                new Image("smile.gif", 50, 400),
                new Image("other.jpg", 40, 60),
                new Image("lenna.jpg", 512, 512),
                new Image("Lenna.jpg", 512, 512)};
        Set<Image> set = new HashSet<Image>(Arrays.asList(images));
        UniqueImages.printImages(set);


        try {
            int c = 1/0;
        } catch (Exception e) {
            System.out.println("E");
        } finally {
            System.out.println("F");
        }
    }

    public class C {
        public float m(float x, float y) throws IOException {
            return 0;
        }
    }

    public class D extends C {
        @Override
        public float m(float x1, float x2) {return 0;}
    }

    public static String m(MyClass o1, MyClass o2) {
        String s1 = o1 == o2 ? "T" : "F";
        String s2 = o1.equals(o2) ? "T" : "F";
        String s3 = o1.hashCode() == o2.hashCode() ? "T" : "F";
        return s1 + s2 + s3;
    }

}
