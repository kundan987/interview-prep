package co.interview;

import java.util.Arrays;

public class OracleCoding2 {
    //max product of any pair;
    //input can be +ve and -ve

    public static void main(String[] args) {
        int[] a = {1,2,3,4};
    }
    public int fun(int[] ar){
        Arrays.sort(ar);
        int product=Integer.MIN_VALUE;
        int l = ar.length-1;

        product = Math.max(product, ar[l]*ar[l-1]);

        if(ar[0]<0 && ar[1]<0){
            product = Math.max(product, ar[0]*ar[1]);
        }

        return product;
    }
}

//@Entity
class Employee {

    private Integer id;
    private String name;

    //setter getter

}

class ComplexAlgo{

   Sort sor;

}

class Bulble implements Sort{
    }
class Insert implements Sort{
}

interface Sort {

}

class AA{
    //@Autowire
    B b;
}

class BB {
    //@Autowire
    A a;
}
