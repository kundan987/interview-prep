package co.interview;

import java.util.ArrayList;
import java.util.Date;

class A {
    public A() {
        System.out.print("A");
    }
}
class B extends A {
    public B() {
        super();
        System.out.print("B");

    }
}
public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(-1);
        list.add(-5);
        list.add(5);
        System.out.println(list);
        removeNegative(list);
        System.out.println(list);
        Date s = new Date();
        System.out.println(s.getTime());
        n_squares(1000);
        System.out.println(new Date().getTime() - s.getTime());
        s = new Date();
        n_squares(2000);
        System.out.println(new Date().getTime() - s.getTime());
    }

    static String n_squares(int n) {
        StringBuffer s = new StringBuffer("0");
        int n_squared = n*n;
        for (int i=0; i<n_squared; i++) {
            s.append (", " + i);
        }
        return s.toString();
    }

    static void removeNegative(ArrayList<Integer> a) {
        int i=0;
        while(i<a.size())
            if (a.get(i) < 0)
                a.remove(i);
            else
                i++;
    }
}

