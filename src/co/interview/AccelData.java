package co.interview;

import java.util.*;

class TNode{
    int data;
    TNode left;
    TNode right;

    public TNode(int data) {
        this.data = data;
    }
}
public class AccelData {

    TNode root;

    public static void main(String[] args) {
        int[] array = {0, 0, 0, 0, 0, 1, 1, 1, 1};
        System.out.println("Index : " + index(array));
        System.out.println("Index : " + index(new int[]{0, 0, 0, 0, 0, 0, 0, 0, 1}));

        AccelData accelData = new AccelData();
        accelData.root = new TNode(1);
        accelData.root.left = new TNode(2);
        accelData.root.right = new TNode(3);
        accelData.root.left.left = new TNode(4);
        accelData.root.left.right = new TNode(5);
        accelData.root.right.left = new TNode(6);
        accelData.root.right.right = new TNode(7);
        accelData.root.left.left.left = new TNode(8);
        accelData.root.left.left.right = new TNode(9);
        accelData.root.left.right.left = new TNode(10);
        accelData.root.left.right.right = new TNode(11);
        accelData.root.right.left.left = new TNode(12);
        accelData.root.right.left.right = new TNode(13);
        accelData.root.right.right.left = new TNode(14);
        accelData.root.right.right.right = new TNode(15);

        System.out.println(accelData.zigzag(accelData.root));
        Map<String, Object> map = new HashMap<>();
        map.put("Name", "Kundan");
        map.put("Age", "28");

        Map<String, Object> internal = new HashMap<>();
        internal.put("work", "snapdeal");
        map.put("place", internal);

        Map<String, String> result = new HashMap<>();
        accelData.flatJson(map, result, "");
        for (Map.Entry<String, String> m : result.entrySet())
            System.out.println(m.getKey() + " : " + m.getValue());
    }

    private static int index(int[] array) {
        int left = 0;
        int right = 1;
        while (array[right] == 0) {
            left = right;
            right = right * 2;
        }
        return search(array, left, right);

    }

    static int search(int[] array, int left, int right) {
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (array[mid] == 1 && (mid == 0 || array[mid - 1] == 0))
                return mid;
            else if (array[mid] == 1)
                right = mid - 1;
            else
                left = mid + 1;
        }
        return -1;
    }

    public List<List<Integer>> zigzag(TNode node){
        List<List<Integer>> result = new ArrayList<>();
        if (node == null)
            return result;
        Queue<TNode> queue = null;
        queue.add(node);
        boolean flag = true;
        while (!queue.isEmpty()){
            int size = queue.size();
            List<Integer> list = new ArrayList<>();
            while (size>0){
                TNode current = queue.poll();
                if (flag){
                    list.add(current.data);
                } else{
                    list.add(0, current.data);
                }
                if(current.left != null)
                    queue.add(current.left);
                if (current.right != null)
                    queue.add(current.right);
                size--;
            }
            result.add(list);
            flag = !flag;
        }
        return result;
    }

    public void flatJson(Map<String, Object> map, Map<String, String> result, String sb){
        for (Map.Entry<String, Object> m : map.entrySet()){
            if (m.getValue() instanceof Map){
                flatJson((Map<String, Object>) m.getValue(), result, sb+m.getKey()+".");
            } else {
                result.put(sb+m.getKey(), (String)m.getValue());
            }
        }
    }
}
