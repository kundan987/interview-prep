package co.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Oracle {

    //Given an unsorted array of integers, find a pair with given sum in it. input [8,7,2,5,1], sum =10  Output - {8,2} 

    public static void main(String[] args) {
        System.out.println("Pair :" + sumPair(new int[]{8,9,2,5,1}, 10));
    }

    private static List<List<Integer>> sumPair(int[] ar, int sum){
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(ar);
        int start = 0, end = ar.length-1;
        while(start<end){
            int temp = ar[start]+ar[end];
            if(temp==sum){
                result.add(Arrays.asList(ar[start], ar[end]));
                start++;
                end--;
            }
            else if(temp>sum)
                end--;
            else
                start++;
        }

        return result;
    }


}
