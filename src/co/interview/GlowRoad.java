package co.interview;

import java.util.HashSet;
import java.util.Set;

public class GlowRoad {

    public static void main(String[] args) {
        System.out.println("No of palindromic substrings : " + countPalindrome("kmkkm"));
        System.out.println("No of ways to get change : "+ waysChange(new int[] {1,3,5}, 5));
    }

    public static int waysChange(int[] coins, int k){
        int[][] dp = new int[coins.length+1][k+1];

        for(int i=1; i<coins.length; i++){
            for(int j=1; j<=k; j++){
                if(j<coins[i])
                if(k%coins[i]==0)
                    dp[i][j] = dp[i-1][j]+1;
                else
                    dp[i][j] = dp[i-1][j];
            }
        }

        return dp[coins.length][k];
    }


    public static int countPalindrome(String input){
        int count =0;
        Set<String> set = new HashSet<>();

        for(int i=0; i<input.length(); i++){
            for(int j=i; j<input.length(); j++){
                String sub = input.substring(i,j+1);
                if (!set.contains(sub) && isPalindrome(sub)){
                    count++;
                    set.add(sub);
                }
            }
        }
        System.out.println(set);
        return count;
    }

    public static boolean isPalindrome(String in){
        int i=0, j=in.length()-1;

        while(i<j){
            if(in.charAt(i++) != in.charAt(j--))
                return false;
        }
        return true;
    }
}
