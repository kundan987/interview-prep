package co.interview;

class Node<T> {
    private T data;
    private Node<T> next;
    private Node<T> random;

    public Node(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public Node<T> getRandom() {
        return random;
    }

    public void setRandom(Node<T> random) {
        this.random = random;
    }
}

class LinkedList<T> {
    private Node<T> root;

    public static void main(String[] args) {


        // Create the list
        Node<String> a = new Node<>("A");
        Node<String> b = new Node<>("B");
        Node<String> c = new Node<>("C");
        Node<String> d = new Node<>("D");
        Node<String> e = new Node<>("E");
        LinkedList<String> list = new LinkedList(a);
        list.root.setNext(b);
        list.root.setRandom(e);
        list.root.getNext().setNext(c);
        list.root.getNext().setRandom(a);
        list.root.getNext().getNext().setNext(d);
        list.root.getNext().getNext().setRandom(e);
        list.root.getNext().getNext().getNext().setNext(e);
        list.root.getNext().getNext().getNext().setRandom(b);
        list.root.getNext().getNext().getNext().getNext().setRandom(c);

        //
        list.print();

        //
        LinkedList<String> cloned = list.deepClone();

        cloned.print();

        list.print();
    }


    public LinkedList(Node<T> root) {
        this.root = root;
    }

    public void print() {
        Node<T> node = this.root;

        while (node != null){
            System.out.println(node.getData() +" -> " + node.getRandom().getData());
            node = node.getNext();
        }

    }

    /*
     * Returns a new instance of the cloned linked list
     */
    public LinkedList<T> deepClone() {
        // You need to implement

        LinkedList<T> cloned = null;

        Node<T> node = this.root;
        if (node == null) {
            return cloned;
        }

        while (node != null) {

            Node<T> newNode = new Node(node.getData()+"1");

            newNode.setNext(node.getNext());
            node.setNext(newNode);
            node = newNode.getNext();
        }

        node = this.root;

        //A->A'->B->B'->C->C'
        while (node != null) {
            node.getNext().setRandom((node.getRandom() != null) ? node.getRandom().getNext() : null);
            node = node.getNext().getNext();
        }

        Node old = this.root; // A->B->C
        Node clone = this.root.getNext(); // A'->B'->C'

        Node cloneHead = this.root.getNext();

        // unlink
        while (old != null) {
            old.setNext(old.getNext().getNext());
            clone.setNext((clone.getNext() != null) ? clone.getNext().getNext() : null);
            old = old.getNext();
            clone = clone.getNext();
        }

        return new LinkedList(cloneHead);
    }
}


