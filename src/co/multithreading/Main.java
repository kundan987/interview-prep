package co.multithreading;

import java.util.concurrent.CyclicBarrier;
import java.util.logging.Logger;

public class Main {

    static Logger LOG = Logger.getLogger("co.interview.Main");

    public static void main(String[] args) {
            start();
    }

    public static void start() {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> {
            // ...
            LOG.info("All previous tasks are completed");
        });

        Thread t1 = new Thread(new Task(cyclicBarrier), "T1");
        Thread t2 = new Thread(new Task(cyclicBarrier), "T2");
        Thread t3 = new Thread(new Task(cyclicBarrier), "T3");

        if (!cyclicBarrier.isBroken()) {
            t1.start();
            t2.start();
            t3.start();
        }
    }
}
