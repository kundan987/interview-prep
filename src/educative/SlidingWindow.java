package educative;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SlidingWindow {

    public static void main(String[] args) {
        int[] a = {1, 3, 2, 6, -1, 4, 1, 8, 2};
        int[] b = {2, 1, 5, 2, 3, 2};
        int[] c = {0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1};
        int[] d = {0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1};
        int k =5;
        String s ="araaci";
        Character[] fruit={'A', 'B', 'C', 'B', 'B', 'C'};

        double[] res = findAverages(a, k);
        System.out.println("Averages of subarrays of size k : " + Arrays.toString(res));
        System.out.println("Maximum sum subarray of size k : " + maximumSum(a,k));
        System.out.println("smallest contiguous subarray whose sum is greater than or equal to k : " + smallestContiguousSubArray(b,7));
        System.out.println("Length of longest sub string with no more than K distinct characters : " + longestSubArray(s,2));
        System.out.println("Maximum number of fruits in two baskets : " + maximumFruits(fruit,2));
        System.out.println("length of the longest substring which has no repeating characters : " + longestSubStringNoRepeatingChar("aabcdefghijccbbcade"));
        System.out.println("length of the longest substring having the same letters after replacement : " + longestSubStringSameLettersAfterReplacement("aabccbccbbbbbbbbbcb", 2));
        System.out.println("length of the longest contiguous subarray having all 1s  after replacing no more than ‘k’ 0s with 1s: " + longestSubArrayAfterReplacement(c, 2));
        System.out.println("length of the longest contiguous subarray having all 1s  after replacing no more than ‘k’ 0s with 1s: " + longestSubArrayAfterReplacementWithoutMap(d, 3));
        System.out.println("string contains any permutation of the pattern : " + stringContainsAnyPermutationOfPattern("oidbcaf", "abc"));
    }

    private static boolean stringContainsAnyPermutationOfPattern(String string, String pattern) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i=0; i < pattern.length(); i++)
            map.put(pattern.charAt(i), map.getOrDefault(pattern.charAt(i), 0)+1);
        int matched=0, start=0;
        for (int i=0; i < string.length(); i++){

            if (map.containsKey(string.charAt(i))){
                map.put(string.charAt(i), map.get(string.charAt(i))-1);
                if (map.get(string.charAt(i))==0)
                    matched++;
            }

            if (matched == map.size())
                return true;

            if (i  >= pattern.length()-1){
                if (map.containsKey(string.charAt(start))){
                    if (map.get(string.charAt(start))==0)
                        matched--;
                    map.put(string.charAt(start), map.get(string.charAt(start))+1);
                }
                start++;
            }
        }

        return false;
    }

    private static int longestSubArrayAfterReplacementWithoutMap(int[] a, int k) {
        int maxLen =0, start=0, maxRepeat=0;
        for (int i=0; i< a.length; i++) {
            if (a[i]==1)
                maxRepeat++;
            if (i-start+1-maxRepeat>k){
                if (a[start]==1)
                    maxRepeat--;
                start++;
            }
            maxLen =  Math.max(maxLen, i-start+1);
        }
        return maxLen;
    }

    private static int longestSubArrayAfterReplacement(int[] a, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int maxLen =0, start=0, maxRepeat=0;
        for (int i=0; i< a.length; i++) {
            int right = a[i];
            map.put(right, map.getOrDefault(right, 0)+1);
            maxRepeat = Math.max(maxRepeat, map.get(right));
            if (i-start+1-maxRepeat>k){
                int left = a[start];
                map.put(left, map.get(left)-1);
                start++;
            }
            maxLen =  Math.max(maxLen, i-start+1);
        }
        return maxLen;
    }

    private static int longestSubStringSameLettersAfterReplacement(String s, int k) {
        Map<Character, Integer> map = new HashMap<>();
        int maxLen =0, start=0, maxRepeat=0;
        for (int i=0; i< s.length(); i++){
            char c = s.charAt(i);
            map.put(c, map.getOrDefault(c, 0)+1);
            maxRepeat = Math.max(maxRepeat, map.get(c));

            if (i-start+1-maxRepeat>k){
                char left = s.charAt(start);
                map.put(left, map.get(left)-1);
                start++;
            }
            maxLen = Math.max(maxLen, i-start+1);
        }
        return maxLen;
    }

    private static int longestSubStringNoRepeatingChar(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int maxLen =0, start=0;
        for (int i=0; i< s.length(); i++){
            char c = s.charAt(i);

            if (map.containsKey(c)){
                start = Math.max(start, map.get(c)+1);
            }
            map.put(c, i);
            maxLen = Math.max(maxLen, i-start+1);
        }
        return maxLen;
    }

    private static int maximumFruits(Character[] fruit, int k) {
        Map<Character, Integer> map = new HashMap<>();
        int maxLen =0 , j=0;
        for (int i=0; i< fruit.length; i++){

            if (map.containsKey(fruit[i]))
                map.put(fruit[i], map.get(fruit[i])+1);
            else
                map.put(fruit[i], 1);

            while (map.size() > k){

                char chr = fruit[i];
                if (map.get(chr) == 1)
                    map.remove(chr);
                else
                    map.put(chr, map.get(chr)-1);

                j++;
            }
            maxLen = Math.max(i-j+1, maxLen);
        }

        return maxLen;

    }

    private static int longestSubArray(String s, int k) {
        Map<Character, Integer> map = new HashMap<>();
        int maxLen =0 , j=0;
        for (int i=0; i< s.length(); i++){


            if (map.containsKey(s.charAt(i)))
                map.put(s.charAt((i)), map.get(s.charAt(i))+1);
            else
                map.put(s.charAt((i)), 1);

            while (map.size() > k){

                char chr = s.charAt(j);
                if (map.get(chr) == 1)
                    map.remove(chr);
                else
                    map.put(chr, map.get(chr)-1);

                j++;
            }
            maxLen = Math.max(i-j+1, maxLen);
        }

        return maxLen;
    }

    private static int smallestContiguousSubArray(int[] a, int k) {
        int sum =0, len =0,minLen=Integer.MAX_VALUE, l=0;
        for (int i=0; i< a.length; i++){
            sum =sum +a[i];

            while(sum>=k){
                minLen = Math.min(minLen, i-len+1);
                sum-= a[len];
                len++;
            }
        }
        return minLen;
    }

    private static int maximumSum(int[] a, int k) {

        int sum =0, maxSum =0, l=0;
        for (int i=0; i< a.length; i++){
            sum =sum +a[i];
            if (i >= k-1){
                if (sum > maxSum)
                    maxSum = sum;
                sum = sum - a[l];
                l++;
            }
        }
        return maxSum;
    }

    private static double[] findAverages(int[] a, int k) {
        double[] res = new double[a.length-k+1];
        int j=0, l=0;
        double sum =0;
        for (int i=0; i< a.length; i++){
            sum =sum +a[i];

            if (i >= k-1){
                res[l]=sum/k;
                sum = sum - a[l];
                l++;
            }
        }

        return res;
    }
}
