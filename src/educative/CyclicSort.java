package educative;

import java.util.*;

public class CyclicSort {

    public static void main(String[] args) {
        int[] a = {3, 1, 5, 4, 2};
        int[] b = {4, 0, 3, 1};

        System.out.println("Sort array : " + Arrays.toString(sortArray(a)));
        System.out.println("Missing number : " + missingNumber(b));
        System.out.println("Missing number : " + missingNumber(new int[]{0,1,2,3,4}));
        System.out.println("Missing all numbers : " + missingAllNumber(new int[]{2, 4, 1, 2}));
        System.out.println("Missing all numbers : " + missingAllNumber(new int[]{2, 3, 1, 8, 2, 3, 5, 1}));
        System.out.println("Find duplicate : " + findDuplicate(new int[]{1, 4, 4, 3, 2}));
        System.out.println("Find duplicate : " + findDuplicate(new int[]{2, 4, 1, 4, 4}));
        System.out.println("Find all duplicates : " + findAllDuplicate(new int[]{3, 4, 4, 5, 5}));
        System.out.println("Find all duplicates : " + findAllDuplicate(new int[]{5, 4, 7, 2, 3, 5, 3}));
        System.out.println("Corrupt pair : " + corruptPair(new int[]{3, 1, 2, 5, 2}));
        System.out.println("Corrupt pair : " + corruptPair(new int[]{3, 1, 2, 3, 6, 4}));
        System.out.println("Smallest missing positive number : " + smallestMissingPositiveNumber(new int[]{-3, 1, 5, 4, 2}));
        System.out.println("Smallest missing positive number : " + smallestMissingPositiveNumber(new int[]{3, 2, 5, 1}));
        System.out.println("Smallest missing positive number : " + smallestMissingPositiveNumber(new int[]{3, -2, 0, 1, 2}));
        System.out.println("Find k missing positive number : " + findMissingPositiveNumber(new int[]{3, -1, 4, 5, 5}, 3));
        System.out.println("Find k missing positive number : " + findMissingPositiveNumber(new int[]{2, 3, 4}, 3));
    }

    private static List<Integer> findMissingPositiveNumber(int[] a, int k) {
        List<Integer> missingNumbers = new ArrayList<>();
        Set<Integer> extraNumbers = new HashSet<>();
        int i=0;
        while (i< a.length){
            if (a[i] >0 && a[i] <= a.length && a[i] != a[a[i]-1]){
                swap(a, i, a[i]-1);
            }else
                i++;
        }
        System.out.println(Arrays.toString(a));
        for (int j =0; j < a.length && missingNumbers.size()<k; j++){
            if (a[j] != j+1){
                missingNumbers.add(j+1);
                extraNumbers.add(a[j]);
            }
        }
        int l =1;
        while (missingNumbers.size() < k){
            int candidateNumber = a.length +l;
            l++;
            if (!extraNumbers.contains(candidateNumber))
                missingNumbers.add(candidateNumber);
        }

        return missingNumbers;
    }

    private static int smallestMissingPositiveNumber(int[] a) {
        int i=0;
        while (i< a.length){
            if (a[i] >0 && a[i] <= a.length && a[i] != a[a[i]-1]){
                swap(a, i, a[i]-1);
            }else
                i++;
        }
        System.out.println(Arrays.toString(a));
        for (int j =0; j < a.length ; j++)
            if (a[j] != j+1){
                return j+1;
            }

            return -1;
    }

    private static List<Integer> corruptPair(int[] a) {
        List<Integer> corruptPair = new ArrayList<>();
        int i=0;
        while (i< a.length){
            if (a[i] != a[a[i]-1]){
               swap(a, i, a[i]-1);
            }else
                i++;
        }
        for (int j =0; j < a.length ; j++)
            if (a[j] != j+1){
                corruptPair.add(a[j]);
                corruptPair.add(j+1);
            }


        System.out.println(Arrays.toString(a));

        return corruptPair;
    }

    private static List<Integer> findAllDuplicate(int[] a) {
        List<Integer> duplicates = new ArrayList<>();
        int i=0;
        while (i< a.length){
            if (a[i] != i+1){
                if (a[i] != a[a[i]-1])
                    swap(a, i, a[i]-1);
                else{
                    duplicates.add(a[i]);
                    i++;
                }

            }else
                i++;
        }
        return duplicates;
    }

    private static int findDuplicate(int[] a) {
        int i=0;
        while (i< a.length){
            if (a[i] != i+1){
                if (a[i] != a[a[i]-1])
                    swap(a, i, a[i]-1);
                else
                    return a[i];
            }else
                i++;
        }
        return -1;
    }

    private static List<Integer> missingAllNumber(int[] a) {
        List<Integer> missingNumbers = new ArrayList<>();
        int i=0;
        while (i< a.length){
            if (a[i] != a[a[i]-1]){
                swap(a, i, a[i]-1);
            } else
                i++;
        }
        System.out.println(Arrays.toString(a));
        for (int j=0; j < a.length; j++) {
            if (a[j] != j + 1)
                missingNumbers.add(j+1);
        }
        return missingNumbers;
    }

    private static int missingNumber(int[] a){

        int i=0;
        while (i< a.length){
            if (a[i] < a.length && a[i]!= i){
                swap(a, i, a[i]);
            } else
                i++;
        }

        for (int j=0; j < a.length; j++){
            if (a[j] != j)
                return j;
        }


        return a.length;
    }

    private static void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    private static int[] sortArray(int[] a) {
        int i=0;
        while (i< a.length){
            if (a[i] == i+1)
                i++;
            else {
                int temp = a[i];
                a[i] = a[temp-1];
                a[temp-1] = temp;
            }
        }
        return a;
    }
}
