package educative;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class TreeNode {
    int data;
    TreeNode left;
    TreeNode right;

    public TreeNode(int data) {
        this.data = data;
    }
}
public class TreeBFS {
    TreeNode root;

    public static void main(String[] args) {
        TreeBFS tree = new TreeBFS();
        tree.root = new TreeNode(1);
        tree.root.left = new TreeNode(2);
        tree.root.left.left = new TreeNode(4);
        tree.root.left.right = new TreeNode(5);
        tree.root.right = new TreeNode(3);
        tree.root.right.left = new TreeNode(6);
        tree.root.right.right = new TreeNode(7);

        System.out.println(tree.levelOrderTraversal(tree.root));
        System.out.println("Minimum depth : " + tree.minimumDepth(tree.root));
        System.out.println("Level order successor : " + tree.levelOrderSuccessor(tree.root, tree.root.left.left));
    }

    private int levelOrderSuccessor(TreeNode root, TreeNode givenNode) {
        if (root == null)
            return -1;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean flag = false;
        while (!queue.isEmpty()){
            int levelSize = queue.size();
            for (int i=0; i< levelSize; i++){
                TreeNode node = queue.poll();
                if (flag)
                    return node.data;
                if (node == givenNode)
                    flag = true;
                if (node.left != null)
                    queue.add(node.left);
                if (node.right != null)
                    queue.add(node.right);
            }
        }
        return -1;
    }

    private int minimumDepth(TreeNode root) {
        int depth=0;
        if (root == null)
            return depth;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()){
            depth++;
            int levelSize = queue.size();
            for (int i=0; i< levelSize; i++){
                TreeNode node = queue.poll();
                if (node.left == null && node.right == null)
                    return depth;
                if (node.left != null)
                    queue.add(node.left);
                if (node.right != null)
                    queue.add(node.right);
            }
        }
        return depth;
    }

    private List<List<Integer>> levelOrderTraversal(TreeNode root) {
        List<List<Integer>> results = new LinkedList<>();
        if (root == null)
            return results;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()){
            int levelSize = queue.size();
            List<Integer> result = new LinkedList<>();
            for (int i=0; i< levelSize; i++){
                TreeNode node = queue.poll();
                result.add(node.data);
                if (node.left != null)
                    queue.add(node.left);
                if (node.right != null)
                    queue.add(node.right);
            }
            results.add(0,result);
        }
        return results;
    }
}
