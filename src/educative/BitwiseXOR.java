package educative;

import java.util.*;

public class BitwiseXOR {

    public static void main(String[] args) {
        System.out.println(findSingleNumbers(new int[]{2, 4, 6, 2}));

        System.out.println((char) 97);

        System.out.println(findTheDifference("abcd", "abcde"));

        String string = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";

        Map<String, Integer> map = new HashMap<>();
        Set<Integer> set = new HashSet<>();
        set.retainAll(set);
        Iterator it = set.iterator();
        List<Integer> list = new LinkedList<>();

       // System.out.println(canConvertString("input", "ouput", 9));
        System.out.println(minInsertions("(()))"));
        System.out.println(minInsertions("())"));
        System.out.println(minInsertions("))())("));
        System.out.println(minInsertions("(((((("));
        System.out.println(minInsertions(")))))))"));
        System.out.println(minInsertions("((())))))"));
        System.out.println(minInsertions("(()))(()))()())))"));
        List<int[]> result = new ArrayList<>();
        result.toArray();
        result.toArray(new int[result.size()][]);

    }

    public static int minInsertions(String s) {
        Stack<Character> stack = new Stack<>();
        if(s.length() == 0)
            return 0;
        while(s != s.replace("())", ""))
            s = s.replace("())", "");
        System.out.println(s);
        if(s.length() == 0)
            return 0;
        int count =0;
        while(s.length() != 0){
            if(s.charAt(0) != '('){
                count++;
                s="("+s;
                s = s.replace("())", "");
            } else {
                if(s.charAt(s.length()-1) == ')'){
                    count++;
                    s=s+")";
                    s = s.replace("())", "");
                } else {
                    count=count+2;
                    s=s+"))";
                    s = s.replace("())", "");
                }
            }
        }
       /*
        int open=0, close =0;
        for(Character ch : s.toCharArray()){
            if(ch=='('){
                open++;
            }else if(ch==')')
                close++;
        }
*/
        return count;

    }

    public static boolean canConvertString(String s, String t, int k) {
        if(s.length() != t.length())
            return false;
        int i=0, j=1;
        int c=0;
        for(Character ch : s.toCharArray()){
            if(ch == t.charAt(i)){
                i++;
            } else {
                while((ch+j) != (int)t.charAt(i)){
                    c++;
                    j++;
                    if(c>k)
                        return false;
                }
                i++;
                j=1;
                c=0;
            }
        }
        return true;
    }

    public static char findTheDifference(String s, String t) {
        int ch =0;
        int i;
        for(i=0; i<s.length(); i++){
            int chs = (int) s.charAt(i);
            int cht = (int) t.charAt(i);
            ch = (ch^(chs^cht));
        }
        ch = ch^(int)t.charAt(i);
        System.out.println(ch);
        return (char) ch;
    }

    private static List<Integer> findSingleNumbers(int[] nums) {
        List<Integer> result = new ArrayList<>();

        int n1n2 = 0;
        for(int num : nums)
            n1n2^=num;

        int rmsb = 1;

        while((rmsb&n1n2) ==0)
            rmsb<<=1;

        int n1=0,n2=0;
        for(int num : nums)
            if ((num&rmsb)!=0)
                n1^=num;
            else
                n2^=num;
            result.add(n1);
            result.add(n2);
        return result;
    }
}
