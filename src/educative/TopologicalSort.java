package educative;


import java.util.*;

public class TopologicalSort {

    public static void main(String[] args) {
        System.out.println("Topological ordering of graph : " + topologicalSort(4, new int[][] {new int[]{3, 2}, new int[]{3, 0}, new int[]{2, 0}, new int[]{2, 1}}));
        printOrders(4, new int[][] {new int[]{3, 2}, new int[]{3, 0}, new int[]{2, 0}, new int[]{2, 1}});
        printOrders(6, new int[][] {new int[]{2, 5}, new int[]{0, 5}, new int[]{0, 4}, new int[]{1, 4}, new int[]{3, 2}, new int[]{1, 3}});
        System.out.println("Correct orders of characters : " + findOrders(new String[]{"ba", "bc", "ac", "cab"}));
        System.out.println("MHT : " + minimumHeightTrees(5, new int[][] {new int[]{0,1}, new int[]{1,2}, new int[]{1,3}, new int[]{2, 4}}));

    }

    private static List<Integer> minimumHeightTrees(int nodes, int[][] edges) {
        List<Integer> result = new ArrayList<>();
        if (nodes<=0)
            return result;

        if (nodes ==1){
            result.add(0);
            return result;
        }
        HashMap<Integer, Integer> inDegrees = new HashMap<>();
        HashMap<Integer, List<Integer>> graph = new HashMap<>();

        for (int i=0;i<nodes;i++){
            inDegrees.put(i, 0);
            graph.put(i, new ArrayList<>());
        }

        for (int[] edge : edges){
            int n1 = edge[0], n2 = edge[1];
            inDegrees.put(n1, inDegrees.get(n1)+1);
            inDegrees.put(n2, inDegrees.get(n2)+1);
            graph.get(n1).add(n2);
            graph.get(n2).add(n1);
        }

        Queue<Integer> leaves = new LinkedList<>();
        for(Map.Entry<Integer, Integer> map : inDegrees.entrySet()){
            if (map.getValue() == 1)
                leaves.add(map.getKey());
        }

        int totalNodes = nodes;

        while (totalNodes > 2){
            int leavesSize = leaves.size();
            totalNodes-=leavesSize;
            for (int i=0; i< leavesSize; i++){
                int vertex = leaves.poll();
                List<Integer> children = graph.get(vertex);
                for (int child : children){
                    inDegrees.put(child, inDegrees.get(child)-1);
                    if (inDegrees.get(child) == 1)
                        leaves.add(child);
                }
            }
        }
        result.addAll(leaves);
        return result;

    }

    private static String findOrders(String[] words) {
        if (words == null || words.length==0)
            return "";
        HashMap<Character, Integer> inDegrees = new HashMap<>();
        HashMap<Character, List<Character>> graph = new HashMap<>();

        for (String word : words){
            for (Character character : word.toCharArray()){
                inDegrees.put(character, 0);
                graph.put(character, new ArrayList<>());
            }
        }

        for (int i=0; i< words.length-1; i++){
            String word1 = words[i], word2 = words[i+1];
            for (int j =0 ; j < Math.min(word1.length(), word2.length()); j++){
                Character parent = word1.charAt(j), child = word2.charAt(j);
                if (parent != child){
                    inDegrees.put(child, inDegrees.get(child)+1);
                    graph.get(parent).add(child);
                    break;
                }
            }
        }
        Queue<Character> queue = new LinkedList<>();
        for (Map.Entry<Character, Integer> map : inDegrees.entrySet()){
            if (map.getValue()==0)
                queue.add(map.getKey());
        }
        StringBuilder sortedOrder = new StringBuilder();
        while (!queue.isEmpty()){
            Character vertex = queue.poll();
            sortedOrder.append(vertex);
            List<Character> children = graph.get(vertex);
            for (Character child : children){
                inDegrees.put(child, inDegrees.get(child)-1);
                if (inDegrees.get(child)==0)
                    queue.add(child);
            }
        }
        if (sortedOrder.length() != graph.size())
            return "";

        return new String(sortedOrder);


    }

    private static void printOrders(int tasks, int[][] prerequisites) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> sortedOrder = new ArrayList<>();
        if (tasks <=0)
            return;
        HashMap<Integer, Integer> inDegrees = new HashMap<>();
        HashMap<Integer, List<Integer>> graph = new HashMap<>();

        for (int i=0;i<tasks;i++){
            inDegrees.put(i, 0);
            graph.put(i, new ArrayList<>());
        }

        for (int i=0; i<prerequisites.length; i++){
            int parent = prerequisites[i][0], child = prerequisites[i][1];
            inDegrees.put(child, inDegrees.get(child)+1);
            graph.get(parent).add(child);
        }

        Queue<Integer> sources = new LinkedList<>();
        for (Map.Entry<Integer, Integer> map : inDegrees.entrySet()){
            if (map.getValue()==0)
                sources.add(map.getKey());
        }

        printOrdersRecursively(graph, inDegrees, sources, sortedOrder, result);
        for (List<Integer> list : result)
            System.out.println(list);
    }

    private static void printOrdersRecursively(HashMap<Integer, List<Integer>> graph, HashMap<Integer, Integer> inDegrees, Queue<Integer> sources, List<Integer> sortedOrder, List<List<Integer>> result) {
        if (!sources.isEmpty()){
            for (Integer vertex : sources){
                sortedOrder.add(vertex);
                Queue<Integer> sourcesCopy = copySources(sources);
                sourcesCopy.remove(vertex);
                List<Integer> children = graph.get(vertex);
                for (Integer child : children){
                    inDegrees.put(child, inDegrees.get(child)-1);
                    if (inDegrees.get(child)==0)
                        sourcesCopy.add(child);
                }

                printOrdersRecursively(graph, inDegrees, sourcesCopy, sortedOrder, result);

                sortedOrder.remove(vertex);
                for (Integer child : children)
                    inDegrees.put(child, inDegrees.get(child)+1);


            }
        }
        if (sortedOrder.size() == graph.size()){
            result.add(new ArrayList<>(sortedOrder));
            //  System.out.println(sortedOrder);
        }
    }


    private static Queue<Integer> copySources(Queue<Integer> sources) {
        Queue<Integer> copy = new LinkedList<>();
        for (Integer vertex : sources){
            copy.add(vertex);
        }
        return copy;
    }

    private static List<Integer> topologicalSort(int vertices, int[][] edges) {
        List<Integer> sortedOrder = new ArrayList<>();

        if (vertices <=0)
            return sortedOrder;

        HashMap<Integer, Integer> inDegrees = new HashMap<>();
        HashMap<Integer, List<Integer>> graph = new HashMap<>();

        for (int i=0;i<vertices;i++){
            inDegrees.put(i, 0);
            graph.put(i, new ArrayList<>());
        }

        for (int i=0; i<edges.length; i++){
            int parent = edges[i][0], child = edges[i][1];
            inDegrees.put(child, inDegrees.get(child)+1);
            graph.get(parent).add(child);
        }

        Queue<Integer> queue = new LinkedList<>();
        for (Map.Entry<Integer, Integer> map : inDegrees.entrySet()){
            if (map.getValue()==0)
                queue.add(map.getKey());
        }

        while (!queue.isEmpty()){
            int vertex = queue.poll();
            sortedOrder.add(vertex);
            List<Integer> children = graph.get(vertex);
            for (Integer child : children){
                inDegrees.put(child, inDegrees.get(child)-1);
                if (inDegrees.get(child)==0)
                    queue.add(child);
            }
        }
        if (sortedOrder.size() != vertices)
            return new ArrayList<>();

        return sortedOrder;
    }
}
