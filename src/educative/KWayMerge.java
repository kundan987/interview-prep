package educative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

class KNode{
    int arrayIndex;
    int elementIndex;

    public KNode(int arrayIndex, int elementIndex) {
        this.arrayIndex = arrayIndex;
        this.elementIndex = elementIndex;
    }
}

class MNode{
    int row;
    int col;

    public MNode(int row, int col) {
        this.row = row;
        this.col = col;
    }
}
public class KWayMerge {
    public static void main(String[] args) {
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> l1 =Arrays.asList(2, 6, 8);
        List<Integer> l2 =Arrays.asList(3, 6, 7);
        List<Integer> l3 =Arrays.asList(1, 3, 4);
        list.add(l1);
        list.add(l2);
        list.add(l3);
        System.out.println("Merge k sorted list : " + mergeKSortedList(list));
        Integer[] i1 = new Integer[]{2, 6, 8};
        Integer[] i2 = new Integer[]{3, 6, 7};
        Integer[] i3 = new Integer[]{1, 3, 4};
        List<Integer[]> input = new ArrayList<>();
        input.add(i1);
        input.add(i2);
        input.add(i3);
        System.out.println("Kth Smallest Number in M Sorted arrays : " + kthSmallestInMSortedArrays(input, 5));
        Integer[][] matrix = {{2, 6, 8},{3, 7, 10}, {5, 8, 11}};
        System.out.println("Kth Smallest Number in a Sorted Matrix : " + kthSmallestInMSortedMatrix(matrix, 5));
        Integer[] in1 = new Integer[]{1, 5, 8};
        Integer[] in2 = new Integer[]{4, 12};
        Integer[] in3 = new Integer[]{7, 8, 10};
        List<Integer[]> integers = new ArrayList<>();
        integers.add(in1);
        integers.add(in2);
        integers.add(in3);
        System.out.println("Smallest number range : " + smallestNumberRange(integers));
        int[] a = new int[]{9, 8, 2};
        int[] b = new int[]{6, 3, 1};
        System.out.println("K Pairs with Largest Sums : " + KPairsLargestSum(a, b, 3));
    }

    private static List<List<Integer>> KPairsLargestSum(int[] nums1, int[] nums2, int k) {
        List<List<Integer>> result = new ArrayList<>();
        PriorityQueue<int[]> minHeap = new PriorityQueue<>((a, b) -> (a[0]+a[1]) - (b[0]+b[1]));

        for (int num1 : nums1) {
            for (int num2 : nums2) {
                if (minHeap.size() < k) {
                    minHeap.add(new int[]{num1, num2});
                } else {
                    if (num1 + num2 > minHeap.peek()[0] + minHeap.peek()[1]) {
                        minHeap.poll();
                        minHeap.add(new int[]{num1, num2});
                    } else
                        break;
                }
            }
        }
        while (!minHeap.isEmpty()){
            List<Integer> res = new ArrayList<>();
            res.add(minHeap.peek()[0]);
            res.add(minHeap.peek()[1]);
            minHeap.poll();
            result.add(res);
        }
        return result;
    }

    private static List<Integer> smallestNumberRange(List<Integer[]> lists) {
        PriorityQueue<KNode> minHeap = new PriorityQueue<>((a, b) -> lists.get(a.arrayIndex)[a.elementIndex] - lists.get(b.arrayIndex)[b.elementIndex]);
        int startRange = 0, endRange = Integer.MAX_VALUE, currentMax = Integer.MIN_VALUE;
        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i) != null){
                minHeap.add(new KNode(i, 0));
                currentMax = Math.max(currentMax, lists.get(i)[0]);
            }
        }

        while (minHeap.size() == lists.size()) {

            KNode element = minHeap.poll();

            if (endRange - startRange > currentMax - lists.get(element.arrayIndex)[element.elementIndex]) {
                startRange = lists.get(element.arrayIndex)[element.elementIndex];
                endRange = currentMax;
            }

            element.elementIndex++;
            if (lists.get(element.arrayIndex).length > element.elementIndex){
                minHeap.add(element);
                currentMax = Math.max(currentMax, lists.get(element.arrayIndex)[element.elementIndex]);
            }

        }
        List<Integer> result = new ArrayList<>();
        result.add(startRange);
        result.add(endRange);
        return result;
    }

    private static int kthSmallestInMSortedMatrix(Integer[][] matrix, int k) {

        PriorityQueue<MNode> minHeap = new PriorityQueue<>((a,b) -> matrix[a.row][a.col] - matrix[b.row][b.col]);

        for (int i=0; i< matrix.length; i++){
            if (matrix[i] != null)
                minHeap.add(new MNode(i, 0));
        }
        int count=0, result=0;
        while (!minHeap.isEmpty()){
            MNode element = minHeap.poll();
            result = matrix[element.row][element.col];
            if (++count == k)
                return result;
            element.col++;
            if (matrix[element.row].length > element.col)
                minHeap.add(element);
        }
        return result;
    }

    private static int kthSmallestInMSortedArrays(List<Integer[]> lists, int k) {

        PriorityQueue<KNode> minHeap = new PriorityQueue<>((a,b) -> lists.get(a.arrayIndex)[a.elementIndex] - lists.get(b.arrayIndex)[b.elementIndex]);

        for (int i=0; i< lists.size(); i++){
            if (lists.get(i) != null)
                minHeap.add(new KNode(i, 0));
        }
        int count=0, result=0;
        while (!minHeap.isEmpty()){
            KNode element = minHeap.poll();
            result = lists.get(element.arrayIndex)[element.elementIndex];
            if (++count == k)
                return result;
            element.elementIndex++;
            if (lists.get(element.arrayIndex).length > element.elementIndex)
                minHeap.add(element);
        }
        return result;
    }

    private static List<Integer> mergeKSortedList(List<List<Integer>> input) {
        List<Integer> result = new ArrayList<>();
        PriorityQueue<Integer> heap = new PriorityQueue<>();

        for (List<Integer> list : input){
            for (int num : list)
                heap.add(num);
        }

        while (!heap.isEmpty())
            result.add(heap.poll());
        return result;
    }

}
