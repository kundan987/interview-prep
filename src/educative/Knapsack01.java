package educative;

import java.util.Arrays;

public class Knapsack01 {
    public static void main(String[] args) {
        int[] profits = new int[]{1,6,10,16};
        int[] weights = new int[]{1,2,3,5};
        System.out.println("Maximum profits : " + maximumProfit(profits, weights, 7));
        System.out.println("Maximum profits : " + solveKnapsack(profits, weights, 7));
        System.out.println("Equal Subset Sum Partition : " + equalSubsetSumPartition(new int[]{1, 2, 3, 4}));
        System.out.println("Equal Subset Sum Partition  with min diff : " + equalSubsetSumPartitionWithMinDiff(new int[]{1, 2, 3, 9}));
    }

    private static boolean equalSubsetSumPartition(int[] ints) {
        int sum =0;
        for (int i=0; i< ints.length; i++)
            sum+=ints[i];
        if (sum%2!=0)
            return false;
        Boolean[][] dp = new Boolean[ints.length][sum/2+1];
        return equalSubsetSumPartitionRecursive(dp, ints, sum/2,  0);
    }

    private static boolean equalSubsetSumPartitionWithMinDiff(int[] ints) {
        int sum =0;
        for (int i=0; i< ints.length; i++)
            sum+=ints[i];
        if (sum%2!=0)
            return false;
        Boolean[][] dp = new Boolean[ints.length][sum/2+1];
        return equalSubsetSumPartitionWithMinDiffRecursive(dp, ints, sum/2,  0);
    }

    private static boolean equalSubsetSumPartitionWithMinDiffRecursive(Boolean[][] dp, int[] ints, int sum, int index) {
        if (sum==0)
            return true;
        if (ints.length <= index || ints.length==0)
            return false;

        if (dp[index][sum] == null){
            if (ints[index] <= sum)
                if (equalSubsetSumPartitionRecursive(dp, ints, sum - ints[index], index++)){
                    dp[index][sum] = true;
                    return true;
                }

            dp[index][sum] =  equalSubsetSumPartitionRecursive(dp, ints, sum, index++);
        }
        return dp[index][sum];
    }

    private static boolean equalSubsetSumPartitionRecursive(Boolean[][] dp, int[] ints, int sum, int index) {
        if (sum==0)
            return true;
        if (ints.length <= index || ints.length==0)
            return false;

        if (dp[index][sum] == null){
            if (ints[index] <= sum)
                if (equalSubsetSumPartitionRecursive(dp, ints, sum - ints[index], index++)){
                    dp[index][sum] = true;
                    return true;
                }

            dp[index][sum] =  equalSubsetSumPartitionRecursive(dp, ints, sum, index++);
        }
        return dp[index][sum];
    }

    private static int solveKnapsack(int[] profits, int[] weights, int capacity){

        if (capacity<=0 || profits.length == 0 || weights.length != profits.length)
            return 0;

        int[][] dp = new int[profits.length][capacity+1];
        for (int i=0; i<profits.length;i++){
            dp[i][0] = 0;
        }
        /*for (int i=0; i<=capacity; i++)
            dp[0][i] = 0;*/

        for (int i=1; i<profits.length;i++){
            for (int j=1; j<=capacity; j++){
                if (j < weights[i])
                    dp[i][j] = dp[i-1][j];
                else
                    dp[i][j] = Math.max(dp[i-1][j], profits[i] + dp[i-1][j-weights[i]]);
            }
        }
        return dp[profits.length-1][capacity];
    }

    private static int maximumProfit(int[] profits, int[] weights, int capacity) {
        Integer[][] dp = new Integer[profits.length][capacity+1];
        return maximumProfitRecursive(dp, profits, weights, capacity, 0);
    }

    private static int maximumProfitRecursive(Integer[][] dp, int[] profits, int[] weights, int capacity, int index) {


        if (capacity <=0 || profits.length <= index)
            return 0;
        if (dp[index][capacity] != null)
            return dp[index][capacity];

        int profit1=0;
        if (weights[index] <= capacity)
            profit1 = profits[index] + maximumProfitRecursive(dp, profits, weights, capacity-weights[index], index+1);

        int profit2 = maximumProfitRecursive(dp, profits, weights, capacity, index+1);

        dp[index][capacity] = Math.max(profit1, profit2);
        return dp[index][capacity];
    }
}
