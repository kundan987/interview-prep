package educative;

import java.util.LinkedHashSet;
import java.util.Set;

public class SlowFastPointers {

    Node head;

    static class Node{
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        SlowFastPointers slowFastPointers = new SlowFastPointers();
        slowFastPointers.head = new Node(2);
        slowFastPointers.head.next = new Node(4);
        slowFastPointers.head.next.next = new Node(6);
        slowFastPointers.head.next.next.next = new Node(8);
        slowFastPointers.head.next.next.next.next = new Node(10);
        slowFastPointers.head.next.next.next.next.next = new Node(12);
        //slowFastPointers.head.next.next.next.next.next.next = new Node(1);
       // slowFastPointers.head.next.next.next.next = slowFastPointers.head.next.next;

        System.out.println("Has Cycle : " + hasCycle(slowFastPointers.head));
        System.out.println("Start node of Cycle : " + startNodeOfCycle(slowFastPointers.head).data);
        System.out.println("Happy number or not ? : "+ happyNumber(23));
        System.out.println("Happy number or not ? : "+ happyNumberWithoutSpace(12));
        System.out.println("Happy number or not ? : "+ happyNumberWithoutSpace(23));
        System.out.println("Middle of linked list : "+ middleNode(slowFastPointers.head).data);
        System.out.println("LinkedList is palindrome ? : "+ linkedListPalindrome(slowFastPointers.head));
        printList(slowFastPointers.head);
        rearrangeLinkedList(slowFastPointers.head);
        printList(slowFastPointers.head);
    }

    private static void rearrangeLinkedList(Node head) {
        Node middle = middleNode(head);
        Node headSecondHalf = reverse(middle);
        Node headFirstHalf = head;

        while (headFirstHalf != null && headSecondHalf != null){

            Node next = headFirstHalf.next;
            headFirstHalf.next = headSecondHalf;
            headFirstHalf = next;

            next = headSecondHalf.next;
            headSecondHalf.next = headFirstHalf;
            headSecondHalf = next;
        }

        if (headFirstHalf != null)
            headFirstHalf.next = null;
    }

    private static void printList(Node node) {
        while (node != null){
            System.out.print(node.data + " -> ");
            node = node.next;
        }
        System.out.println();
    }

    private static boolean linkedListPalindrome(Node node) {

        Node middle = middleNode(node);
        Node reverse = reverse(middle);
        Node reverseCopy = reverse;


        while (node != null){
            if (node.data != reverse.data)
                break;
            node = node.next;
            reverse = reverse.next;
        }
        reverse(reverseCopy);
        if (node == null || reverse == null)
            return true;
        return false;
    }

    private static Node reverse(Node node) {
        Node prev = null;
        Node next = null;
        Node curr = node;

        while (curr != null){
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }

    private static Node middleNode(Node head) {
        Node slow = head, fast = head;

        while (fast != null && fast.next != null){
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    private static boolean happyNumber(int num) {
        Set<Integer> set = new LinkedHashSet<>();
        int sum =num;
        while (sum != 1){
            sum = sumOfSquareOfDigit(sum);
            if (set.contains(sum))
                return false;
            set.add(sum);
        }
        return true;
    }
    private static boolean happyNumberWithoutSpace(int num) {
        int slow = num, fast = num;
        do{
            slow = sumOfSquareOfDigit(slow);
            fast = sumOfSquareOfDigit(sumOfSquareOfDigit(fast));
        } while (slow != fast);

        return slow == 1;
    }

    private static int sumOfSquareOfDigit(int n){
        int sum =0;
        while (n>0){
            int r = n%10;
            n = n/10;
            sum+=r*r;
        }
        return sum;
    }

    private static Node startNodeOfCycle(Node node) {
        int length=0;
        if (node==null || node.next==null)
            return null;
        Node slow = node;
        Node fast = node.next;

        while(fast != null && fast.next != null){
            if (slow==fast){
                length = findLength(slow);
                break;
            }

            slow= slow.next;
            fast=fast.next.next;
        }
        return findStartNode(node, length);
    }

    private static Node findStartNode(Node node, int length) {
        Node ptr1 = node;
        Node ptr2 = node;

        while (length > 0){
            ptr1 = ptr1.next;
            length--;
        }

        while (ptr1 != ptr2){
            ptr1 = ptr1.next;
            ptr2 = ptr2.next;
        }
        return ptr1;
    }

    private static int findLength(Node slow) {
        Node ptr1 = slow;
        int count =0;
        do{
            ptr1 = ptr1.next;
            count++;
        } while (ptr1 != slow);
        return count;
    }

    private static boolean hasCycle(Node node) {
        if (node==null || node.next==null)
            return false;
        Node slow = node;
        Node fast = node.next;

        while(fast != null && fast.next != null){
            if (slow==fast)
                return true;
            slow= slow.next;
            fast=fast.next.next;
        }
        return false;
    }

}
