package educative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TwoPointers {

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 6};
        int[] b = {2, 3, 3, 3, 6, 9, 9,10};
        int[] c = {3, 2, 3, 6, 3, 10, 9, 3};
        int[] d = {-3, -1, 0, 1, 2};
        int[] e = {-3,-3, 0, 1, 2,2,2, -1, 1, -2};

        System.out.println("Indices are : " + Arrays.toString(pairSum(a, 6)));
        System.out.println("Length of array after removing duplicates is : " + removeDuplicates(b));

        System.out.println(Arrays.toString(b));
        System.out.println("Length of array after removing duplicates keys is : " + removeGivenKey(c, 3));
        System.out.println(Arrays.toString(c));
        System.out.println("Squaring a sorted array : " + Arrays.toString(squareSortedArray(d)));
        System.out.println("unique triplet sum to zero : " + tripletSumZero(e));
        System.out.println("unique triplet sum to zero : " + tripletSumZero(new int[]{-5, 2, -1, -2, 3}));
        System.out.println("Sort arrays of 0,1,2 : " + Arrays.toString(sort012(new int[]{1, 0, 2, 1, 0})));
        System.out.println("triplet sum close to target : " + tripletSumCloseToTarget(new int[]{-2, 0, 1, 2}, 2));
        System.out.println("triplet sum close to target : " + tripletSumCloseToTarget(new int[]{-3, -1, 1, 2}, 1));
        System.out.println("triplet with smaller sum than target : " + tripletSumSmallerToTarget(new int[]{-1, 4, 2, 1, 3}, 5));
        System.out.println("triplet with smaller sum than target : " + tripletSumSmallerToTarget(new int[]{-1, 0, 2, 3}, 3));
        System.out.println("String equals : " + stringsEqual("xy#z", "xzz#"));
        System.out.println("String equals : " + stringsEqual("xy#z", "xyz#"));
        System.out.println("String equals : " + stringsEqual("xp#", "xyz##"));
        System.out.println("String equals : " + stringsEqual("xywrrmp", "xywrrmu#p"));
        System.out.println("unique triplet sum to zero : " + tripletSumZero(new int[]{-1, 0, 1, 2, -1, -4}));
    }

    private static boolean stringsEqual(String s1, String s2) {

        int l1 = s1.length()-1;
        int l2 = s2.length()-1;
        int count=0;
        while (l1>=0 || l2>=0){

            if (s1.charAt(l1) == '#'){
                int m1 = l1;
                while (s1.charAt(m1) == '#'){
                    m1--;
                    count++;
                }
                l1=l1 - count*2;
                count=0;
                continue;
            }
            if (s2.charAt(l2) == '#'){
                int m2 = l2;
                while (s2.charAt(m2) == '#'){
                    m2--;
                    count++;
                }
                l2=l2 - count*2;
                count=0;
                continue;
            }
            if (s1.charAt(l1) == s2.charAt(l2)){
                l1--;
                l2--;
            }else
                return false;
        }
        if (l1==l2)
            return true;
        else return false;

    }

    private static int tripletSumSmallerToTarget(int[] a, int target) {

        Arrays.sort(a);
        int count=0;
        for (int i=0; i<a.length-2;i++){
            count += countTriplet(a,target, a[i], i+1);
        }
        return count;
    }

    private static int countTriplet(int[] a, int target, int first, int start) {
        int left = start;
        int count =0;
        int right = a.length-1;
        while (left<right){
            if (first +a[left] + a[right] < target){
                count = count+right-left;
                left++;
            }else
                right--;
        }
        return count;
    }

    private static int tripletSumCloseToTarget(int[] a, int target) {
        
        Arrays.sort(a);
        int smallestSum=Integer.MAX_VALUE;
        for (int i=0; i<a.length-2;i++){
            smallestSum = Math.min(searchPair(a,target, a[i], i+1), smallestSum);
        }
        return smallestSum;
    }

    private static int searchPair(int[] a, int targetSum, int first, int start) {
        int smallestDiff=Integer.MAX_VALUE;
        int left = start;
        int right = a.length-1;
        while (left<right){
            int targetDiff = targetSum - (first +a[left] + a[right]);
            if ( targetDiff == 0) {
                return targetSum-targetDiff;
            }
            if (smallestDiff > targetDiff){
                smallestDiff = targetDiff;
            }
            if (targetDiff > 0)
                left++;
            else
                right--;
        }
        return targetSum-smallestDiff;
    }

    private static int[] sort012(int[] a) {

        int low =0, high = a.length-1;
        for (int i=0; i< high;){

            if (a[i] == 2){
                int temp = a[high];
                a[high] = a[i];
                a[i] = temp;
                high--;
            } else if (a[i]==0){
                int temp = a[low];
                a[low] = a[i];
                a[i] = temp;
                low++;
                i++;
            } else
                i++;
        }

        return a;

    }

    private static List<List<Integer>> tripletSumZero(int[] e) {

        Arrays.sort(e);
        List<List<Integer>> triplets = new ArrayList<>();
        for(int i =0 ; i < e.length - 2; i++){
            if (i > 0 && e[i] == e[i-1])
                continue;
            searchPair(e, -e[i], i+1, triplets);
        }
        return triplets;
    }

    private static void searchPair(int[] a, int targetSum, int start, List<List<Integer>> triplets) {

        int end = a.length-1;

        while (start < end){
            if (a[start]+a[end] == targetSum){
                triplets.add(Arrays.asList(-targetSum, a[start], a[end]));
                start++;
                end--;
                while (start < end && a[start] == a[start-1])
                    start++;
                while (start < end && a[end] == a[end+1])
                    end--;
            } else if(a[start]+a[end] < targetSum){
                start++;
            } else {
                end--;
            }
        }
    }

    private static int[] squareSortedArray(int[] d) {
        int start = 0, end = d.length-1, k=d.length-1;
        int[] result = new int[d.length];
        while (start < end){
            if (d[start] * d[start] > d[end]*d[end]){
                result[k--] = d[start] * d[start];
                start++;
            } else {
                result[k--] = d[end]*d[end];
                end--;
            }
        }
        return result;
    }

    private static int removeGivenKey(int[] c, int k) {
        int nonDuplicates = 0;
        for(int i = 0; i< c.length ; i++){
            if (c[i] != k){
                c[nonDuplicates] = c[i];
                nonDuplicates++;
            }
        }
        return nonDuplicates;
    }

    private static int removeDuplicates(int[] b) {
        int nonDuplicates = 1;
        for(int i = 1; i< b.length ; i++){
            if (b[nonDuplicates-1] != b[i]){

                b[nonDuplicates] = b[i];
                nonDuplicates++;
            }
        }
        return nonDuplicates;
    }

    private static int[] pairSum(int[] a, int k) {
        int start =0, end = a.length-1;
        int[] result = new int[2];
        while (start < end){
            if (a[start] + a[end] == k){
                result[0] = start;
                result[1] = end;
                break;
            } else if (a[start] + a[end] < k){
                start++;
            } else
                end--;
        }
        return result;
    }
}
