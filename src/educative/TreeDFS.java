package educative;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class TreeDFS {

    private static int treeDiameter=0;
    private static int maxSum=0;
    TreeNode root;

    public static void main(String[] args) {
        TreeDFS tree = new TreeDFS();
        tree.root = new TreeNode(2);
        tree.root.left = new TreeNode(-1);/*
        tree.root.left.left = new TreeNode(4);
        tree.root.left.right = new TreeNode(5);
        tree.root.right = new TreeNode(9);
        tree.root.right.left = new TreeNode(2);
        tree.root.right.right = new TreeNode(2);*/

        System.out.println("Has sum ? : " + hasSum(tree.root, 12));
        System.out.println("All sum path : " + allPathSum(tree.root, 12));
        System.out.println("Root to leaf paths : " + rootToLeafPath(tree.root));
        System.out.println("Root to leaf path maximum sum : " + rootToLeafPathMaximumSum(tree.root, 0));
        System.out.println("Sum path numbers : " + sumPathNumbers(tree.root, 0));
        System.out.println("path with given sequence ? : " + pathSequence(tree.root, new int[]{1,7,5},0));
        System.out.println("Count paths given sum : " + countPaths(tree.root, 12));
        System.out.println("longest path between any two leaf nodes : " + treeDiameter(tree.root));
        System.out.println("path with the maximum sum : " + maxSumPath(tree.root));
    }

    private static int maxSumPath(TreeNode root){
        maxSumPathRecursive(root);
        return maxSum;
    }

    private static int maxSumPathRecursive(TreeNode root) {

        if (root==null)
            return 0;

        int ld = maxSumPathRecursive(root.left);
        int rd = maxSumPathRecursive(root.right);

        int sum = ld + rd + root.data;
        maxSum = Math.max(maxSum, sum);
        return ld > rd ? ld+root.data : rd+root.data;
    }

    private static int treeDiameter(TreeNode root){
        treeHeight(root);
        return treeDiameter;
    }

    private static int treeHeight(TreeNode root) {

        if (root==null)
            return 0;

        int ld = treeHeight(root.left);
        int rd = treeHeight(root.right);

        int diameter = ld + rd + 1;
        treeDiameter = Math.max(treeDiameter, diameter);
        return ld > rd ? ld+1 : rd+1;
    }

    private static int countPaths(TreeNode root, int givenSum) {
        List<Integer> currentPath = new ArrayList<>();
        return  countPathsRecursive(root, currentPath, givenSum);
    }

    private static int countPathsRecursive(TreeNode root, List<Integer> currentPath, int givenSum) {
        if (root == null)
            return 0;
        currentPath.add(root.data);
        int pathCount=0, pathSum=0;
        ListIterator<Integer> iterator = currentPath.listIterator(currentPath.size());
        while (iterator.hasPrevious()){
            pathSum+=iterator.previous();
            if (pathSum==givenSum)
                pathCount++;
        }
        pathCount+=countPathsRecursive(root.left, currentPath, givenSum);
        pathCount+=countPathsRecursive(root.right, currentPath, givenSum);
        currentPath.remove(currentPath.size()-1);
        return pathCount;

    }

    private static boolean pathSequence(TreeNode root, int[] sequence, int sequenceIndex) {

        if (root == null)
            return false;

        if (sequenceIndex >= sequence.length || root.data != sequence[sequenceIndex])
            return false;

        if (root.left == null && root.right == null && sequenceIndex == sequence.length-1)
            return true;

        return pathSequence(root.left, sequence, sequenceIndex+1) || pathSequence(root.right, sequence, sequenceIndex+1);
    }

    private static int sumPathNumbers(TreeNode root, int sum) {

        if (root==null)
            return 0;

        sum = sum*10+root.data;

        if (root.left == null && root.right == null)
            return sum;
        return sumPathNumbers(root.left, sum) + sumPathNumbers(root.right, sum);
    }

    private static int rootToLeafPathMaximumSum(TreeNode root, int sum) {

        if (root==null)
            return 0;

        sum += root.data;

        if (root.left == null && root.right == null)
            return sum;

        return Math.max(rootToLeafPathMaximumSum(root.left, sum), rootToLeafPathMaximumSum(root.right, sum));
    }

    private static List<List<Integer>> rootToLeafPath(TreeNode root) {
        List<List<Integer>> rootToLeafPath = new ArrayList<>();
        List<Integer> currentPath = new ArrayList<>();
        rootToLeafPath(root, currentPath, rootToLeafPath);
        return rootToLeafPath;

    }

    private static void rootToLeafPath(TreeNode root, List<Integer> currentPath, List<List<Integer>> rootToLeafPath) {
        if (root==null)
            return;

        currentPath.add(root.data);
        if (root.left == null && root.right == null)
            rootToLeafPath.add(new ArrayList<>(currentPath));

        rootToLeafPath(root.left,  currentPath, rootToLeafPath);
        rootToLeafPath(root.right, currentPath, rootToLeafPath);

        currentPath.remove(currentPath.size()-1);

    }

    private static List<List<Integer>> allPathSum(TreeNode root, int sum) {
        List<List<Integer>> allPathSum = new ArrayList<>();
        List<Integer> currentPathSum = new ArrayList<>();
        allPathSum(root, sum, currentPathSum, allPathSum);
        return allPathSum;
    }

    private static void allPathSum(TreeNode root, int sum, List<Integer> currentPathSum, List<List<Integer>> allPathSum) {
        if (root==null)
            return;

        currentPathSum.add(root.data);
        if (root.left == null && root.right == null && root.data == sum)
            allPathSum.add(new ArrayList<>(currentPathSum));

            allPathSum(root.left, sum-root.data, currentPathSum, allPathSum);
            allPathSum(root.right, sum-root.data, currentPathSum, allPathSum);

        currentPathSum.remove(currentPathSum.size()-1);

    }

    private static boolean hasSum(TreeNode root, int sum) {

        if (root == null)
            return false;

        if (root.left == null && root.right == null && root.data == sum)
            return true;

        return hasSum(root.left, sum-root.data) || hasSum(root.right, sum-root.data);
    }
}
