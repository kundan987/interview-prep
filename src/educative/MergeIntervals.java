package educative;

import java.util.*;

class Interval {
    int start;
    int end;
    int load;

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public Interval(int start, int end, int load) {
        this.start = start;
        this.end = end;
        this.load = load;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Interval{");
        sb.append("start=").append(start);
        sb.append(", end=").append(end);
        sb.append('}');
        return sb.toString();
    }
}
class EmployeeInterval {
    Interval interval;
    Integer employeeIndex;
    Integer intervalIndex;

    public EmployeeInterval(Interval interval, Integer employeeIndex, Integer intervalIndex) {
        this.interval = interval;
        this.employeeIndex = employeeIndex;
        this.intervalIndex = intervalIndex;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EmployeeInterval{");
        sb.append("interval=").append(interval);
        sb.append(", employeeIndex=").append(employeeIndex);
        sb.append(", intervalIndex=").append(intervalIndex);
        sb.append('}');
        return sb.toString();
    }
}

public class MergeIntervals {

    public static void main(String[] args) {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 4));
        intervals.add(new Interval(2, 5));
        intervals.add(new Interval(7, 9));
        System.out.println("Merged intervals");
        System.out.println(MergeIntervals.merge(intervals));

        intervals = new ArrayList<>();
        intervals.add(new Interval(6, 7));
        intervals.add(new Interval(2, 4));
        intervals.add(new Interval(5, 9));
        System.out.println("Merged intervals");
        System.out.println(MergeIntervals.merge(intervals));

        intervals = new ArrayList<>();
        intervals.add(new Interval(1, 4));
        intervals.add(new Interval(2, 6));
        intervals.add(new Interval(3, 5));
        System.out.println("Merged intervals");
        System.out.println(MergeIntervals.merge(intervals));

        intervals = new ArrayList<>();
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(5, 7));
        intervals.add(new Interval(8, 12));
        System.out.println("Merged intervals after insert");
        System.out.println(MergeIntervals.insert(intervals, new Interval(4, 6)));

        List<Interval> interval1 = new ArrayList<>();
        interval1.add(new Interval(1, 3));
        interval1.add(new Interval(5, 6));
        interval1.add(new Interval(7, 9));

        List<Interval> interval2 = new ArrayList<>();
        interval2.add(new Interval(2, 3));
        interval2.add(new Interval(5, 7));
        System.out.println("Merged intervals after intersection");
        System.out.println(MergeIntervals.intersection(interval1, interval2));

        List<Interval> interval3 = new ArrayList<>();
        interval3.add(new Interval(1, 3));
        interval3.add(new Interval(5, 7));
        interval3.add(new Interval(9, 12));

        List<Interval> interval4 = new ArrayList<>();
        interval4.add(new Interval(5, 10));
        System.out.println("Merged intervals after intersection");
        System.out.println(MergeIntervals.intersection(interval3, interval4));

        List<Interval> interval5 = new ArrayList<>();
        interval5.add(new Interval(1, 4));
        interval5.add(new Interval(2, 5));
        interval5.add(new Interval(7, 9));
        System.out.println("Able to attend all appointments ? : " + attendAppointment(interval5));

        List<Interval> interval6 = new ArrayList<>();
        interval6.add(new Interval(6, 7));
        interval6.add(new Interval(2, 4));
        interval6.add(new Interval(8, 12));
        System.out.println("Able to attend all appointments ? : " + attendAppointment(interval6));

        List<Interval> interval7 = new ArrayList<>();
        interval7.add(new Interval(4, 5));
        interval7.add(new Interval(2, 3));
        interval7.add(new Interval(3, 6));
        interval7.add(new Interval(5, 7));
        interval7.add(new Interval(7, 8));
        conflictingAppointment(interval7);

        List<Interval> interval8 = new ArrayList<>();
        interval8.add(new Interval(1, 4));
        interval8.add(new Interval(2, 5));
        interval8.add(new Interval(7, 9));

        System.out.println("Minimum number of rooms to hold meetings : " + minimumRoomMeetings(interval8));

        List<Interval> interval9 = new ArrayList<>();
        interval9.add(new Interval(6, 7));
        interval9.add(new Interval(2, 4));
        interval9.add(new Interval(8, 12));
        interval9.add(new Interval(12, 14));
        System.out.println("Minimum number of rooms to hold meetings : " + minimumRoomMeetings(interval9));

        List<Interval> interval10 = new ArrayList<>();
        interval10.add(new Interval(1, 4, 3));
        interval10.add(new Interval(2, 5, 4));
        interval10.add(new Interval(7, 9, 6));

        System.out.println("Maximum CPU load : " + maximumCpuLoad(interval10));

        List<Interval> interval11 = new ArrayList<>();
        interval11.add(new Interval(6, 7, 10));
        interval11.add(new Interval(2, 4, 11));
        interval11.add(new Interval(8, 12, 15));

        System.out.println("Maximum CPU load : " + maximumCpuLoad(interval11));

        List<Interval> interval12 = new ArrayList<>();
        interval12.add(new Interval(1, 4, 2));
        interval12.add(new Interval(2, 4, 1));
        interval12.add(new Interval(3, 6, 5));

        System.out.println("Maximum CPU load : " + maximumCpuLoad(interval12));

        List<Interval> emp1 = new ArrayList<>();
        emp1.add(new Interval(1, 3));
        emp1.add(new Interval(5, 6));
        List<Interval> emp2 = new ArrayList<>();
        emp2.add(new Interval(2, 3));
        emp2.add(new Interval(6, 8));
        List<List<Interval>> list = new ArrayList<>();
        list.add(emp1);
        list.add(emp2);
        System.out.println("Employees free time : " + employeeFreeTime(list));
        System.out.println("Employees free time : " + employeeFreeTimeOptimised(list));
    }

    public int[][] merge(int[][] intervals) {
        if(intervals.length == 0)
            return new int[0][0];
        if(intervals[0].length <1)
            return new int[0][0];
        Arrays.sort(intervals, (a,b)->a[0]-b[0]);

        List<int[]> res = new ArrayList<>();
        int i=0;
        int start = intervals[0][0];
        int end = intervals[0][1];

        while (i<intervals.length){
            int[] interval = intervals[i];

            if (interval[0] <= end){
                end = Math.max(interval[1], end);
            } else {
                res.add(new int[]{start, end});
                start = interval[0];
                end = interval[1];
            }
            i++;
        }
        res.add(new int[]{start, end});
        int[][] ar = new int[res.size()][2];
        int j=0;
        for(int[] aa : res){
            ar[j] = aa;
            j++;
        }
        return ar;

    }

    private static List<Interval> employeeFreeTimeOptimised(List<List<Interval>> schedule){
        List<Interval> freeTime = new ArrayList<>();

        PriorityQueue<EmployeeInterval> heap = new PriorityQueue<>((a,b) -> Integer.compare(a.interval.start, b.interval.start));

        for (int i=0; i< schedule.size(); i++)
            heap.offer(new EmployeeInterval(schedule.get(i).get(0), i, 0));

        Interval previousInterval = heap.peek().interval;

        while (!heap.isEmpty()){
            EmployeeInterval queueTop = heap.poll();
            if (previousInterval.end < queueTop.interval.start){
                freeTime.add(new Interval(previousInterval.end, queueTop.interval.start));
                previousInterval = queueTop.interval;
            } else if (previousInterval.end < queueTop.interval.end){
                previousInterval = queueTop.interval;
            }

            List<Interval> employeeSchedule = schedule.get(queueTop.employeeIndex);
            if (employeeSchedule.size() > queueTop.intervalIndex+1){
                heap.offer(new EmployeeInterval(employeeSchedule.get(queueTop.intervalIndex+1), queueTop.employeeIndex, queueTop.intervalIndex+1));
            }
        }
        return freeTime;
    }

    private static List<Interval> employeeFreeTime(List<List<Interval>> empList) {
        List<Interval> freeTime = new ArrayList<>();
        List<Interval> intervals = new ArrayList<>();
        for (List<Interval> list : empList){
            intervals.addAll(list);
        }
        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));
        for (int i=1; i<intervals.size(); i++){
            if (intervals.get(i).start > intervals.get(i-1).end)
                freeTime.add(new Interval(intervals.get(i-1).end, intervals.get(i).start));
        }
        return freeTime;
    }

    private static int maximumCpuLoad(List<Interval> intervals) {
        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));
        PriorityQueue<Interval> heap = new PriorityQueue<>(intervals.size(), Comparator.comparingInt(Interval::getEnd));
        int maxCpuLoad=0, currentCpuLoad=0;
        for (Interval interval : intervals){
            while (!heap.isEmpty() && interval.start >= heap.peek().end)
                currentCpuLoad -= heap.poll().load;
            heap.offer(interval);
            currentCpuLoad += interval.load;
            maxCpuLoad = Math.max(maxCpuLoad, currentCpuLoad);
        }
        return maxCpuLoad;
    }

    private static int minimumRoomMeetings(List<Interval> intervals) {
        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));

        PriorityQueue<Interval> heap = new PriorityQueue<>(intervals.size(), Comparator.comparingInt(Interval::getEnd));
        System.out.println(intervals);
        int minRooms=0;
        for (Interval interval : intervals){
            while (!heap.isEmpty() && interval.start >= heap.peek().end)
                heap.poll();
            heap.offer(interval);

            minRooms = Math.max(minRooms, heap.size());
        }
        return minRooms;
    }

    private static boolean attendAppointment(List<Interval> intervals) {
        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));
        for (int i=0; i<intervals.size()-1; i++){
            if (intervals.get(i).end > intervals.get(i+1).start)
                return false;
        }
        return true;
    }

    private static void conflictingAppointment(List<Interval> intervals) {
        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));

        for (int i=1; i<intervals.size()-1; i++){
            if (intervals.get(i-1).end > intervals.get(i).start){
                System.out.println(intervals.get(i-1) + " " + intervals.get(i));
                intervals.remove(intervals.get(i));
                i--;
            }

        }
        //System.out.println(intervals);
    }

    private static List<Interval> intersection(List<Interval> interval1, List<Interval> interval2) {
        List<Interval> mergedInterval = new ArrayList<>();
        int i=0, j=0;
        while (i< interval1.size() && j < interval2.size() && interval1.get(i).end < interval2.get(j).start){
                i++;
        }
        while (i< interval1.size() && j < interval2.size() && interval1.get(i).end >= interval2.get(j).start){
            int start = Math.max(interval1.get(i).start, interval2.get(j).start);
            int end = Math.min(interval1.get(i).end, interval2.get(j).end);
            mergedInterval.add(new Interval(start, end));

            if (interval1.get(i).end > interval2.get(j).end)
                j++;
            i++;
        }
        return mergedInterval;
    }

    private static List<Interval> insert(List<Interval> intervals, Interval newInterval) {

        if (intervals == null || intervals.isEmpty())
            return Arrays.asList(newInterval);
        List<Interval> mergedInterval = new ArrayList<>();

        int i=0;
        while (i<intervals.size() && intervals.get(i).end < newInterval.start){
            mergedInterval.add(intervals.get(i++));
        }

        while (i<intervals.size() && intervals.get(i).start <= newInterval.end){
            newInterval.start = Math.min(intervals.get(i).start, newInterval.start);
            newInterval.end = Math.max(intervals.get(i).end, newInterval.end);
            i++;
        }
        mergedInterval.add(newInterval);

        while (i< intervals.size())
            mergedInterval.add(intervals.get(i++));

        return mergedInterval;
    }

    private static List<Interval> merge(List<Interval> intervals) {
        if (intervals.size() < 2)
            return intervals;

        Collections.sort(intervals, Comparator.comparingInt(Interval::getStart));
        Collections.sort(intervals, (a,b) -> a.start - b.start);
        Collections.sort(intervals, (a,b) -> Integer.compare(a.start, b.start));
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return Integer.compare(o1.start, o2.start);
            }
        });
        List<Interval> mergedInterval = new ArrayList<>();
        Iterator<Interval> iterator = intervals.iterator();
        Interval interval = iterator.next();
        int start = interval.start;
        int end = interval.end;

        while (iterator.hasNext()){
            interval = iterator.next();

            if (interval.start <= end){
                end = Math.max(interval.end, end);
            } else {
                mergedInterval.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }
        mergedInterval.add(new Interval(start, end));
        return mergedInterval;
    }
}
