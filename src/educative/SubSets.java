package educative;

import java.util.*;

class ParenthesesString{
    String str;
    int openCount;
    int closeCount;

    public ParenthesesString(String str, int openCount, int closeCount) {
        this.str = str;
        this.openCount = openCount;
        this.closeCount = closeCount;
    }
}

public class SubSets {

    static Map<String, List<Integer>> map = new HashMap<>();
    static Map<Integer, Integer> integerMap = new HashMap<>();

    public static void main(String[] args) {
        System.out.println("List of subsets : " + SubSets.subSets(new int[] {1,5,3}));
        System.out.println("List of subsets : " + SubSets.subSetsDuplicates(new int[] {1,3,3}));
        System.out.println("Find Permutations : " + SubSets.findPermutations(new int[] {1,5,3}));
        System.out.println("Find Permutations : " + SubSets.permutation(new int[] {1,2,3}));
        System.out.println("Find Permutations : " + SubSets.permutationDuplicates(new int[] {1,2,2}));
        System.out.println("Letter case string permutation " + letterCaseStringPermutation("ab7c"));
        System.out.println("Generate valid parentheses : " + generateValidParentheses(3));
        System.out.println("Expression evaluations : " + diffWaysToEvaluateExpression("1+2*3"));
        findUniqueTree(3);
        System.out.println("Count trees : " + countTrees(3));
        System.out.println("List of subsets : " + new SubSets().subsetsss(new int[] {1,5,3}));

    }
    List<List<Integer>> output = new ArrayList();
    int n, k;

    public void backtrack(int first, ArrayList<Integer> curr, int[] nums) {
        // if the combination is done
        if (curr.size() == k)
            output.add(new ArrayList(curr));

        for (int i = first; i < n; ++i) {
            // add i into the current combination
            curr.add(nums[i]);
            // use next integers to complete the combination
            backtrack(i + 1, curr, nums);
            // backtrack
            curr.remove(curr.size() - 1);
        }
    }

    public List<List<Integer>> subsetss(int[] nums) {
        n = nums.length;
        for (k = 0; k < n + 1; ++k) {
            backtrack(0, new ArrayList<Integer>(), nums);
        }
        return output;
    }
    public List<List<Integer>> subsetsss(int[] nums) {
        List<List<Integer>> output = new ArrayList();
        int n = nums.length;

        for (int i = (int)Math.pow(2, n); i < (int)Math.pow(2, n + 1); ++i) {
            // generate bitmask, from 0..00 to 1..11
            String bitmask = Integer.toBinaryString(i).substring(1);
            System.out.println(Integer.toBinaryString(i));

            // append subset corresponding to that bitmask
            List<Integer> curr = new ArrayList();
            for (int j = 0; j < n; ++j) {
                if (bitmask.charAt(j) == '1') curr.add(nums[j]);
            }
            output.add(curr);
        }
        return output;
    }

    private static int countTrees(int n) {
        if (integerMap.containsKey(n))
            return integerMap.get(n);
        if (n<=1)
            return 1;
        int count =0;
        for (int i = 1; i<=n; i++){
            int leftTrees = countTrees(i-1);
            int righTrees = countTrees(n-i);
            count+=leftTrees*righTrees;
        }
        integerMap.put(n,count);
        return count;
    }

    private static void inorder(TreeNode root) {

        if (root != null){
            inorder(root.left);
            System.out.print(root.data + " - ");
            inorder(root.right);
        }
    }

    private static void findUniqueTree(int n) {
        List<TreeNode> result =   findUniqueTreeRecursive(1,n);
        System.out.println("Total trees : " + result.size());
        for (TreeNode root : result){
            System.out.println();
            inorder(root);
            System.out.println();
        }
    }

    private static List<TreeNode> findUniqueTreeRecursive(int start, int end) {
        List<TreeNode> result = new ArrayList<>();

        if (start> end){
            result.add(null);
            return result;
        }

        for (int i= start; i<= end; i++){
            List<TreeNode> leftSubtrees = findUniqueTreeRecursive(start,i-1);
            List<TreeNode> rightSubtrees = findUniqueTreeRecursive(i+1,end);
            for (TreeNode left : leftSubtrees)
                for (TreeNode right : rightSubtrees){
                TreeNode root = new TreeNode(i);
                root.left = left;
                root.right = right;
                result.add(root);
                }
        }
        return result;
    }

    private static List<Integer> diffWaysToEvaluateExpression(String input) {
        if (map.containsKey(input))
            return map.get(input);

        List<Integer> result = new ArrayList<>();

        if (!input.contains("+") && !input.contains("-") && !input.contains("*"))
            result.add(Integer.parseInt(input));
        else {
            for (int i =0; i< input.length(); i++){
                char ch = input.charAt(i);
                if (!Character.isDigit(ch)){
                    List<Integer> leftParts = diffWaysToEvaluateExpression(input.substring(0,i));
                    List<Integer> rightParts = diffWaysToEvaluateExpression(input.substring(i+1));

                    for (int left : leftParts)
                        for (int right : rightParts){
                            if (ch == '+')
                                result.add(left+right);
                            if (ch == '-')
                                result.add(left-right);
                            if (ch == '*')
                                result.add(left*right);
                        }
                }
            }
        }
        map.put(input, result);
        return result;
    }

    private static List<String> generateValidParentheses(int num) {
        List<String> result = new ArrayList<>();
        Queue<ParenthesesString> queue = new LinkedList<>();
        queue.add(new ParenthesesString("", 0, 0));

        while (!queue.isEmpty()){
            ParenthesesString parenthesesString = queue.poll();
            if (parenthesesString.closeCount ==  num && parenthesesString.openCount == num)
                result.add(parenthesesString.str);
            else {

                if (parenthesesString.openCount < num)
                    queue.add(new ParenthesesString(parenthesesString.str + "(", parenthesesString.openCount+1, parenthesesString.closeCount));

                if (parenthesesString.openCount > parenthesesString.closeCount)
                    queue.add(new ParenthesesString(parenthesesString.str + ")", parenthesesString.openCount, parenthesesString.closeCount+1));
            }
        }

        return result;
    }

    private static List<String> letterCaseStringPermutation(String string) {
        List<String> result = new ArrayList<>();
        int i=0;
        result.add(string);
        while (i<string.length()){
            int size = result.size();
            if (Character.isLetter(string.charAt(i)))
            for (int j =0; j < size;j++){
                String current = result.get(j);
                String subCurrent = "" + current.charAt(i);
                current = current.replace(current.charAt(i), subCurrent.toUpperCase().toCharArray()[0]);
                result.add(current);
            }
            i++;
        }
        return  result;
    }
    private static List<List<Integer>> permutationDuplicates(int[] ints) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> input = new ArrayList<>();
        for(int i : ints)
            input.add(i);

        permutationDuplicatesUtil(result, input,ints.length,0, new HashSet<>());
        return result;
    }

    private static void permutationDuplicatesUtil(List<List<Integer>> result, List<Integer> input, int length, int index, Set<Integer> set) {

        if (index==length)
            result.add(new ArrayList<>(input));
        for(int i=index;i<length;i++){
            Collections.swap(input,index,i);
            permutationDuplicatesUtil(result,input,length,index+1, set);
            Collections.swap(input,index,i);
        }
    }

    private static List<List<Integer>> permutation(int[] ints) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> input = new ArrayList<>();
        for(int i : ints)
            input.add(i);

        permutationUtil(result, input,ints.length,0);
        return result;
    }

    private static void permutationUtil(List<List<Integer>> result, List<Integer> input, int length, int index) {

        if (index==length)
            result.add(new ArrayList<>(input));
        for(int i=index;i<length;i++){
            Collections.swap(input,index,i);
            permutationUtil(result,input,length,index+1);
            Collections.swap(input,index,i);
        }
    }


    private static List<List<Integer>> findPermutations(int[] ints) {
        List<List<Integer>> result = new ArrayList<>();
        Queue<List<Integer>> permutation = new LinkedList<>();
        permutation.add(new ArrayList<>());

        for (int i=0; i< ints.length; i++){

            int size = permutation.size();

            for (int j=0; j< size; j++){
                List<Integer> oldPermutation = permutation.poll();
                //System.out.println("oldPermutation : "+ oldPermutation);
                for (int k=0; k <= oldPermutation.size(); k++){

                   List<Integer> currentPermutation = new ArrayList<>(oldPermutation);

                   currentPermutation.add(k, ints[i]);
                    //System.out.println("currentPermutation : "+currentPermutation);
                   if (currentPermutation.size() == ints.length)
                       result.add(currentPermutation);
                   else
                       permutation.add(currentPermutation);
                   //System.out.println("Result : " + result);
                }

            }
        }
        return result;
    }

    private static List<List<Integer>> subSets(int[] ints) {
        List<List<Integer>> subsets = new ArrayList<>();
        subsets.add(new ArrayList<>());
        for (int i=0; i< ints.length; i++){
            int j =0, size = subsets.size();
            while (j < size){
                List<Integer> set = new ArrayList<>(subsets.get(j));
                set.add(ints[i]);
                subsets.add(set);
                j++;
            }

        }

        return subsets;
    }
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> output = new ArrayList();
        output.add(new ArrayList<Integer>());

        for (int num : nums) {
            List<List<Integer>> newSubsets = new ArrayList();
            for (List<Integer> curr : output) {
                newSubsets.add(new ArrayList<Integer>(curr){{add(num);}});
            }
            for (List<Integer> curr : newSubsets) {
                output.add(curr);
            }
        }
        return output;
    }

    private static List<List<Integer>> subSetsDuplicates(int[] ints) {
        List<List<Integer>> subsets = new ArrayList<>();
        subsets.add(new ArrayList<>());
        int startIndex=0, endIndex=0;
        for (int i=0; i< ints.length; i++){
            startIndex = 0;

            if ( i>0 && ints[i] == ints[i-1])
                startIndex = endIndex +1;
            endIndex = subsets.size()-1;
            for (int j = startIndex; j<= endIndex; j++){
                List<Integer> set = new ArrayList<>(subsets.get(j));
                set.add(ints[i]);
                subsets.add(set);
            }
        }

        return subsets;
    }
}
