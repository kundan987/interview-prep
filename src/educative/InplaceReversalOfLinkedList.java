package educative;

public class InplaceReversalOfLinkedList {

    Node head;

    static class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        InplaceReversalOfLinkedList linkedList = new InplaceReversalOfLinkedList();
        linkedList.head = new Node(2);
        linkedList.head.next = new Node(4);
        linkedList.head.next.next = new Node(6);
        linkedList.head.next.next.next = new Node(8);
        linkedList.head.next.next.next.next = new Node(10);/*
        linkedList.head.next.next.next.next.next = new Node(12);
        linkedList.head.next.next.next.next.next.next = new Node(13);
        linkedList.head.next.next.next.next.next.next.next = new Node(14);*/

        /*linkedList.head = linkedList.reverse(linkedList.head);
        //linkedList.head = linkedList.reverseRecursive(linkedList.head);
        linkedList.printList(linkedList.head);*/
      //  linkedList.head = linkedList.reverseSubList(linkedList.head, 2, 4);
      //  linkedList.head = linkedList.reverseFirstKElements(linkedList.head, 4);
       // linkedList.head = linkedList.reverseBasedOnSize(linkedList.head);
      //  linkedList.head = linkedList.reverseEveryKElement(linkedList.head, 3);
      //  linkedList.head = linkedList.reverseAlternateKElement(linkedList.head, 3);
        linkedList.head = linkedList.rotateLinkListByK(linkedList.head, 8);
        linkedList.printList(linkedList.head);
    }

    private Node rotateLinkListByK(Node node, int k) {
        Node current = node;
        int n =0;
        Node prev = null;
        while (current!=null){
            prev = current;
            current=current.next;
            n++;
        }
        k = k%n;
        k = n -k;
        prev.next = node;
        Node start = node;
        for (int i=0; i< k-1; ++i){
            start = start.next;
        }
        node = start.next;
        start.next = null;
        return node;
    }

    private Node reverseAlternateKElement(Node node, int k) {
        Node prev = null;
        Node current = node;

        while (true){
            Node lastNodeOfPreviousPart = prev;
            Node lastNodeOfSubList = current;
            Node next = null;
            for (int i=0; current!=null && i<k; i++){
                next = current.next;
                current.next = prev;
                prev = current;
                current = next;
            }
            if (lastNodeOfPreviousPart != null)
                lastNodeOfPreviousPart.next = prev;
            else
                node = prev;
            lastNodeOfSubList.next = current;

            for (int i=0; current!= null && i<k;i++){
                prev = current;
                current = current.next;
            }
            if (current == null)
                break;
        }
        return node;

    }

    private Node reverseEveryKElement(Node node, int k) {
        Node prev = null;
        Node current = node;

        while (true){
            Node lastNodeOfPreviousPart = prev;
            Node lastNodeOfSubList = current;
            Node next = null;
            for (int i=0; current!=null && i<k; i++){
                next = current.next;
                current.next = prev;
                prev = current;
                current = next;
            }
            if (lastNodeOfPreviousPart != null)
                lastNodeOfPreviousPart.next = prev;
            else
                node = prev;
            lastNodeOfSubList.next = current;
            if (current == null)
                break;
            prev = lastNodeOfSubList;
        }
        return node;

    }

    private Node reverseBasedOnSize(Node head) {

        Node current = head;
        int n =0;
        while (current!=null){
            current=current.next;
            n++;
        }
        if (n%2==0){
            head = reverseSubList(head, 1, n/2);
            head = reverseSubList(head, n/2+1,n);
        } else {
            head = reverseSubList(head, 1, n/2);
            head = reverseSubList(head, n/2+2,n);
        }

        return head;
    }

    private Node reverseFirstKElements(Node head, int k) {
        return reverseSubList(head, 1,k);
    }

    private Node reverseSubList(Node node, int p, int q) {
        if (node == null || node.next==null)
            return node;
        Node prev = null;
        Node current = node;
        Node next = null;
        Node back = null;
        Node start = null;
        for (int i=0 ; current != null && i<p-1;++i){
            back = current;
            current = current.next;
        }
        start = current;
        for (int i=0 ; current != null && i< q-p+1;i++){
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        if (back != null)
            back.next = prev;
        else
            node = prev;
        start.next = current;
        return node;

    }

    private Node reverseRecursive(Node node) {
        Node current = node;
        Node next = null;
        Node prev = null;
        if (current!= null){
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        prev = reverseRecursive(current);
        return prev;
    }

    private void printList(Node head) {
        while (head!= null){
            System.out.print(head.data + " -> ");
            head = head.next;
        }
    }

    private Node reverse(Node node) {
        if (node == null || node.next==null)
            return node;
        Node current = node;
        Node next = null;
        Node prev = null;

        while (current != null){
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }
}
