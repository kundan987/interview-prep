package educative;

public class KthSmallestNumber {
    public static void main(String[] args) {

        System.out.println("Kth Smallest Number : " + bruteForce(3, new int[]{1, 5, 12, 2, 11, 5}));
    }

    private static int bruteForce(int k, int[] arr) {
        int smallest = Integer.MAX_VALUE, result =0;
        for (int i =0; i<arr.length;i++){
            int small = smallest;
            for (int j=0; j< arr.length; j++){
                if (small>arr[j])
                    small = arr[j];
            }
            smallest = small;
        }
        return smallest;
    }
}
