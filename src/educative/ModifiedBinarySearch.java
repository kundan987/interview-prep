package educative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModifiedBinarySearch {
    public static void main(String[] args) {
        System.out.println("Index of key : " + ModifiedBinarySearch.search(new int[]{4, 6, 10}, 10));
        System.out.println("Index of key : " + ModifiedBinarySearch.search(new int[]{10, 6, 4}, 10));
        System.out.println("Index of key : " + ModifiedBinarySearch.searchIterative(new int[]{1, 2, 3, 4, 5, 6, 7}, 5));
        System.out.println("Ceiling Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, 6, false));
        System.out.println("Ceiling Number : " + ceilingFloorNumber(new int[]{1, 3, 8, 10, 15}, 12, false));
        System.out.println("Ceiling Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, 17, false));
        System.out.println("Ceiling Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, -1, false));
        System.out.println("floor Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, 6, true));
        System.out.println("floor Number : " + ceilingFloorNumber(new int[]{1, 3, 8, 10, 15}, 12, true));
        System.out.println("floor Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, 17, true));
        System.out.println("floor Number : " + ceilingFloorNumber(new int[]{4, 6, 10}, -1, true));
        System.out.println("Next letter : " + searchNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'f'));
        System.out.println("Next letter : " + searchNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'b'));
        System.out.println("Next letter : " + searchNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'm'));
        System.out.println("Next letter : " + searchNextLetter(new char[]{'a', 'c', 'f', 'h'}, 'h'));
        System.out.println("Find range : " + Arrays.toString(findRange(new int[]{4, 6, 6, 6, 9}, 6)));
        System.out.println("Minimum diff element : " + minDiffElement(new int[]{4, 6, 10}, 7));
        System.out.println("Minimum diff element : " + minDiffElement(new int[]{1, 3, 8, 10, 15}, 12));
        System.out.println("Bitonic array maximum : " + bitonicArrayMaximum(new int[]{1, 3, 8, 12, 4, 2}));
        System.out.println("Bitonic array maximum : " + bitonicArrayMaximum(new int[]{3, 8, 3, 1}));
        System.out.println("Bitonic array maximum : " + bitonicArrayMaximum(new int[]{1, 3, 8, 12}));
        System.out.println("Bitonic array maximum : " + bitonicArrayMaximum(new int[]{10, 9, 8}));
        System.out.println("Search bitonic array : " + searchBitonicArray(new int[]{1, 3, 8, 4, 3}, 4));
        System.out.println("Search in Rotated Array : " + searchInRotatedArray(new int[]{10, 15, 1, 3, 8}, 15));
        System.out.println("Search in Rotated Array : " + searchInRotatedArray(new int[]{4, 5, 7, 9, 10, -1, 2}, 10));
        System.out.println("Search in Rotated Array : " + searchInRotatedArray(new int[]{3, 7, 3, 3, 3}, 7));
        System.out.println("Rotation Count : " + rotationCount(new int[]{4, 5, 7, 9, 10, -1, 2}));
        System.out.println("Rotation Count : " + rotationCount(new int[]{10, 15, 1, 3, 8}));
        System.out.println("Rotation Count : " + rotationCount(new int[]{3, 3, 7, 3}));
    }

    private static int rotationCount(int[] input) {
        int rotatedIndex = findRotatedIndex(input);
        return rotatedIndex+1;
    }

    private static int searchInRotatedArray(int[] input, int key) {
        int maxIndex = findRotatedIndex(input);
        int index = search(input, key, 0, maxIndex, true);
        if (index != -1)
            return index;
        return search(input, key, maxIndex+1, input.length-1, true);
    }

    private static int findRotatedIndex(int[] input) {
        int start = 0, end = input.length - 1;
        while (start < end) {

            int mid = start + (end - start) / 2;
            if (input[mid] < input[mid-1])
                return mid-1;
            if (input[mid] > input[mid+1])
                end = mid;
            else
                start = mid +1;

        }
        return start;
    }

    private static int searchBitonicArray(int[] input, int key) {

        int maxIndex = bitonicArrayMaximumIndex(input);
        int index = search(input, key, 0, maxIndex, true);
        if (index != -1)
            return index;
        return search(input, key, maxIndex+1, input.length-1, false);
    }

    private static int search(int[] num, int key, int start, int end, boolean isAscending) {
        while (start<=end){

            int mid = start + (end-start)/2;

            if (key == num[mid])
                return mid;

            if (isAscending){
                if (key < num[mid])
                    end = mid-1;
                else
                    start = mid+1;
            } else {
                if (key > num[mid])
                    end = mid-1;
                else
                    start = mid+1;
            }
        }
        return -1;
    }

    private static int bitonicArrayMaximumIndex(int[] input) {
        int start = 0, end = input.length - 1;
        while (start < end) {

            int mid = start + (end - start) / 2;
            if (input[mid] > input[mid+1])
                end = mid;
            else
                start = mid +1;

        }
        return start;
    }

    private static int bitonicArrayMaximum(int[] input) {
        int start = 0, end = input.length - 1;
        while (start < end) {

            int mid = start + (end - start) / 2;
            if (input[mid] > input[mid+1])
                end = mid;
            else
                start = mid +1;

        }
            return input[start];
    }

    private static int minDiffElement(int[] input, int key) {
        int start = 0, end = input.length - 1;

        if (key < input[start])
            return input[start];

        if (key > input[end])
            return input[end];

        while (start <= end) {

            int mid = start + (end - start) / 2;

            if (input[mid] == key)
                return input[mid];

            if (key < input[mid])
                end = mid-1;
            else
                start = mid+1;
        }
        if (input[start] - key < key - input[end])
            return input[start];
        else
            return input[end];
    }

    private static int[] findRange(int[] num, int key) {
        int[] result = new int[]{-1, -1};
        result[0] = searchRange(num, key, false);
        if (result[0] != -1)
            result[1] = searchRange(num, key, true);
        return result;
    }

    private static Integer searchRange(int[] num, int key, boolean findMaxIndex) {
        int start = 0, end = num.length - 1;
        int index = -1;
        while (start <= end) {

            int mid = start + (end - start) / 2;

            if (key < num[mid])
                end = mid-1;
            else if (key > num[mid])
                start = mid+1;
            else {
                index = mid;
                if (findMaxIndex)
                    start = mid+1;
                else
                    end = mid -1;

            }
        }
        return index;
    }

    private static char searchNextLetter(char[] input, char key) {
        int start = 0, end = input.length - 1;
        if (key < input[start] || key > input[end])
            return input[0];
        while (start <= end) {

            int mid = start + (end - start) / 2;

            if (key < input[mid])
                end = mid-1;
            else
                start = mid+1;

        }
        return input[start%input.length];
    }

    private static int ceilingFloorNumber(int[] num, int key, boolean floor) {

        int start = 0, end = num.length - 1;

        if (!floor && key> num[end])
            return -1;
        if (floor && key< num[start])
            return -1;

        while (start <= end) {

            int mid = start + (end - start) / 2;

            if (key == num[mid])
                return mid;

            if (key < num[mid])
                end = mid-1;
            else
                start = mid+1;

        }
        return floor ? end :start;
    }

    private static int searchIterative(int[] num, int key) {
        int start =0, end = num.length-1;

        boolean isAscending = num[start]<num[end];
        while (start<=end){

            int mid = start + (end-start)/2;

            if (key == num[mid])
                return mid;

            if (isAscending){
                if (key < num[mid])
                    end = mid-1;
                else
                    start = mid+1;
            } else {
                if (key > num[mid])
                    end = mid-1;
                else
                    start = mid+1;
            }
        }
        return -1;
    }

    private static int search(int[] num, int key) {
        return searchRecursive(num, 0, num.length-1, key);
    }

    private static int searchRecursive(int[] num, int start, int end, int key) {
        if (start<= end) {
            int mid = (end + start) / 2;
            if (num[mid] == key)
                return mid;
            else if (num[mid] > key){
                if (mid > 1 && num[mid - 1] > num[mid])
                    return searchRecursive(num, mid + 1, end, key);
                else
                    return searchRecursive(num, start, mid, key);
            }
            else if (num[mid] < key){
                    if (mid < end && num[mid + 1] < num[mid])
                        return searchRecursive(num, start, mid, key);
                    else
                        return searchRecursive(num, mid + 1, end, key);
            }

        }
        return -1;
    }
}
