package educative;

import java.util.PriorityQueue;

public class TwoHeaps {

    PriorityQueue<Integer> maxHeap;
    PriorityQueue<Integer> minHeap;

    public TwoHeaps() {
        this.maxHeap = new PriorityQueue<>((a,b) -> b-a);
        this.minHeap = new PriorityQueue<>((a,b) -> a-b);
    }

    public static void main(String[] args) {
        TwoHeaps medianOfAStream = new TwoHeaps();

        medianOfAStream.insert(3);
        medianOfAStream.insert(1);
        System.out.println("Median : " + medianOfAStream.median());
    }

    private double median() {

        if (maxHeap.size() == minHeap.size())
            return maxHeap.peek()/2.0 + minHeap.peek()/2.0;
        else
            return maxHeap.peek();
    }

    private void insert(int num) {

        if (maxHeap.isEmpty() || maxHeap.peek() > num)
            maxHeap.add(num);
        else
            minHeap.add(num);

        if (maxHeap.size() > minHeap.size() +1)
            minHeap.add(maxHeap.poll());
        else if (minHeap.size()> maxHeap.size())
            maxHeap.add(minHeap.poll());
    }
}
