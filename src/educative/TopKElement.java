package educative;

import java.util.*;
class Element{
    int number;
    int frequency;
    int sequence;

    public Element(int number, int frequency, int sequence) {
        this.number = number;
        this.frequency = frequency;
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Element{");
        sb.append("number=").append(number);
        sb.append(", frequency=").append(frequency);
        sb.append(", sequence=").append(sequence);
        sb.append('}');
        return sb.toString();
    }
}

class ElementComparator implements Comparator<Element>{

    @Override
    public int compare(Element o1, Element o2) {
        if(o1.frequency != o2.frequency)
            return o2.frequency - o1.frequency;
        return o2.sequence - o1.sequence;
    }
}

class FrequencyStack {
    int sequence =0;
    Map<Integer, Integer> frequencyMap = new HashMap<>();
    PriorityQueue<Element> maxHeap = new PriorityQueue<>(new ElementComparator());

    public void insert(int num){
        frequencyMap.put(num, frequencyMap.getOrDefault(num, 0)+1);
        maxHeap.offer(new Element(num, frequencyMap.get(num), sequence++));
    }

    public void display(){
        while (!maxHeap.isEmpty()){
            System.out.println(maxHeap.poll());
        }
    }

    public int pop(){
        Element element = maxHeap.poll();

        frequencyMap.put(element.number, frequencyMap.get(element.number)-1);
        if (frequencyMap.get(element.number) == 0)
            frequencyMap.remove(element.number);

        return element.number;
    }
}
class StreamNumber{
    PriorityQueue<Integer> minHeap = new PriorityQueue<>();
    int k;

    public StreamNumber(int[] array, int k) {
        this.k = k;
        for (int i=0; i< array.length; i++)
            add(array[i]);
    }

    public int add(int num){
        minHeap.add(num);
        if (minHeap.size()>this.k)
            minHeap.poll();
        return minHeap.peek();
    }
}
class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int distanceFromOrigin(){
        return (x*x) + (y*y);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Point{");
        sb.append("x=").append(x);
        sb.append(", y=").append(y);
        sb.append('}');
        return sb.toString();
    }
}
public class TopKElement {

    public static void main(String[] args) {
        System.out.println("Find K largest number : " + findKLargestNumber(new int[]{3, 1, 5, 12, 2, 11}, 3));
        System.out.println("Find K largest number : " + findKLargestNumber(new int[]{5, 12, 11, -1, 12}, 3));
        System.out.println("Find Kth smallest number : " + findKthSmallestNumber(new int[]{1, 5, 12, 2, 11, 5}, 3));
        System.out.println("Find Kth smallest number : " + findKthSmallestNumber(new int[]{5, 12, 11, -1, 12}, 3));
        System.out.println("Find Kth smallest number : " + findKthSmallestNumber(new int[]{1, 5, 12, 2, 11, 5}, 4));
        System.out.println("Find K closest point from origin  : " + findKClosestPoint(new Point[]{new Point(1,3), new Point(3,4), new Point(2,-1)}, 2));
        System.out.println("Minimum cost to connect ropes : " + minimumCostToConnectRopes(new int[]{1, 3, 11, 5}));
        System.out.println("find the top ‘K’ frequently occurring numbers : " + findTopKFrequentNumbers(new int[]{1, 3, 5, 12, 11, 12, 11}, 2));
        System.out.println("find the top ‘K’ frequently occurring numbers : " + findTopKFrequentNumbers(new int[]{5, 12, 11, 3, 11}, 2));
        System.out.println("Sort character by frequency : " + sortCharacterByFrequency("Programming"));

        StreamNumber streamNumber = new StreamNumber(new int[]{3, 1, 5, 12, 2, 11}, 4);
        System.out.println("Kth larget number in a stream : " +streamNumber.add(6));
        System.out.println("Kth larget number in a stream : " +streamNumber.add(13));
        System.out.println("Kth larget number in a stream : " +streamNumber.add(4));
        System.out.println("K closest number from given number : " + kClosestNumber(new int[]{5, 6, 7, 8, 9}, 3, 7));
        System.out.println("Maximum distinct elements : " + maximumDistinctElements(new int[]{7, 3, 5, 8, 5, 3, 3}, 2));
        System.out.println("Sum of elements : " + sumOfElements(new int[]{1, 3, 12, 5, 15, 11}, 3, 6));
        System.out.println("Sum of elements : " + sumOfElements(new int[]{3, 5, 8, 7}, 1, 4));
        System.out.println("Rearrange String : " + rearrangeString("aappp"));
        System.out.println("Rearrange String : " + rearrangeString("Programming"));
        System.out.println("Rearrange String : " + rearrangeString("aapa"));
        System.out.println("Rearrange String K distance apart : " + rearrangeStringKDistanceApart("mmpp", 2));
        System.out.println("Rearrange String K distance apart : " + rearrangeStringKDistanceApart("Programming", 3));
        System.out.println("Rearrange String K distance apart : " + rearrangeStringKDistanceApart("aappa", 3));
        System.out.println("Scheduling tasks : " + schedulingTasks(new char[]{'a', 'a', 'a', 'b', 'c', 'c'}, 2));

        FrequencyStack frequencyStack = new FrequencyStack();
        frequencyStack.insert(1);
        frequencyStack.insert(2);
        frequencyStack.insert(3);
        frequencyStack.insert(2);
        frequencyStack.insert(1);
        frequencyStack.insert(2);
        frequencyStack.insert(5);
        frequencyStack.display();/*
        System.out.println("Poped element : " + frequencyStack.pop());
        System.out.println("Poped element : " + frequencyStack.pop());
        System.out.println("Poped element : " + frequencyStack.pop());*/
        int a = -0;
        System.out.println(a);
        Set<Integer> [] rows = new HashSet[9];
        String s = "kundan kumar mishra";
        StringBuilder sb = new StringBuilder(s);
        sb.reverse();
        int large = "2147483648".compareTo("2147483647");
        System.out.println(large);
        s = s.replace(" ", "");
        System.out.println(s);
    }

    private static int schedulingTasks(char[] chars, int k) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        PriorityQueue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>( (a, b) -> b.getValue() - a.getValue());
        for (int i=0; i< chars.length; i++)
            frequencyMap.put(chars[i], frequencyMap.getOrDefault(chars[i], 0)+1);
        maxHeap.addAll(frequencyMap.entrySet());

        int resultCount=0;
        while (!maxHeap.isEmpty()){
            List<Map.Entry<Character, Integer>> waitList = new ArrayList<>();
            int n = k+1;
            for (;n>0 && !maxHeap.isEmpty(); n--){
                resultCount++;
                Map.Entry<Character, Integer> currentEntry = maxHeap.poll();
                currentEntry.setValue(currentEntry.getValue()-1);
                if (currentEntry.getValue()>0)
                    waitList.add(currentEntry);
            }
            maxHeap.addAll(waitList);

            if (!maxHeap.isEmpty())
                resultCount+=n;

        }
        return resultCount;
    }

    private static String rearrangeStringKDistanceApart(String str, int k) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        PriorityQueue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>( (a, b) -> b.getValue() - a.getValue());
        for (int i=0; i< str.length(); i++)
            frequencyMap.put(str.charAt(i), frequencyMap.getOrDefault(str.charAt(i), 0)+1);
        if (frequencyMap.size()<k)
            return "";
        maxHeap.addAll(frequencyMap.entrySet());

        Queue<Map.Entry<Character, Integer>> entryQueue = new LinkedList<>();
        StringBuilder result = new StringBuilder(str.length());

        while (!maxHeap.isEmpty()){

            Map.Entry<Character, Integer> currentEntry = maxHeap.poll();
            result.append(currentEntry.getKey());
            currentEntry.setValue(currentEntry.getValue()-1);
            entryQueue.add(currentEntry);

            if (entryQueue.size() == k){
                Map.Entry<Character, Integer> previousEntry = entryQueue.poll();
                if (previousEntry.getValue() > 0)
                    maxHeap.offer(previousEntry);
            }
        }
        if (result.length() == str.length())
            return new String(result);
        else return "";
    }

    private static String rearrangeString(String str) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        PriorityQueue<Map.Entry<Character, Integer>> maxHeap = new PriorityQueue<>( (a, b) -> b.getValue() - a.getValue());

        for (int i=0; i< str.length(); i++)
            frequencyMap.put(str.charAt(i), frequencyMap.getOrDefault(str.charAt(i), 0)+1);

        for (Map.Entry<Character, Integer> map : frequencyMap.entrySet()){
            maxHeap.add(map);
        }
        Map.Entry<Character, Integer> previousMap = null;
        StringBuilder result = new StringBuilder(str.length());
        while (!maxHeap.isEmpty()){
            Map.Entry<Character, Integer> currentMap = maxHeap.poll();
            if (previousMap!= null && previousMap.getValue() >0)
                maxHeap.offer(previousMap);
            result.append(currentMap.getKey());
            currentMap.setValue(currentMap.getValue()-1);
            previousMap = currentMap;

        }
        if (result.length() == str.length())
            return new String(result);
        else return "";


    }

    private static int sumOfElements(int[] ints, int x, int y) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i=0; i <ints.length; i++){
            minHeap.add(ints[i]);
        }
        int sum =0;y=y-x-1;
        while (!minHeap.isEmpty()){
            if (x>0){
                minHeap.poll();
                x--;
            }
            else if (y>0){
                sum+=minHeap.poll();
                y--;
            }
            if (y==0)
                break;

        }
        return sum;
    }

    private static int maximumDistinctElements(int[] ints, int k) {
        int maximumDistinctElementsCount =0;
        if (ints.length<=k)
            return maximumDistinctElementsCount;

        Map<Integer, Integer> frequencyMap = new HashMap<>();

        PriorityQueue<Map.Entry<Integer, Integer>> minHeap = new PriorityQueue<>( (a, b) -> a.getValue() - b.getValue());

        for (int i=0; i< ints.length; i++)
            frequencyMap.put(ints[i], frequencyMap.getOrDefault(ints[i], 0)+1);

        for (Map.Entry<Integer, Integer> map : frequencyMap.entrySet()){
            if (map.getValue() == 1)
                maximumDistinctElementsCount++;
            else
                minHeap.add(map);
        }
        while (k>0 && !minHeap.isEmpty()){
            k-=minHeap.poll().getValue()-1;
            if (k>=0)
                maximumDistinctElementsCount++;
        }
        if (k>0)
            maximumDistinctElementsCount-=k;

        return maximumDistinctElementsCount;
    }

    private static List<Integer> kClosestNumber(int[] ints, int k, int x) {
        List<Integer> result = new ArrayList<>();
        Map<Integer, Integer> closestMap = new HashMap<>();
        PriorityQueue<Map.Entry<Integer, Integer>> maxheap = new PriorityQueue<>( (a, b) -> b.getValue() - a.getValue());

        for (int i=0; i< ints.length; i++)
            closestMap.put(ints[i], Math.abs(x-ints[i]));

        for (Map.Entry<Integer, Integer> map : closestMap.entrySet()){
            maxheap.add(map);
            if (maxheap.size()>k)
                maxheap.poll();
        }
        while (!maxheap.isEmpty())
            result.add(maxheap.poll().getKey());

        Collections.sort(result);

        return result;
    }

    private static String sortCharacterByFrequency(String str) {
        Map<Character, Integer> frequencyMap = new HashMap<>();
        for (int i=0; i< str.length(); i++)
            frequencyMap.put(str.charAt(i), frequencyMap.getOrDefault(str.charAt(i), 0)+1);
        PriorityQueue<Map.Entry<Character, Integer>> maxheap = new PriorityQueue<>( (a, b) -> b.getValue() -a.getValue());
        for (Map.Entry<Character, Integer> map : frequencyMap.entrySet()){
            maxheap.add(map);
        }
        String result = "";
        int i=0;
        while (!maxheap.isEmpty()){
            int frequency = maxheap.peek().getValue();
            char ch = maxheap.peek().getKey();
            while (frequency>0){
                result+=ch;
                frequency--;
            }
            maxheap.poll();
        }
        return result;

    }

    private static List<Integer> findTopKFrequentNumbers(int[] ints, int k) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        PriorityQueue<Map.Entry<Integer, Integer>> minHeap = new PriorityQueue<>( (a, b) -> a.getValue() -b.getValue());
        for (int i=0; i< ints.length; i++)
            frequencyMap.put(ints[i], frequencyMap.getOrDefault(ints[i], 0)+1);
        for (Map.Entry<Integer, Integer> map : frequencyMap.entrySet()){
            minHeap.add(map);
            if (minHeap.size()>k)
                minHeap.poll();
        }
        List<Integer> result = new ArrayList<>();
        while (!minHeap.isEmpty())
            result.add(minHeap.poll().getKey());

        return result;
    }

    private static int minimumCostToConnectRopes(int[] ints) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int i =0; i<ints.length; i++)
            minHeap.add(ints[i]);
        int result=0, temp=0;
        while (minHeap.size()>1){
            temp = minHeap.poll() +minHeap.poll();
            result+=temp;
            minHeap.add(temp);
        }
        return result;
    }

    private static List<Point> findKClosestPoint(Point[] points, int k) {
        PriorityQueue<Point> maxHeap = new PriorityQueue<>((n1,n2) -> n2.distanceFromOrigin()-n1.distanceFromOrigin());

        for (int i =0; i<points.length; i++){
            if (i<k){
                maxHeap.add(points[i]);
            } else {
                if (points[i].distanceFromOrigin() < maxHeap.peek().distanceFromOrigin()){
                    maxHeap.poll();
                    maxHeap.add(points[i]);
                }
            }
        }
        return new ArrayList<>(maxHeap);
    }

    private static int findKthSmallestNumber(int[] ints, int k) {
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>((n1,n2) -> n2-n1);

        for (int i =0; i<ints.length; i++){
            if (i<k){
                maxHeap.add(ints[i]);
            } else {
                if (ints[i] < maxHeap.peek()){
                    maxHeap.poll();
                    maxHeap.add(ints[i]);
                }
            }
        }
        return maxHeap.poll();
    }

    private static List<Integer> findKLargestNumber(int[] ints, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();

        for (int i =0; i<ints.length; i++){
            if (i<k){
                minHeap.add(ints[i]);
            } else {
                if (ints[i] > minHeap.peek()){
                    minHeap.poll();
                    minHeap.add(ints[i]);
                }
            }
        }
        return new ArrayList<>(minHeap);
    }
}
